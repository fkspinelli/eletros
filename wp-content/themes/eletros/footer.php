    <!-- footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-3 eletros">
                    <img src="<?php bloginfo('template_url'); ?>/img/logo-footer.png" class="img-responsive">
                </div>
                <div class="col-sm-3">
                    <form>
                        <div class="form-group search">
                            <input type="search" class="form-control" placeholder="Busca">
                            <button type="submit"><i class="icon icon-basic-magnifier"></i></button>
                        </div>
                    </form>
                </div>
                <div class="col-sm-6">
                    <div class="redes-sociais">
                        <a href="https://www.facebook.com/fundacaoeletros" target="_blank"><i class="icon icon-social-facebook-circle"></i></a>
                        <a href="https://twitter.com/fundacaoeletros" target="_blank"><i class="icon icon-social-tumblr-circle"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 eletros hidden-xs">
                    A ELETROS é uma entidade fechada de previdência privada criada em 1971, em que podem ser participantes os empregados da Eletrobras, do Cepel, da EPE , do ONS, da Eletrobras Distribuição Rondônia e da própria ELETROS.
                    <br/>
                    <br/>
                    A sua função é garantir a esses participantes benefícios de aposentadoria complementares aos da Previdência Social, contribuindo assim para a manutenção do padrão de vida desses participantes e seus dependentes.
                    <div class="direitos">
                        2016 - @Fundação Eletrobrás de Seguridade Social ELETROS
                    </div>
                </div>
                <div class="col-sm-3">
                    <?php wp_nav_menu( array('menu' => 'menu-footer-left', 'container'=>'ul') ); ?>
                </div>
                <div class="col-sm-3">
                    <?php wp_nav_menu( array('menu' => 'menu-footer-right', 'container'=>'ul') ); ?>
                </div>
                <div class="col-sm-3 area-conselho" id="area-conselho-rodape">
                <p class="blue">Área do Conselho</p>
                    <p>Este é um ambiente restrito aos integrantes dos órgãos colegiados da ELETROS.</p>
                    <?php if ( is_user_logged_in() ): $current_user = wp_get_current_user(); ?>
                        <p>Olá <?php echo $current_user->user_login; ?>, <a href="<?php echo wp_logout_url(); ?>">Sair</a></p>
                    <?php else: ?>
                   <form name="loginform" id="loginform" action="/wp-login.php" method="post" autocomplete="off">
                    <div class="form-group">
                            <input type="text" name="log" id="user_login" class="form-control cpfuser" placeholder="CPF ou Email">
                        </div>
                        <div class="form-group">
                            <input type="password" name="pwd" id="user_pass" class="form-control" placeholder="Senha">
                            <a href="<?php echo wp_lostpassword_url(); ?>">Esqueci a senha</a>
                        </div>
                         <input type="hidden" name="redirect_to" value="<?php echo home_url('conselho'); ?>">
                        <button type="submit" class="btn btn-blue-1 btn-block text-uppercase">ENTRAR</button>
                        </form>
                    <?php endif; ?>
                    <!--<a href="http://www2.eletros.com.br/html/conselho.aspx" class="button-primary btn btn-blue-1 btn-block text-uppercase" target="_blank" style="margin-bottom: 10px;">Entrar</a>-->
                    <script>
                        $(document).ready(function(){
                           
                            $('#loginform').attr('autocomplete','off');
                            $('#user_login').attr( 'placeholder', 'CPF ou Email' ).attr('autocomplete','off').addClass('form-control');
                            $('#user_pass').attr( 'placeholder', 'Senha' ).attr('autocomplete','off').addClass('form-control');   
                            $('#wp-submit').addClass('btn btn-blue-1 btn-block text-uppercase')                     });
                    </script>
                    <style>
                        .login-username{margin-bottom: 0;}
                        
                    </style>
                
                    <p class="blue">Links úteis</p>
                    <div class="links">
                        <a href="http://www.previc.gov.br/" target="_blank">
                            <img src="<?php bloginfo('template_url'); ?>/img/banner-previc.png" class="img-responsive">
                        </a>
                        <a href="http://www.abrapp.org.br/Paginas/Home.aspx" target="_blank">
                            <img src="<?php bloginfo('template_url'); ?>/img/abrapp.png" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- /footer -->


    <!-- <div id="modalHomeForm" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Atualize seus dados cadastrais!</h4>
          </div>
          <div class="modal-body">
            <?php echo do_shortcode('[contact-form-7 id="5500" title="Portal do Participante"]'); ?>
          </div>
        </div>
      </div>
    </div>   

    <script type="text/javascript">
        $(document).ready(function(){
            if (localStorage.modalHomeForm == undefined) {
                $('#modalHomeForm').modal('show');
            }

            if ($('#modalHomeForm .wpcf7-form').hasClass('invalid')) {
                $('#modalHomeForm').modal('show');
            }

            if ($('#modalHomeForm .wpcf7-form').hasClass('sent')) {
                if (localStorage.modalHomeForm == undefined) {
                    localStorage.modalHomeForm = 1;
                }
            }
        });
    </script>

    <style type="text/css">
        #modalHomeForm .form-group {
            margin-bottom: 10px;
        }

        #modalHomeForm .checkbox input[type=checkbox] {
            margin-left: -26px;
        }

        #modalHomeForm .checkbox .wpcf7-list-item-label {
            position: relative;
            top: 2px;
        }
    </style> -->

</body>

</html>