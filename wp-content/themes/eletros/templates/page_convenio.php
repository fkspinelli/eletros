<!--?php /* Template name: Template Convenio */ ?-->
<?php get_header();
the_post();
get_template_part('partials/content', 'top'); ?>
<?php
global $wpdb; // this is how you get access to the database

$slug = key($_GET);

$args = array(
    'posts_per_page' => -1,
    'name' => $slug,
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
    'post_type' => 'conveniados'
);

$posts = get_posts($args);
$post = current($posts);
$postMeta = get_post_meta($post->ID);

$tablename = $wpdb->prefix . 'ratings_eletros';
$mediaRating = $wpdb->get_var(
    $wpdb->prepare(
        "
		SELECT AVG(rating)  
		FROM $tablename   
        WHERE id_post = %d
        AND enabled = 1
	",
        $post->ID
    ) );

$countRating = $wpdb->get_var(
    $wpdb->prepare(
        "
		SELECT count(rating)  
		FROM $tablename   
        WHERE id_post = %d
        AND enabled = 1
	",
        $post->ID
    ) );

$lastRating = $wpdb->get_row(
    $wpdb->prepare(
        "
		SELECT *   
		FROM $tablename   
        WHERE id_post = %d
        AND enabled = 1
		order by created_at desc
	",
        $post->ID
    ) );

?>
<!-- content -->
<section class="bg-eeeeee">
    <div>
        <div style="padding-bottom:25px;"></div>
        

        <?php eletros_breadcrumbs(); ?>

        <div style="padding-bottom:50px;"></div>

    </div>

    <div class="eletros-convenio">

        <div class="container">
            <div class="menu-c">
                <div class="row">
                    <div class="col-sm-2">
                        <img src="/wp-content/themes/eletros/img/logo-eletros-convenio.png" class="img-responsive">
                    </div>
                    <div class="col-sm-10">
                        <nav>
                            <ul>
                                <li>
                                    <a href="<?php echo esc_url(get_permalink(get_page_by_title('Eletros Convênio'))); ?>">
                                        O que é
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(get_permalink(get_page_by_title('Seja um Parceiro'))); ?>">
                                        Seja um Parceiro
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(get_permalink(get_page_by_title('Solicite seu Cartão'))); ?>">
                                        Solicite seu Cartão
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(get_permalink(get_page_by_title('Empresas conveniadas'))); ?>">
                                        Empresas conveniadas
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div id="bannerConvenios" class="carousel slide carousel-fade banner-convenio" data-ride="carousel">

            <?php
            $idGaleria = 2;

            $query = $wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "huge_itgallery_images WHERE gallery_id=%d", $idGaleria);
            $images_row = $wpdb->get_results($query);
            if (!$images_row) {
                ob_start();
                printf(__("Gallery with ID %s is empty.", "gallery-img"), $idGaleria);
                return ob_get_clean();
            }
            ?>

            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php $i = 0; ?>
                <?php foreach ($images_row as $row) : ?>
                    <li data-target="#bannerConvenios" data-slide-to="<?php echo $i ?>" <?php if ($i++ == 0) : ?>class="active"<?php endif ?>></li>
                <?php endforeach ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <?php $i = 0; ?>
                <?php foreach ($images_row as $row) : ?>
                    <div class="item <?php if ($i++ == 0) : ?>active<?php endif ?>" style="background-image: url(<?php echo $row->image_url ?>);">
                        <div class="container">
                            <div class="text">
                                <a href="<?php echo $row->sl_url ?>" target="_blank">
                                    <?php echo $row->description ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>

        <div class="block">
            <div class="container">
                <h2>Empresas Conveniadas</h2>
                <div class="panel panel-default detalhe-conveniada">
                    <div class="panel-body">
                        <?php if (strlen(current($postMeta['percentual_desconto'])) > 0): ?>
                        <span class="percent"><?php echo current($postMeta['percentual_desconto']) ?>%</span>
                        <?php endif; ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="text-center">
                                    <img src="<?php echo wp_get_attachment_image_url(current($postMeta['imagem'])) ?>">
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div>
                                    <div class="stars">
                                        
                                        <?php for($i = 0; $i < $mediaRating; $i++): ?>
                                        <i class="fa fa-star"></i>
                                        <?php endfor; ?>
                                        <?php for($i = 0; $i < abs($mediaRating - 5); $i++): ?>
                                        <i class="fa fa-star-o"></i>
                                        <?php endfor; ?>
                                    </div>
                                </div>
                                <div class="row" style="min-height: 110px;">
                                    <div class="col-sm-6 border-right">
                                        <?php echo current($postMeta['descricao_da_empresa']) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php echo current($postMeta['texto_obervacao_desconto']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr style="margin: 40px 0;">
                        <div class="row">
                            <div class="col-sm-6">
                                <hr style="margin: 0 0 20px;">
                                <div class="text">
                                    <?php if ($lastRating): ?>
                                        <?php $dateRating = date_create_from_format('Y-m-d H:i:s', $lastRating->created_at); ?>
                                        <p><b><?php echo $dateRating->format('d/m/Y') ?></b></p>
                                        <p><i>“<?php echo $lastRating->description ?>”</i></p>
                                        <h5><?php echo $lastRating->name ?></h5>
                                    <?php else: ?>
                                        <p>Nenhum avaliação</p>
                                    <?php endif; ?>
                                </div>
                                <hr style="margin: 20px 0;">
                                
                                    <?php if (($countRating - 1) > 1): ?>
                                        <a href="#" id="btn-ver-avaliacoes" data-id="<?php echo $post->ID ?>">Veja todas as <?php echo ($countRating - 1) ?> Avaliações  <i class="fa fa-arrow-right"></i></a>
                                    <?php endif; ?>

                            </div>
                            <div class="col-sm-6">
                                <h6>Avalie você também!</h6>
                                <form id="form-avaliacao" data-parsley-validate>
                                    <input type="hidden" name="post_id" id="post-id" value="<?php echo $post->ID ?>">
                                    <div class="stars">
                                        <input type="radio" id="star0" name="star" class="star_checked" value="" checked/>

                                        <label for="star1"><i class="fa"></i></label>
                                        <input type="radio" id="star1" name="star" class="star_checked" value="1"/>

                                        <label for="star2"><i class="fa"></i></label>
                                        <input type="radio" id="star2" name="star" class="star_checked" value="2"/>

                                        <label for="star3"><i class="fa"></i></label>
                                        <input type="radio" id="star3" name="star" class="star_checked" value="3"/>

                                        <label for="star4"><i class="fa"></i></label>
                                        <input type="radio" id="star4" name="star" class="star_checked" value="4"/>

                                        <label for="star5"><i class="fa"></i></label>
                                        <input type="radio" id="star5" name="star" class="star_checked" value="5"/>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="name" id="avaliacao-name" class="form-control" placeholder="Nome"
                                                       required data-parsley-required-message="Informe seu nome">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="number_eletros" class="form-control" id="avaliacao-number-eletros"
                                                       placeholder="Nº Eletros" required data-parsley-required-message="Informe seu Número Eletros">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <textarea class="form-control" name="message" placeholder="Mensagem" id="avaliacao-message"
                                                          rows="5" required data-parsley-required-message="Informe a Mensagem"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-gray-1 btn-lg flat btn-block text-uppercase"
                                               >
                                                enviar avaliação
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <hr style="margin: 60px 0;">
                        <div class="row div-avaliacao" style="display: none;">
                            <div class="col-sm-12">
                                <h3>Avaliações</h3>
                                <div class="avaliacoes">
                                    <ul>
                                    </ul>
                                </div>
                                <div class="pagination-nav text-right">
                                    <div class="pull-right">
                                        <span class="count"></span>
                                        <ul>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $idGaleria = 3;

        $query = $wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "huge_itgallery_images WHERE gallery_id=%d", $idGaleria);
        $images_row = $wpdb->get_results($query);
        if (!$images_row) {
            ob_start();
            printf(__("Gallery with ID %s is empty.", "gallery-img"), $idGaleria);
            return ob_get_clean();
        }
        ?>
        <div class="bg-ffffff">
            <div class="block">
                <div class="container">
                    <h2>Promoções do mês</h2>
                    <div class="promo">
                        <div class="row">
                            <div class="col-sm-1 text-left hidden-xs">
                                <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                            </div>
                            <div class="col-sm-10">
                                <div class="promo-slider">
                                    <?php foreach ($images_row as $row) : ?>
                                        <div>
                                            <a href="<?php echo $row->sl_url ?>" target="_blank">
                                                <img src="<?php echo $row->image_url ?>">
                                                <p> 
                                                    <?php echo $row->description ?>
                                                </p>
                                            </a>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            </div>
                            <div class="col-sm-1 text-right hidden-xs">
                                <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $(document).on('ready', function() {
                            $('.promo-slider').slick({
                                slidesToShow: 2,
                                slidesToScroll: 1,
                                autoplay: true,
                                autoplaySpeed: 2000,
                                prevArrow: $('.promo .fa-chevron-circle-left'),
                                nextArrow: $('.promo .fa-chevron-circle-right'),
                                responsive: [
                                    {
                                        breakpoint: 767,
                                        settings: {
                                            slidesToShow: 1,
                                            slidesToScroll: 1
                                        }
                                    }
                                ]
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
</section>
<!-- /content -->
<?php get_footer(); ?>