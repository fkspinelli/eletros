<!--?php /* Template name: Template Atas */ ?-->
<?php if(wp_get_current_user()->roles[0] == 'administrator' || wp_get_current_user()->roles[0] == 'subscriber'): ?>
<?php 
get_header(); get_template_part('partials/content','top'); 

?>
  <section class="bg-eeeeee">
        <div>
           <?php eletros_breadcrumbs(); ?>
            <div style="padding-bottom:50px;"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-3"> 
                    <a href="/conselhos/" class="btn btn-default text-uppercase" style="margin-bottom: 20px;"><i class="fa fa-chevron-left" aria-hidden="true" style="vertical-align: middle;"></i> Voltar</a>
                </div>
            </div>
            <div class="row tabs tab-vertical">
                <div class="col-sm-3 tab-col-left">
                    <ul>
                       <?php  $c = $_GET['conselho'];  $args = array('post_type'=>'conselhos'); query_posts($args); while (have_posts()): the_post(); ?>
                        <li class="<?php echo $c == $post->post_name ? 'active' : null; ?>"><a href="<?php bloginfo('url'); ?>/atas?conselho=<?php echo $post->post_name; ?>"><?php the_title(); ?></a></li>                       
                    <?php endwhile;  ?>
                    </ul>
                </div>
                <div class="col-sm-9 tab-col-right font-zoom">
                    <div class="tab-content">

                        <?php $t = 0; while (have_posts()): the_post(); ?>
                            <?php if ($c == $post->post_name): ?>
                            <div id="tab1" class="tab-pane fade <?php echo $c == $post->post_name ? 'in active' : null; ?> collapse-reunioes">
                                <div class="area-conselho">
                                    <h2>
                                    <?php
                                    if ($_GET['conselho'] == 'conselho-deliberativo' && $_GET['ano-especifico'] == '2018') {
                                        echo 'Reuniões';
                                    } else {
                                        //echo 'Atas de reuniões';
                                        echo 'Reuniões';
                                    }
                                    ?>
                                    // <?php the_title(); ?>
                                    </h2>
                                    <?php //get_template_part('partials/content','buscaata'); ?>

                                    <form name="filtro-reunioes">
                                        <input type="hidden" name="type_filter" value="1">
                                        <div class="fix">
                                            <div class="radio pull-left">
                                                <input id="radio_2_1975" type="radio" value="1" name="f" checked>
                                                <label for="radio_2_1975"></label>
                                                <span>Reuniões de um ano específico</span>
                                            </div>
                                            <select class="form-control ano pull-left" name="ano-especifico">
                                                <option value="">Ano</option>
                                                
                                                <?php $anos = CFS()->get( 'anos' ); if ( count( $anos ) ) { ?>
                                                    <?php $i_ano = 0; foreach ( $anos as $ano ) { ?>
                                                        <?php
                                                            foreach ( $ano['ano'] as $key => $label ) {
                                                                echo "<option>".$label."</option>";
                                                            }
                                                        ?>
                                                    <?php } ?>
                                                <?php } ?>

                                                <?php
                                                $atas = get_field('atas'); 
                                                if(!empty($atas)): 
                                                    $ata = new WP_Query( array(
                                                        'post_type' => 'atas',
                                                        'posts_per_page' => -1,
                                                        'order' => 'DESC',
                                                        'orderby' => 'meta_value',
                                                        'meta_type' => 'DATE',
                                                        'meta_key' => 'data_emissao',
                                                        'meta_date_format' => '%d/%m/%Y', // esse filtro NÃO é padrão WP, foi criado no arquivo functions.php
                                                    ) );

                                                    $_y = 0;
                                                    while ( $ata->have_posts() ) : $ata->the_post();  

                                                        $date = explode('/', get_field('data_emissao', $ata->ID));
                                                        $y = (int)$date[2];

                                                        if ($y != $_y && $y != 0) {
                                                            echo "<option>".$y."</option>";
                                                            $_y = $y;
                                                        }

                                                    endwhile;  
                                                    wp_reset_postdata();
                                                endif; 
                                                ?>

                                            </select>
                                        </div>
                                        <div class="fix">
                                            <div class="radio">
                                                <input id="radio_3__1975" type="radio" value="2" name="f">
                                                <label for="radio_3__1975"></label>
                                                <span>Reuniões de um período específico</span>
                                            </div>
                                        </div>
                                        <div class="row periodo">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>De:</label>
                                                    <select name="mes_from" class="form-control" disabled>
                                                        <option value="">Mês</option>
                                                        <option value="1">Janeiro</option>
                                                        <option value="2">Fevereiro</option>
                                                        <option value="3">Março</option>
                                                        <option value="4">Abril</option>
                                                        <option value="5">Maio</option>
                                                        <option value="6">Junho</option>
                                                        <option value="7">Julho</option>
                                                        <option value="8">Agosto</option>
                                                        <option value="9">Setembro</option>
                                                        <option value="10">Outubro</option>
                                                        <option value="11">Novembro</option>
                                                        <option value="12">Dezembro</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>&nbsp;</label>
                                                    <select class="form-control" name="ano_from" disabled> 
                                                        <option value="">Ano</option>
                                                        
                                                        <?php $anos = CFS()->get( 'anos' ); if ( count( $anos ) ) { ?>
                                                            <?php $i_ano = 0; foreach ( $anos as $ano ) { ?>
                                                                <?php
                                                                    foreach ( $ano['ano'] as $key => $label ) {
                                                                        echo "<option>".$label."</option>";
                                                                    }
                                                                ?>
                                                            <?php } ?>
                                                        <?php } ?>

                                                        <?php
                                                        $atas = get_field('atas'); 
                                                        if(!empty($atas)): 
                                                            $ata = new WP_Query( array(
                                                                'post_type' => 'atas',
                                                                'posts_per_page' => -1,
                                                                'order' => 'DESC',
                                                                'orderby' => 'meta_value',
                                                                'meta_type' => 'DATE',
                                                                'meta_key' => 'data_emissao',
                                                                'meta_date_format' => '%d/%m/%Y', // esse filtro NÃO é padrão WP, foi criado no arquivo functions.php
                                                            ) );

                                                            $_y = 0;
                                                            while ( $ata->have_posts() ) : $ata->the_post();  

                                                                $date = explode('/', get_field('data_emissao', $ata->ID));
                                                                $y = (int)$date[2];

                                                                if ($y != $_y && $y != 0) {
                                                                    echo "<option>".$y."</option>";
                                                                    $_y = $y;
                                                                }

                                                            endwhile;  
                                                            wp_reset_postdata();
                                                        endif; 
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Até:</label>
                                                    <select name="mes_to" class="form-control" disabled>
                                                        <option value="">Mês</option>
                                                        <option value="1">Janeiro</option>
                                                        <option value="2">Fevereiro</option>
                                                        <option value="3">Março</option>
                                                        <option value="4">Abril</option>
                                                        <option value="5">Maio</option>
                                                        <option value="6">Junho</option>
                                                        <option value="7">Julho</option>
                                                        <option value="8">Agosto</option>
                                                        <option value="9">Setembro</option>
                                                        <option value="10">Outubro</option>
                                                        <option value="11">Novembro</option>
                                                        <option value="12">Dezembro</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>&nbsp;</label>
                                                    <select class="form-control" name="ano_to" disabled>
                                                        <option value="">Ano</option>
                                                        
                                                        <?php $anos = CFS()->get( 'anos' ); if ( count( $anos ) ) { ?>
                                                            <?php $i_ano = 0; foreach ( $anos as $ano ) { ?>
                                                                <?php
                                                                    foreach ( $ano['ano'] as $key => $label ) {
                                                                        echo "<option>".$label."</option>";
                                                                    }
                                                                ?>
                                                            <?php } ?>
                                                        <?php } ?>

                                                        <?php
                                                        $atas = get_field('atas'); 
                                                        if(!empty($atas)): 
                                                            $ata = new WP_Query( array(
                                                                'post_type' => 'atas',
                                                                'posts_per_page' => -1,
                                                                'order' => 'DESC',
                                                                'orderby' => 'meta_value',
                                                                'meta_type' => 'DATE',
                                                                'meta_key' => 'data_emissao',
                                                                'meta_date_format' => '%d/%m/%Y', // esse filtro NÃO é padrão WP, foi criado no arquivo functions.php
                                                            ) );

                                                            $_y = 0;
                                                            while ( $ata->have_posts() ) : $ata->the_post();  

                                                                $date = explode('/', get_field('data_emissao', $ata->ID));
                                                                $y = (int)$date[2];

                                                                if ($y != $_y && $y != 0) {
                                                                    echo "<option>".$y."</option>";
                                                                    $_y = $y;
                                                                }

                                                            endwhile;  
                                                            wp_reset_postdata();
                                                        endif; 
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <a href="#" class="btn btn-blue-2 btn-block text-uppercase filter">Filtrar</a>
                                            </div>
                                        </div>
                                        <hr>
                                    </form>


                                    <div id="b<?php echo $post->ID; ?>" class="row">
                                        <div class="col-sm-12">
                                            <h3>
                                            <?php
                                            // if ($_GET['conselho'] == 'conselho-deliberativo' && $_GET['ano-especifico'] == '2018') {
                                            //     echo 'Reunião 337ª do CDE - 18/01/2018';
                                            // } else {
                                            //     echo 'Reuniões';
                                            // }
                                            ?>
                                            </h3>

                                            <?php 
                                            // Anos
                                            $anos = CFS()->get( 'anos' );
                                            if ( count( $anos ) ) {

                                                function monthToInt($month) {
                                                    switch ($month) {
                                                        case "Janeiro":
                                                            return 1;
                                                            break;
                                                        case "Fevereiro":
                                                            return 2;
                                                            break;
                                                        case "Março":
                                                            return 3;
                                                            break;
                                                        case "Abril":
                                                            return 4;
                                                            break;
                                                        case "Maio":
                                                            return 5;
                                                            break;
                                                        case "Junho":
                                                            return 6;
                                                            break;
                                                        case "Julho":
                                                            return 7;
                                                            break;
                                                        case "Agosto":
                                                            return 8;
                                                            break;
                                                        case "Setembro":
                                                            return 9;
                                                            break;
                                                        case "Outubro":
                                                            return 10;
                                                            break;
                                                        case "Novembro":
                                                            return 11;
                                                            break;
                                                        case "Dezembro":
                                                            return 12;
                                                            break;
                                                    }
                                                }


                                                ?>

                                                <?php $i_ano = 0; foreach ( $anos as $ano ) { ?>
                                                <div class="box-conselho" style="margin-bottom: 5px;" data-year="<?php foreach ( $ano['ano'] as $key => $label ) { echo $label; } ?>">
                                                    <div class="box-header">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse"  href="#collapse_ano_<?php echo $i_ano; ?>">
                                                            <?php
                                                            foreach ( $ano['ano'] as $key => $label ) {
                                                                echo $label; //ano
                                                            }
                                                            ?>
                                                            <i class="fa fa-caret-right"></i>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse_ano_<?php echo $i_ano; ?>" class="panel-collapse collapse">
                                                        <div class="box-body">

                                                            <?php if ( count( $ano['meses'] ) ) { ?>
                                                                <div class="panel-group" id="accordion_mes_<?php echo $i_ano; ?>">  
                                                                <?php $i_mes = 0; foreach ( $ano['meses'] as $meses ) { ?>  
                                                                    <?php foreach ( $meses['mes'] as $key => $label ) { ?>  

                                                                        
                                                                        <div class="box-conselho" style="margin-bottom: 5px;" data-month="<?php echo monthToInt($label); ?>">
                                                                            <div class="box-header">
                                                                                <h4 class="panel-title">
                                                                                    <a data-toggle="collapse"  href="#collapse_mes_<?php echo $i_ano."_".$i_mes; ?>">
                                                                                        <?php echo $label; //mes ?>
                                                                                        <i class="fa fa-caret-right"></i>
                                                                                    </a>
                                                                                </h4>
                                                                            </div>
                                                                            <div id="collapse_mes_<?php echo $i_ano."_".$i_mes; ?>" class="panel-collapse collapse">
                                                                                <div class="box-body">

                                                                                    <?php if ( count( $meses['reunioes'] ) ) { ?> 
                                                                                    <div class="panel-group" id="accordion_reuniao_<?php echo $i_ano."_".$i_mes; ?>">
                                                                                        <?php $i_reuniao = 0; foreach ( $meses['reunioes'] as $reunioes ) { ?>
                                                                                        <div class="box-conselho" style="margin-bottom: 5px;">
                                                                                            <div class="box-header">
                                                                                                <h4 class="panel-title">
                                                                                                    <a data-toggle="collapse"  href="#collapse_reuniao_<?php echo $i_ano."_".$i_mes."_".$i_reuniao; ?>">
                                                                                                        <?php echo $reunioes['reuniao']; //reuniao ?> - 
                                                                                                        
                                                                                                        <small><?php echo date("d/m/Y", strtotime($reunioes['data'])); //data?></small>
                                                                                                        <i class="fa fa-caret-right"></i>
                                                                                                    </a>
                                                                                                </h4>
                                                                                            </div>
                                                                                            <div id="collapse_reuniao_<?php echo $i_ano."_".$i_mes."_".$i_reuniao; ?>" class="panel-collapse collapse">
                                                                                                <div class="box-body">

                                                                                                    <ul class="atas">
                                                                                                        <?php if (!empty($reunioes['ata'])): ?>
                                                                                                        <li>
                                                                                                            <a href="<?php echo $reunioes['ata'];?>">
                                                                                                                <?php echo $reunioes['titulo_da_ata']; //titulo_da_ata?>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <?php endif; ?>
                                                                                                        <?php if (!empty($reunioes['pauta'])): ?>
                                                                                                        <li>
                                                                                                            <a href="<?php echo $reunioes['pauta'];?>">
                                                                                                                <?php echo $reunioes['titulo_da_pauta']; //titulo_da_pauta?>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <?php endif; ?>
                                                                                                    </ul>

                                                                                                    <?php if ( count( $reunioes['materiais_complementares'] ) ) { ?>
                                                                                                    <div class="panel-group" id="accordion_materiais_<?php echo $i_ano."_".$i_mes."_".$i_reuniao; ?>">
                                                                                                        <div class="box-conselho">
                                                                                                            <div class="box-header">
                                                                                                                <h4 class="panel-title">
                                                                                                                    <a data-toggle="collapse"  href="#collapse_material_<?php echo $i_ano."_".$i_mes."_".$i_reuniao; ?>">
                                                                                                                    Materiais da Reunião
                                                                                                                    <i class="fa fa-caret-right"></i>
                                                                                                                    </a>
                                                                                                                </h4>
                                                                                                            </div>
                                                                                                            <div id="collapse_material_<?php echo $i_ano."_".$i_mes."_".$i_reuniao; ?>" class="panel-collapse collapse">
                                                                                                                <div class="box-body">
                                                                                                                    <ul class="atas">
                                                                                                                    <?php foreach ( $reunioes['materiais_complementares'] as $materiais_complementares ) { ?>  
                                                                                                                        <li>
                                                                                                                            <a href="<?php echo $materiais_complementares['material_complementar'];?>" download>
                                                                                                                                <?php echo $materiais_complementares['titulo_do_material_complementar']; //titulo_da_pauta?>
                                                                                                                            </a>
                                                                                                                        </li>
                                                                                                                    <?php } ?>
                                                                                                                    </ul>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div> 
                                                                                                    <?php } ?>

                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <?php $i_reuniao++; } ?> 
                                                                                    </div> 
                                                                                    <?php } ?> 

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        

                                                                    <?php } ?>  
                                                                <?php $i_mes++; } ?>  
                                                                </div> 
                                                            <?php } ?>  

                                                        </div>
                                                    </div>
                                                </div>
                                                <?php $i_ano++; } ?>
                                            <?php } ?>







                                            <div class="area-conselho">
                                                <!-- atas antigas -->
                                                <?php
                                                $atas = get_field('atas'); 
                                                if(!empty($atas)): 
                                                    $ata = new WP_Query( array(
                                                        'post_type' => 'atas',
                                                        'posts_per_page' => -1,
                                                        'order' => 'DESC',
                                                        'orderby' => 'meta_value',
                                                        'meta_type' => 'DATE',
                                                        'meta_key' => 'data_emissao',
                                                        'meta_date_format' => '%d/%m/%Y', // esse filtro NÃO é padrão WP, foi criado no arquivo functions.php
                                                    ) );

                                                    $_y = 0;
                                                    while ( $ata->have_posts() ) : $ata->the_post();  

                                                        $date = explode('/', get_field('data_emissao', $ata->ID));
                                                        $y = (int)$date[2];
                                                        $m = (int)$date[1];
                                                        $d = (int)$date[0];

                                                        if ($y != $_y) {
                                                            echo "</ul></div> </div> </div>";
                                                ?>

                                                <div class="box-conselho" style="margin-bottom: 5px;" data-year="<?php echo $y; ?>">
                                                    <div class="box-header">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse"  href="#collapse_ano_old_<?php echo $y.'-'.$_GET['conselho'].'-'.$t; ?>">
                                                                <?php echo $y; ?>
                                                                <i class="fa fa-caret-right"></i>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse_ano_old_<?php echo $y.'-'.$_GET['conselho'].'-'.$t; ?>" class="panel-collapse collapse">
                                                        <div class="box-body">
                                                            <ul class="atas">

                                                                <?php $_y = $y; } ?>

                                                                <li data-month="<?php echo $m; ?>">
                                                                <?php $arq = get_field('arquivo', $ata->ID); ?>
                                                                    <a href="<?php echo $arq['url']; ?>" download><?php echo the_title(); ?> - <span>[ <?php echo get_field('data_emissao', $ata->ID); ?> ]</span></a>
                                                                </li>

                                                <?php
                                                    endwhile;  
                                                    wp_reset_postdata();
                                                    endif; 
                                                ?>
                                            </div>




                                                
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                       <?php $t++; endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>



<script>
    $(document).ready(function(){
        $('.item-home a').text('Conselhos').prop('href','/conselhos');
    });
</script>
    <!-- /content -->
<?php get_footer(); else: wp_redirect(home_url()); exit(); endif; ?>