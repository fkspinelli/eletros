<!--?php /* Template name: Template menu com Tabs */ ?-->
<?php get_header(); the_post(); get_template_part('partials/content','top'); ?>
<section class="bg-eeeeee">
<div>
  	<?php eletros_breadcrumbs(); ?>

    <div style="padding-bottom:50px;"></div>
</div>
<div class="container">
    
    <?php get_template_part('partials/content', 'social'); ?>

    <div class="row tabs tab-vertical">
        <div class="col-sm-3 tab-col-left">
            <ul>
            	<?php $args = array(
                	'post_type' => 'page',
                	'posts_per_page' => -1,
                	'post_status' => 'publish',
                	'order' => 'ASC',
                	'post_parent' => $post->ID
                );
                 query_posts($args); $cont = 0;
                 while(have_posts()): the_post(); $cont++; ?>
                    <li class="<?php echo $cont == '1'? 'active' : null; ?>"><a data-toggle="pill" href="#tab_<?php echo $post->ID; ?>"><?php the_title(); ?></a></li>
                <?php endwhile; wp_reset_query(); ?>
            </ul>
            <div class="diver"></div>
            <div class="files">
               <?php the_content(); ?>
            </div>
        </div>
        <div class="col-sm-9 tab-col-right font-zoom">
            <div class="tab-content">
            <?php  query_posts($args); $cont_2 = 0; while(have_posts()): the_post(); $cont_2++; ?>
                <div id="tab_<?php echo $post->ID; ?>" class="tab-pane fade in <?php echo $cont_2 == '1'? 'active' : null; ?>">
                <h3><?php the_title(); ?></h3>
                 	<?php the_content(); ?>
                </div>
            <?php endwhile; wp_reset_query(); ?>
            </div>
        </div>
    </div>
</div>
</section>
<script type="text/javascript">
    function Soma() {
        var soma1, soma2, soma3, soma4, soma5, soma6, soma7;
        soma1 = 0;
        soma2 = 0;
        soma3 = 0;
        soma4 = 0;
        soma5 = 0;
        soma6 = 0;
        soma7 = 0;

        for (i = 1; i <= 7; i++) {
            for (j = 1; j <= 5; j++) {
                x = document.getElementById('txt' + i + '_' + j);
                if (x.checked) {
                    if (j == 1)
                        soma1 = soma1 + parseInt(x.value);
                    if (j == 2)
                        soma2 = soma2 + parseInt(x.value);
                    if (j == 3)
                        soma3 = soma3 + parseInt(x.value);
                    if (j == 4)
                        soma4 = soma4 + parseInt(x.value);
                    if (j == 5)
                        soma5 = soma5 + parseInt(x.value);
                }
            }
        }
        //window.alert(soma1);
        document.getElementById('txt8_1').value = soma1;
        document.getElementById('txt8_2').value = soma2;
        document.getElementById('txt8_3').value = soma3;
        document.getElementById('txt8_4').value = soma4;
        document.getElementById('txt8_5').value = soma5;
        document.getElementById('TxtSomaGeral').innerHTML = soma1 + soma2 + soma3 + soma4 + soma5;

    }

</script>
<!-- // content -->
<?php get_footer(); ?>