<!--?php /* Template name: Template especial cores */ ?-->
<?php get_header(); the_post(); get_template_part('partials/content','top'); ?>
<?php
function closetags($html) {
    preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
    $openedtags = $result[1];
    preg_match_all('#</([a-z]+)>#iU', $html, $result);
    $closedtags = $result[1];
    $len_opened = count($openedtags);
    if (count($closedtags) == $len_opened) {
        return $html;
    }
    $openedtags = array_reverse($openedtags);
    for ($i=0; $i < $len_opened; $i++) {
        if (!in_array($openedtags[$i], $closedtags)) {
            $html .= '</'.$openedtags[$i].'>';
        } else {
            unset($closedtags[array_search($openedtags[$i], $closedtags)]);
        }
    }
    return $html;
}
?>
<!-- content -->
<section class="bg-eeeeee">
    <div>
        <div style="padding-bottom:25px;"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                   <a target="_blank" href="<?php echo get_field('link'); ?>">
                    <?php get_the_image(['size'=>'normal','link_to_post'=>false]); ?>
                   </a>
                </div>
            </div>
        </div>

        <?php eletros_breadcrumbs(); ?>

        <div style="padding-bottom:50px;"></div>

    </div>
    <div class="container">
       <?php get_template_part('partials/content', 'social'); ?>
        <div class="row tabs tabs-warning tab-vertical">
            <div class="col-sm-3 tab-col-left">
                <ul>
                   <?php $args = array(
                    'post_type' => 'page',
                    'posts_per_page' => -1,
                    'post_status' => 'publish',
                    'order' => 'ASC',
                    'post_parent' => $post->ID
                );
                 query_posts($args); $cont = 0;
                 $paginas = [];
                 while(have_posts()): the_post(); $cont++; ?>
                    <?php $paginas[] = $post->ID; ?>
                    <li class="<?php echo $cont == '1'? 'active' : null; ?>"><a data-toggle="pill" href="#tab_<?php echo $post->ID; ?>"><?php the_title(); ?></a></li>
                <?php endwhile; wp_reset_query(); ?>
                   
                </ul>
               
            </div>
            
            <div class="col-sm-9 tab-col-right font-zoom">
                <div class="tab-content">
                     <?php $cont_2 = 0; ?>
                     <?php foreach($paginas as $id): ?>
                     
                        <?php $cont_2++; $post = get_post($id); setup_postdata($post); ?>
                        <div id="tab_<?php echo $id; ?>" class="tab-pane fade in <?php echo $cont_2 == '1'? 'active' : null; ?>">
                       
                            <h3><?php /**echo $page->post_title;**/ the_title() ?></h3> 

                            <?php  
                            ob_start(); 
                            the_content();
                            $ret = ob_get_contents();
                            ob_end_clean();
                            echo closetags($ret);
                             ?>
                            
                        </div>
                    
                    <?php endforeach; ?>
                    <div class="extra_content"></div>
                    <span style="display: none; margin: auto; width: 200px;" class="loader"><svg style="vertical-align: -16px;" width='42px' height='42px' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-facebook"><rect x="0" y="0" width="100" height="100" fill="#ffffff" class="bk"></rect><g transform="translate(20 50)"><rect x="-10" y="-30" width="20" height="60" fill="#21539b" opacity="0.6"><animateTransform attributeName="transform" type="scale" from="2" to="1" begin="0s" repeatCount="indefinite" dur="1s" calcMode="spline" keySplines="0.1 0.9 0.4 1" keyTimes="0;1" values="2;1"></animateTransform></rect></g><g transform="translate(50 50)"><rect x="-10" y="-30" width="20" height="60" fill="#21539b" opacity="0.8"><animateTransform attributeName="transform" type="scale" from="2" to="1" begin="0.1s" repeatCount="indefinite" dur="1s" calcMode="spline" keySplines="0.1 0.9 0.4 1" keyTimes="0;1" values="2;1"></animateTransform></rect></g><g transform="translate(80 50)"><rect x="-10" y="-30" width="20" height="60" fill="#21539b" opacity="0.9"><animateTransform attributeName="transform" type="scale" from="2" to="1" begin="0.2s" repeatCount="indefinite" dur="1s" calcMode="spline" keySplines="0.1 0.9 0.4 1" keyTimes="0;1" values="2;1"></animateTransform></rect></g></svg> Carregando</span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /content -->

<?php get_footer(); ?>