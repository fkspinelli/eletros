<!--?php /* Template name: Template convênio mãe */ ?-->
<?php get_header();
the_post();
get_template_part('partials/content', 'top'); ?>

<!-- content -->
<section class="bg-eeeeee">
    <div>
        <div style="padding-bottom:25px;"></div>
        

        <?php eletros_breadcrumbs(); ?>

        <div style="padding-bottom:50px;"></div>

    </div>

    <div class="eletros-convenio">

        <div class="container">
            <div class="menu-c">
                <div class="row">
                    <div class="col-sm-2">
                        <img src="/wp-content/themes/eletros/img/logo-eletros-convenio.png" class="img-responsive">
                    </div>
                    <div class="col-sm-10">
                    <nav>
                        <ul>
                            <li>
                                <a href="<?php echo esc_url(get_permalink(get_page_by_title('Eletros Convênio'))); ?>">
                                    O que é
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo esc_url(get_permalink(get_page_by_title('Seja um Parceiro'))); ?>">
                                    Seja um Parceiro
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo esc_url(get_permalink(get_page_by_title('Solicite seu Cartão'))); ?>">
                                    Solicite seu Cartão
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo esc_url(get_permalink(get_page_by_title('Empresas conveniadas'))); ?>">
                                    Empresas conveniadas
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div id="bannerConvenios" class="carousel slide carousel-fade banner-convenio" data-ride="carousel">

        <?php
        $idGaleria = 2;

        $query = $wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "huge_itgallery_images WHERE gallery_id=%d", $idGaleria);
        $images_row = $wpdb->get_results($query);
        if (!$images_row) {
            ob_start();
            printf(__("Gallery with ID %s is empty.", "gallery-img"), $idGaleria);
            return ob_get_clean();
        }
        ?>

        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php $i = 0; ?>
            <?php foreach ($images_row as $row) : ?>
                <li data-target="#bannerConvenios" data-slide-to="<?php echo $i ?>" <?php if ($i++ == 0) : ?>class="active"<?php endif ?>></li>
            <?php endforeach ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <?php $i = 0; ?>
            <?php foreach ($images_row as $row) : ?>
                
                <div class="item <?php if ($i++ == 0) : ?>active<?php endif ?>" style="background-image: url(<?php echo $row->image_url ?>);">
                    <div class="container">
                        <div class="text">
                            <a href="<?php echo $row->sl_url ?>" target="_blank">
                                <?php echo $row->description ?>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
    <?php
    $destaques = null;
    $pagina = get_page_by_title('Eletros Convênio');
    $pagina_conveniadas = get_page_by_title('Empresas conveniadas');
    $pagina_seja_parceiro = get_page_by_title('Seja um Parceiro');
    $pagina_solicite_seu_cartao = get_page_by_title('Solicite seu Cartão');

    $destaques = ($pagina->post_name == $post->post_name);
    ?>
    

    <?php if (in_array($post->post_name, array($pagina_solicite_seu_cartao->post_name, $pagina_seja_parceiro->post_name)) ): ?>
    
    <div class="container">
        <?php the_content(); ?>
    </div>
    <?php endif; ?>
    
    <?php
    if ($pagina->post_name == $post->post_name) :
    ?>

    <?php

        if(!empty($_GET['s'])) {

            $search_term = $_GET['s'];

            $args = array(
                'post_type' => 'post',
                's' => $search_term,
                'post_status' => 'publish',
                'orderby'     => 'title',
                'order'       => 'ASC'
            );

            $wp_query = new WP_Query($args);

            if($the_query_post->have_posts()) {
                while($the_query_post->have_posts()) {
                    $the_query_post->the_post();
                    echo "<p>".get_the_title()."</p><hr>";
                }
            }

            global $wpdb;

            //$id = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_type = 'photo' ORDER BY ID DESC LIMIT 1" );
        }
    ?>
    <div class="container">
        <div class="box-text-convenio">
            <p>A Eletros oferece aos seus participantes e dependentes o Programa de Benefícios Eletros Convênio. Com ele é possível obter descontos e condições especiais em estabelecimentos de diversos segmentos de serviços e produtos tais como restaurantes, academias, agências de viagens, colégios, faculdades, universidades, drogarias e farmácias, entre outros.</p>
            <p>Para ter direito aos descontos e vantagens, basta apresentar o seu cartão Eletros Convênio em nossa rede conveniada.</p>
            <p>O cartão de convênio é de uso exclusivo dos participantes da ELETROS.</p>
        </div>
    </div>
    <div class="block">
        <div class="container">
            <h2>Conveniadas em destaque</h2>
            
            <div class="row" id="lista-conveniadas">
                
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-push-4">
                    <a href="/eletros-convenio/empresas/" class="btn btn-gray-1 btn-lg flat btn-block text-uppercase" id="btn-carregar-conveniadas">Ver todas as Conveniadas</a>
                </div>
            </div>
        </div>
    </div>
    
    <?php endif; ?>

    <?php if ($pagina_conveniadas->post_name == $post->post_name): ?>
    <div class="block">
        <div class="container">
            <h2>Empresas Conveniadas</h2>
            <h3>Gosta muito de uma empresa e gostaria que ela fosse conveniada? Clique no botão e preencha o formulário.</h3>
            <div class="row">
                <div class="col-sm-4 col-sm-push-4">
                    <a href="#" class="btn btn-orange btn-md flat text-uppercase btn-block" data-toggle="modal" data-target="#suggestion" style="margin: 0 0 40px;">Sugerir uma empresa</a>
                </div>
            </div>
            <div class="form-conveniadas">
                <form method="GET">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="#" id="btn-filter" class="btn btn-filter btn-block" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#filter" style="margin-bottom: 10px;"> <i class="fa fa-filter"></i> FILTRAR CONVENIADAS </a>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group select">
                                <span class="input-group-addon"><i class="fa fa-list-ul"></i></span>
                                <select class="form-control" id="selectOrdem">
                                    <option>ORDENAR LISTA</option>
                                    <option value="az">A - Z</option>
                                    <option value="za">Z - A</option>
                                    <option value="novo">Mais novas</option>
                                    <option value="antigo">Mais antigas</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                <input type="text" name="s" id="search" class="form-control" placeholder="BUSCA">
                                <div class="input-group-btn">
                                    <button class="btn btn-default btn-filtrar" type="button">
                                        <i class="fa fa-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row" id="lista-conveniadas">
                
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-push-4">
                    <a href="#" id="btn-carregar-conveniadas" class="btn btn-gray-1 btn-lg flat btn-block text-uppercase">Carregar mais</a>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
        
            
    <?php
    $idGaleria = 3;

    $query = $wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "huge_itgallery_images WHERE gallery_id=%d", $idGaleria);
    $images_row = $wpdb->get_results($query);
    if (!$images_row) {
        ob_start();
        printf(__("Gallery with ID %s is empty.", "gallery-img"), $idGaleria);
        return ob_get_clean();
    }
    ?>
    <div class="bg-ffffff">
        <div class="block">
            <div class="container">
                <h2>Promoções do mês</h2>
                <div class="promo">
                    <div class="row">
                        <div class="col-sm-1 text-left hidden-xs">
                            <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                        </div>
                        <div class="col-sm-10">
                            <div class="promo-slider">
                                <?php foreach ($images_row as $row) : ?>
                                <div>
                                    <a href="<?php echo $row->sl_url ?>" target="_blank">
                                        <img src="<?php echo $row->image_url ?>">
                                        <p> 
                                            <?php echo $row->description ?>
                                        </p>
                                    </a>
                                </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                        <div class="col-sm-1 text-right hidden-xs">
                            <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                $(document).on('ready', function() {
                    $('.promo-slider').slick({
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    autoplay: true,
                    autoplaySpeed: 2000,
                    prevArrow: $('.promo .fa-chevron-circle-left'),
                    nextArrow: $('.promo .fa-chevron-circle-right'),
                    responsive: [
                    {
                        breakpoint: 767,
                        settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                        }
                    }
                    ]
                    });
                });

                $('#btn-filter').on('click', function(e){

                    e.preventDefault();
                    $('#filter .modal').modal('show');
                });

                </script>
            </div>
        </div>
    </div>
</section>

<!-- /content -->
<div id="filter" class="modal fade suggestion filter form-conveniadas" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <a href="#" class="close-modal" data-dismiss="modal">FECHAR <i class="fa fa-times" aria-hidden="true"></i></a>
                <form>
                    <div class="row" style="padding-top: 30px;">
                        <div class="col-sm-4 col-sm-push-2">
                            <div class="form-group">
                                <input type="checkbox" name="recentes" value=1 class="checkbox-hidden" id="new">
                                <label href="#" class="btn btn-filter btn-block" for="new"><i class="fa fa-check"></i> Lançamentos</label>
                            </div>
                        </div>
                        <div class="col-sm-4 col-sm-push-2">
                            <div class="input-group select">
                                <span class="input-group-addon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                                <select class="form-control text-uppercase" id="estado-filtro">
                                    <option>Acre</option>
                                    <option>Alagoas</option>
                                    <option>Amapá</option>
                                    <option>Amazonas</option>
                                    <option>Bahia</option>
                                    <option>Ceará</option>
                                    <option>Distrito Federal</option>
                                    <option>Espírito Santo</option>
                                    <option>Goiás</option>
                                    <option>Maranhão</option>
                                    <option>Mato Grosso</option>
                                    <option>Mato Grosso do Sul</option>
                                    <option>Minas Gerais</option>
                                    <option>Pará</option>
                                    <option>Paraíba</option>
                                    <option>Paraná</option>
                                    <option>Pernambuco</option>
                                    <option>Piauí</option>
                                    <option selected>Rio de Janeiro</option>
                                    <option>Rio Grande do Norte</option>
                                    <option>Rio Grande do Sul</option>
                                    <option>Rondônia</option>
                                    <option>Roraima</option>
                                    <option>Santa Catarina</option>
                                    <option>São Paulo</option>
                                    <option>Sergipe</option>
                                    <option>Tocantins</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h3>Categorias</h3>
                    <div class="categorias">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_1" name="categoria[]" value="agencia-de-viagens" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_1">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/viagens.png">
                                            <img src="/eletros-convenio-html/img/categorias/viagens-white.png">
                                        </div>
                                        <p>Agência de Viajens</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_2" name="categoria[]" value="atividade-fisica" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_2">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/atividade-fisica.png">
                                            <img src="/eletros-convenio-html/img/categorias/atividade-fisica-white.png">
                                        </div>
                                        <p>Atividade Física</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_3" name="categoria[]" value="auto-escolas" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_3">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/auto-escolas.png">
                                            <img src="/eletros-convenio-html/img/categorias/auto-escolas-white.png">
                                        </div>
                                        <p>Auto escolas</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_4" name="categoria[]" value="automoveis" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_4">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/automoveis.png">
                                            <img src="/eletros-convenio-html/img/categorias/automoveis-white.png">
                                        </div>
                                        <p>Automóveis</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_5" name="categoria[]" type="checkbox" value="beleza-e-estetica" class="checkbox-hidden">
                                    <label for="cat_5">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/beleza-e-estetica.png">
                                            <img src="/eletros-convenio-html/img/categorias/beleza-e-estetica-white.png">
                                        </div>
                                        <p>Beleza e Estética</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_6" name="categoria[]" value="casa-e-decoracao" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_6">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/casa-e-decoracao.png">
                                            <img src="/eletros-convenio-html/img/categorias/casa-e-decoracao-white.png">
                                        </div>
                                        <p>Casa e Decoração</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_7" name="categoria[]" value="consulatoria" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_7">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/consultoria.png">
                                            <img src="/eletros-convenio-html/img/categorias/consultoria-white.png">
                                        </div>
                                        <p>Consultoria</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_8" name="categoria[]" value="curso-de-idiomas" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_8">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/curso-de-idiomas.png">
                                            <img src="/eletros-convenio-html/img/categorias/curso-de-idiomas-white.png">
                                        </div>
                                        <p>Curso de idiomas</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_9" name="categoria[]" value="drograrias-e-farmacias" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_9">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/drogarias-e-farmacia.png">
                                            <img src="/eletros-convenio-html/img/categorias/drogarias-e-farmacia-white.png">
                                        </div>
                                        <p>Drogarias e Farmácias</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_10" name="categoria[]" value="educacao" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_10">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/educacao.png">
                                            <img src="/eletros-convenio-html/img/categorias/educacao-white.png">
                                        </div>
                                        <p>Educação</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_11" name="categoria[]" value="fotografias" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_11">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/fotografias.png">
                                            <img src="/eletros-convenio-html/img/categorias/fotografias-white.png">
                                        </div>
                                        <p>Fotografias</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_12" name="categoria[]" value="funerarias" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_12">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/funerarias.png">
                                            <img src="/eletros-convenio-html/img/categorias/funerarias-white.png">
                                        </div>
                                        <p>Funerárias</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_13" name="categoria[]" value="livrarias-e-papelarias" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_13">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/livrarias-e-papelarias.png">
                                            <img src="/eletros-convenio-html/img/categorias/livrarias-e-papelarias-white.png">
                                        </div>
                                        <p>Livrarias e papelarias</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_14" name="categoria[]" value="locadoras" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_14">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/locadoras.png">
                                            <img src="/eletros-convenio-html/img/categorias/locadoras-white.png">
                                        </div>
                                        <p>Locadoras</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_15" name="categoria[]" value="mudancas-e-transportes" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_15">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/mudancas-e-transportes.png">
                                            <img src="/eletros-convenio-html/img/categorias/mudancas-e-transportes-white.png">
                                        </div>
                                        <p>Mudanças e Transportes</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_16" name="categoria[]" value="oticas" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_16">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/oticas.png">
                                            <img src="/eletros-convenio-html/img/categorias/oticas-white.png">
                                        </div>
                                        <p>óticas</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_17" name="categoria[]" value="petshop-e-veterinaria" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_17">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/petshop-e-veterinaria.png">
                                            <img src="/eletros-convenio-html/img/categorias/petshop-e-veterinaria-white.png">
                                        </div>
                                        <p>petShop e veterinária</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_18" name="categoria[]" value="pousadas-e-hoteis" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_18">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/pousadas-e-hoteis.png">
                                            <img src="/eletros-convenio-html/img/categorias/pousadas-e-hoteis-white.png">
                                        </div>
                                        <p>Pousadas e hotéis</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_19" name="categoria[]" value="psicologia" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_19">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/psicologia.png">
                                            <img src="/eletros-convenio-html/img/categorias/psicologia-white.png">
                                        </div>
                                        <p>Psicologia</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_20" name="categoria[]" value="restaurantes" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_20">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/restaurantes.png">
                                            <img src="/eletros-convenio-html/img/categorias/restaurantes-white.png">
                                        </div>
                                        <p>Restaurantes</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_21" name="categoria[]" value="seguros" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_21">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/seguros.png">
                                            <img src="/eletros-convenio-html/img/categorias/seguros-white.png">
                                        </div>
                                        <p>Seguros</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_22" name="categoria[]" value="servicos-de-fisioterapia" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_22">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/fisioterapia.png">
                                            <img src="/eletros-convenio-html/img/categorias/fisioterapia-white.png">
                                        </div>
                                        <p>Fisioterapia</p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="box">
                                    <input id="cat_23" name="categoria[]" value="vestuarioacessorios" type="checkbox" class="checkbox-hidden">
                                    <label for="cat_23">
                                        <div class="icon">
                                            <img src="/eletros-convenio-html/img/categorias/vestuario-acessorios.png">
                                            <img src="/eletros-convenio-html/img/categorias/vestuario-acessorios-white.png">
                                        </div>
                                        <p>Vestuário / Acessórios</p>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4 col-sm-push-4">
                                <a href="#" class="btn btn-gray-1 btn-block text-uppercase" id="btn-filtro-lista">Filtrar lista</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<div id="suggestion" class="modal fade suggestion" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-body">
              <a href="#" class="close-modal" data-dismiss="modal"><i class="fa fa-window-close"></i></a>
              <h4>Sugerir uma empresa</h4>
              <form id="form-sugestao" class="form-sugestao" data-parsley-validate> 
                <div class="form-group">
                  <input type="text" class="form-control" id="nome-sugestao" placeholder="Nome"
                         required data-parsley-required-message="Informe seu nome">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="numero-sugestao" placeholder="Nº Eletros"
                         required data-parsley-required-message="Informe seu Número Eletros">
                </div>
                <h5>Dados da Empresa</h5>
                <div class="form-group">
                  <input type="text" class="form-control" id="empresa-sugestao" placeholder="Nome"
                        required data-parsley-required-message="Informe a Empresa">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control phone" id="telefone-sugestao" placeholder="Telefone" maxlength="20">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" id="email-sugestao" placeholder="E-mail">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="site-sugestao" placeholder="Site">
                </div>
                <button type="submit" class="btn btn-orange btn-md flat text-uppercase btn-block">Enviar Dados</button>
              </form>
            </div>
          </div>

        </div>
      </div>
<?php get_footer(); ?>

