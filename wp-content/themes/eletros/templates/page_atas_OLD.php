<!--?php /* Template name: Template Atas */ ?-->
<?php if(wp_get_current_user()->roles[0] == 'administrator' || wp_get_current_user()->roles[0] == 'subscriber'): ?>
<?php 
get_header(); get_template_part('partials/content','top'); 

?>
  <section class="bg-eeeeee">
        <div>
           <?php eletros_breadcrumbs(); ?>
            <div style="padding-bottom:50px;"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-3"> 
                    <a href="/conselhos/" class="btn btn-default text-uppercase" style="margin-bottom: 20px;"><i class="fa fa-chevron-left" aria-hidden="true" style="vertical-align: middle;"></i> Voltar</a>
                </div>
            </div>
            <div class="row tabs tab-vertical">
                <div class="col-sm-3 tab-col-left">
                    <ul>
                       <?php  $c = $_GET['conselho'];  $args = array('post_type'=>'conselhos'); query_posts($args); while (have_posts()): the_post(); ?>
                        <li class="<?php echo $c == $post->post_name ? 'active' : null; ?>"><a href="<?php bloginfo('url'); ?>/atas?conselho=<?php echo $post->post_name; ?>"><?php the_title(); ?></a></li>                       
                    <?php endwhile;  ?>
                    </ul>
                </div>
                <div class="col-sm-9 tab-col-right font-zoom">
                    <div class="tab-content">

                        <?php while (have_posts()): the_post(); ?>
                            <div id="tab1" class="tab-pane fade <?php echo $c == $post->post_name ? 'in active' : null; ?> ">
                                <div class="area-conselho">
                                    <h2>
                                    <?php
                                    if ($_GET['conselho'] == 'conselho-deliberativo' && $_GET['ano-especifico'] == '2018') {
                                        echo 'Reuniões';
                                    } else {
                                        //echo 'Atas de reuniões';
                                        echo 'Reuniões';
                                    }
                                    ?>
                                    // <?php the_title(); ?>
                                    </h2>
                                    <?php get_template_part('partials/content','buscaata'); ?>
                                    <div id="b<?php echo $post->ID; ?>" class="row">
                                        <div class="col-sm-12">
                                            <h3>
                                            <?php
                                            if ($_GET['conselho'] == 'conselho-deliberativo' && $_GET['ano-especifico'] == '2018') {
                                                echo 'Reunião 337ª do CDE - 18/01/2018';
                                            } else {
                                                echo 'Reuniões';
                                            }
                                            ?>
                                            </h3>

                                            <?php 
                                            // Anos
                                            $anos = CFS()->get( 'anos' );
                                            if ( count( $anos ) ) {
                                            ?>

                                            <div class="panel-group reunioes" id="accordion" style="margin-bottom: 0;">
                                                <?php $i_ano = 0; foreach ( $anos as $ano ) { ?>
                                                <div class="box-conselho">
                                                    <div class="box-header">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse_ano_<?php echo $i_ano; ?>">
                                                            <?php
                                                            foreach ( $ano['ano'] as $key => $label ) {
                                                                echo $label; //ano
                                                            }
                                                            ?>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse_ano_<?php echo $i_ano; ?>" class="panel-collapse collapse">
                                                        <div class="box-body">

                                                            <?php if ( count( $ano['meses'] ) ) { ?>
                                                                <div class="panel-group" id="accordion_mes_<?php echo $i_ano; ?>">  
                                                                <?php $i_mes = 0; foreach ( $ano['meses'] as $meses ) { ?>  
                                                                    <?php foreach ( $meses['mes'] as $key => $label ) { ?>  

                                                                        
                                                                        <div class="box-conselho">
                                                                            <div class="box-header">
                                                                                <h4 class="panel-title">
                                                                                    <a data-toggle="collapse" data-parent="#accordion_mes_<?php echo $i_ano; ?>" href="#collapse_mes_<?php echo $i_ano."_".$i_mes; ?>">
                                                                                        <?php echo $label; //mes ?>
                                                                                    </a>
                                                                                </h4>
                                                                            </div>
                                                                            <div id="collapse_mes_<?php echo $i_ano."_".$i_mes; ?>" class="panel-collapse collapse">
                                                                                <div class="box-body">

                                                                                    <?php if ( count( $meses['reunioes'] ) ) { ?> 
                                                                                    <div class="panel-group" id="accordion_reuniao_<?php echo $i_ano."_".$i_mes; ?>">
                                                                                        <?php $i_reuniao = 0; foreach ( $meses['reunioes'] as $reunioes ) { ?>
                                                                                        <div class="box-conselho">
                                                                                            <div class="box-header">
                                                                                                <h4 class="panel-title">
                                                                                                    <a data-toggle="collapse" data-parent="#accordion_reuniao_<?php echo $i_ano."_".$i_mes; ?>" href="#collapse_reuniao_<?php echo $i_ano."_".$i_mes."_".$i_reuniao; ?>">
                                                                                                        <?php echo $reunioes['reuniao']; //reuniao ?> - 
                                                                                                        <small><?php echo $reunioes['data']; //data?></small>
                                                                                                    </a>
                                                                                                </h4>
                                                                                            </div>
                                                                                            <div id="collapse_reuniao_<?php echo $i_ano."_".$i_mes."_".$i_reuniao; ?>" class="panel-collapse collapse">
                                                                                                <div class="box-body">

                                                                                                    <ul class="atas">
                                                                                                        <li>
                                                                                                            <a href="<?php echo $reunioes['ata'];?>">
                                                                                                                Ata - <?php echo $reunioes['titulo_da_ata']; //titulo_da_ata?>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="<?php echo $reunioes['pauta'];?>">
                                                                                                                Pauta - <?php echo $reunioes['titulo_da_pauta']; //titulo_da_pauta?>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                    </ul>

                                                                                                    <?php if ( count( $reunioes['materiais_complementares'] ) ) { ?>
                                                                                                    <div class="panel-group" id="accordion_materiais_<?php echo $i_ano."_".$i_mes."_".$i_reuniao; ?>">
                                                                                                        <div class="box-conselho">
                                                                                                            <div class="box-header">
                                                                                                                <h4 class="panel-title">
                                                                                                                    <a data-toggle="collapse" data-parent="#accordion_materiais_<?php echo $i_ano."_".$i_mes."_".$i_reuniao; ?>" href="#collapse_material_<?php echo $i_ano."_".$i_mes."_".$i_reuniao; ?>">
                                                                                                                    Materiais Complementáres
                                                                                                                    </a>
                                                                                                                </h4>
                                                                                                            </div>
                                                                                                            <div id="collapse_material_<?php echo $i_ano."_".$i_mes."_".$i_reuniao; ?>" class="panel-collapse collapse">
                                                                                                                <div class="box-body">
                                                                                                                    <ul class="atas">
                                                                                                                    <?php foreach ( $reunioes['materiais_complementares'] as $materiais_complementares ) { ?>  
                                                                                                                        <li>
                                                                                                                            <a href="<?php echo $materiais_complementares['material_complementar'];?>" download>
                                                                                                                                <?php echo $materiais_complementares['titulo_do_material_complementar']; //titulo_da_pauta?>
                                                                                                                            </a>
                                                                                                                        </li>
                                                                                                                    <?php } ?>
                                                                                                                    </ul>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div> 
                                                                                                    <?php } ?>

                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <?php $i_reuniao++; } ?> 
                                                                                    </div> 
                                                                                    <?php } ?> 

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        

                                                                    <?php } ?>  
                                                                <?php $i_mes++; } ?>  
                                                                </div> 
                                                            <?php } ?>  

                                                        </div>
                                                    </div>
                                                </div>
                                                <?php $i_ano++; } ?>








                                                <!-- atas antigas -->
                                                <?php 
                                                $atas = get_field('atas'); 
                                                if(!empty($atas)): 

                                                    // Testa se é preciso filtrar
                                                    if (!empty($_GET['f'])) {
                                                        $ano_atual = date('Y');

                                                        // monta um array com os IDs selecionados no "conselho"
                                                        $atas_ids = array();
                                                        foreach ($atas as $key => $ata) {
                                                            $atas_ids[] = $ata->ID;
                                                        }

                                                        // argumentos padrão, usados em todos os tipos de filtro
                                                        $args = array(
                                                            'post_type' => 'atas',
                                                            'posts_per_page' => -1,
                                                            'order' => 'DESC',
                                                            'orderby' => 'meta_value',
                                                            'meta_type' => 'DATE',
                                                            'meta_key' => 'data_emissao',
                                                            'post__in' => $atas_ids, // só busca nas atas selecionadas
                                                            'meta_date_format' => '%d/%m/%Y', // esse filtro NÃO é padrão WP, foi criado no arquivo functions.php
                                                        );

                                                        // Configura os argumentos de acordo com o filtro selecionado
                                                        switch ($_GET['f']) {
                                                            case '1': // ano corrente
                                                            case '2': // ano específico
                                                                $ano_from = $ano_to = ('1' == $_GET['f']) ? $ano_atual : absint($_GET['ano-especifico']);
                                                                $mes_from = '01';
                                                                $mes_to = '12';
                                                                break;

                                                            case '3':
                                                                $mes_from = absint($_GET['mes_from']);
                                                                $ano_from = absint($_GET['ano_from']);
                                                                $mes_to = absint($_GET['mes_to']);
                                                                $ano_to = absint($_GET['ano_to']);
                                                                break;
                                                        }
                                                        $args['meta_query'] = array(
                                                            array(
                                                                'key'     => 'data_emissao',
                                                                'value'   => $ano_from.'-'.$mes_from.'-01',
                                                                'type'    => 'DATE',
                                                                'compare' => '>=',
                                                            ),
                                                            array(
                                                                'key'     => 'data_emissao',
                                                                'value'   => $ano_to.'-'.$mes_to.'-31',
                                                                'type'    => 'DATE',
                                                                'compare' => '<=',
                                                            ),
                                                        );
                                                        $atas_query = new WP_Query( $args );
                                                        $atas = (array) $atas_query->posts;
                                                    }
                                                    ?>



                                                        <?php
                                                        $_y = 0;
                                                        foreach ($atas as $ata):
                                                            $date = explode('/', get_field('data_emissao', $ata->ID));
                                                            $y = (int)$date[2];
                                                            $m = (int)$date[1];
                                                            $d = (int)$date[0];
                                                        ?>


                                                        <?php
                                                            if ($y != $_y) {
                                                                echo "</ul></div> </div> </div>";
                                                        ?>

                                                            <div class="box-conselho" style="margin-bottom: 5px;">
                                                                <div class="box-header">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse_ano_old_<?php echo $y; ?>">
                                                                            <?php echo $y; ?>
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapse_ano_old_<?php echo $y; ?>" class="panel-collapse collapse">
                                                                    <div class="box-body">
                                                                        <ul class="atas">

                                                        <?php 
                                                                $_y = $y;
                                                            }
                                                        ?>

                                                                        <li>
                                                                        <?php $arq = get_field('arquivo', $ata->ID); ?>
                                                                            <a href="<?php echo $arq['url']; ?>" download><?php echo $ata->post_title; ?> - <span>[ <?php echo get_field('data_emissao', $ata->ID); ?> ]</span></a>
                                                                        </li>

                                                            
                                                                
                                                           

                                                        <?php endforeach; ?>

                                                        </ul></div> </div> </div>
                                                    <?php 
                                                endif; 
                                            ?>
                                            </div>

                                            <?php } ?>



                                            

                                        </div>
                                    </div>
                                </div>
                            </div>
                       <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script>
    $(document).ready(function(){
        $('.item-home a').text('Conselhos').prop('href','/conselhos');
    });
</script>
    <!-- /content -->
<?php get_footer(); else: wp_redirect(home_url()); exit(); endif; ?>