<!--?php /* Template name: Template para usuários */ ?-->
<?php  if ( is_user_logged_in() ): wp_redirect('/conselho'); endif; get_header(); the_post(); $p = get_post($post->ID); ?>

<!-- topo -->
<section>
    <div class="topo" style="background-image: url(<?php bloginfo('template_url'); ?>/img/bg-topo.png);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 content">
                    <div class="text">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /topo -->

<!-- content -->
<section>
 <?php eletros_breadcrumbs(); ?>
    <div style="padding-bottom:50px;"></div>
    <div class="<?php echo $p->post_name;  ?>">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                  <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>   
</section>
<!-- /content -->

<?php get_footer(); ?>