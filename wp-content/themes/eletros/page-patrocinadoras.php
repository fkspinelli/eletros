<?php get_header(); the_post(); $p = get_post($post->ID); get_template_part('partials/content','top'); ?>

<!-- content -->
<section>
 <?php eletros_breadcrumbs(); ?>
    <div style="padding-bottom:50px;"></div>
    <div class="<?php echo $p->post_name;  ?>">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                  <?php the_content(); ?>
                </div>
            </div>
            <div style="margin-bottom:50px;"></div>          
            <div class="row row-eq-height colh">
            <?php query_posts('cat=23&posts_per_page=-1'); while(have_posts()): the_post(); ?>
                <div class="col-sm-4 colh">
                    <div class="box-patrocinadoras">
                        <div class="header">
                           <?php get_the_image(['size'=>'normal', 'link_to_post'=>false]); ?>
                        </div>
                        <div class="content">
                           <?php the_content(); ?>
                        </div>
                    </div>
                </div>
             <?php endwhile; ?>  
            </div>
            <div style="margin-bottom:30px;"></div>
        </div>
    </div>   
</section>
<!-- /content -->

<?php get_footer(); ?>