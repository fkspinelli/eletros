<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Eletros <?php wp_title(); ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="image/x-icon" title="Eletros" rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.png">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/fonts/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/beauty-font.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/fotorama.css" />
      <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/slick/slick.css"/>
    
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/slick/slick.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/parsley.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/bootbox.min.js"></script>

    <script src="<?php bloginfo('template_url'); ?>/js/jquery-ui.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.bxslider/jquery.bxslider.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/fotorama.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.mask.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.blockUI.min.js"></script>
    
    <?php wp_head(); ?>
    
    
    <style>html {margin-top: 0 !important;}</style>

</head>

<body>
    <!-- header -->
    <header>
        <iframe id="area-participante" src="https://eletros.participante.com.br/login/"></iframe>
        <div class="header-bottom">
            <div class="container">
                <nav>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="left">
                                <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.png" class="img-responsive logo"></a>
                            </div>
                            <div class="right">
                                <div class="menu-a">
                                    <i class="fa fa-bars"></i>
                                    <?php wp_nav_menu( array('menu' => 'menu1', 'container'=>'ul') ); ?>
                                    <div class="form-group">
                                        <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
                                        <input type="search"  value="<?php echo get_search_query(); ?>" name="s" id="s" class="form-control input-lg" placeholder="Busca">
                                        <i class="icon icon-basic-magnifier"></i>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php wp_nav_menu( array('menu' => 'menu2', 'container'=>'ul', 'menu_class'=>'menu-b') ); ?>
<!--                     <ul class="menu-b">
                        <li>
                            <a href="#">A Eletros</a>
                        </li>
                        <li>
                            <a href="#">Previdência</a>
                            <ul>
                                <li><a href="#">Plano BD Eletrobrás</a></li>
                                <li><a href="#">Plano CD Eletrobrás</a></li>
                                <li><a href="#">Plano CD CERON</a></li>
                                <li><a href="#">Plano CV ONS</a></li>
                                <li><a href="#">Plano CV EPE</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Investimentos</a>
                        </li>
                        <li>
                            <a href="#">Programas</a>
                        </li>
                    </ul> -->
                </nav>
            </div>
        </div>
    </header>
    <!-- /header -->
