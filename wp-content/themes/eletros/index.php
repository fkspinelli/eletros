<?php get_header(); ?>

    <!-- banner -->
    <?php include('partials/banner_home.php'); ?>
    <!-- /banner -->

    <!-- content -->
    <section>
        <div class="bg-eeeeee">
            <div class="breadcrumb">
                <div class="container">
                    <i class="icon icon-circle-slelected"></i>
                    <ul>
                        <li><a>Página inicial</a></li>
                    </ul>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        
                        <?php include('widgets/destaque.php'); ?>
                        <?php include('widgets/ultimas_noticias.php'); ?>

                        
                    </div>
                    <div class="col-sm-4">
                       <?php get_template_part('partials/content','programas'); ?>
                    </div>
                </div>
                <div style="margin-bottom:50px;"></div>
            </div>
        </div>
        <div class="galeria-de-midia">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>Galeria de Mídia</h2>
                        <div class="slide">
                            <ul class="bxslider">
                                <?php query_posts('cat=22'); while(have_posts()): the_post(); ?>
                                    <li><a href="<?php bloginfo('url'); ?>/midias"><?php get_the_image(['size'=>'peq','link_to_post'=>false]); ?></a></li>
                               <?php endwhile; wp_reset_query(); ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="patrocinadoras patrocinadoras-home">
            <div class="container">
                <div style="margin-top:30px;"></div>
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="patrocinadoras-home">Patrocinadoras</h2>
                        <div class="box-patrocinadoras">
                            <?php query_posts('cat=23'); while(have_posts()): the_post(); ?>
                                <a href="<?php echo get_field('link_externo'); ?>" target="_blank">
                                    <?php get_the_image(['size'=>'thumb', 'image_class'=>'hover','link_to_post'=>false]); ?>
                                </a>
                            <?php endwhile; wp_reset_query(); ?>
                           
                        </div>
                    </div>
                    <div class="col-sm-4">
                     <div class="programas">
                       
                             <div class="box-programas-1">
                                <ul>
                                    <li>
                                        <a href="/seja-uma-patrocinadora">
                                            <img style="max-height: 118px;" src="https://eletros.com.br/wp-content/uploads/2016/09/banner_seja_patrocinadora-1.png" class="img-responsive hover">
                                        </a>
                                    </li>
                                   
                                </ul>
                            </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /content -->
<?php get_footer(); ?>