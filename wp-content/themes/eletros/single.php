<?php get_header(); the_post(); 
    // definições
    $image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
    $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),'normal' );


    
     ?>
  
    <section class="bg-eeeeee">
        <div>
            <div class="breadcrumb">
                <div class="container">
                    <i class="icon icon-circle-slelected"></i>
                    <ul>
                        <li><a href="#">Página inicial</a></li>
                        <li><a href="#">Notícias</a></li>
                        <li><a>INformes de REndimentos...</a></li>
                    </ul>
                </div>
            </div>
            <div style="padding-bottom:50px;"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 detalhes-noticia">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="tool-bar">
                                <div class="pull-left">
                                    <a href="#" target="_blank"><i class="icon icon-social-facebook"></i></a>
                                    <a href="#" target="_blank"><i class="icon icon-social-twitter"></i></a>
                                </div>
                                <div class="pull-right">
                                    <a href="#" class="more-font">+A</a>
                                    <a href="#" class="less-font">-A</a>
                                    <a href="#" class="print-page"><img src="<?php bloginfo('template_url'); ?>/img/icon/print.png"></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="label financas">finanças</div>
                            <div class="data-noticia"><?php echo get_the_date(); ?></div>
                        </div>
                    </div>
                    <div class="row font-zoom">
                        <div class="col-sm-12">
                            <h1><?php the_title(); ?></h1>

                            <img src="<?php echo $image_url ?>">

                            <p>O que temos que ter sempre em mente é que o aumento do diálogo entre os diferentes setores produtivos promove a alavancagem do fluxo de informações. O incentivo ao avanço tecnológico, assim como a mobilidade dos capitais internacionais auxilia a preparação e a composição dos níveis de motivação departamental.</p>
                            <p>No entanto, não podemos esquecer que o acompanhamento das preferências de consumo cumpre um papel essencial na formulação dos paradigmas corporativos.</p>
                            <p>Percebemos, cada vez mais, que o desenvolvimento contínuo de distintas formas de atuação oferece uma interessante oportunidade para verificação do remanejamento dos quadros funcionais. Neste sentido, a mobilidade dos capitais internacionais causa impacto indireto na reavaliação de todos os recursos funcionais envolvidos. A certificação de metodologias que nos auxiliam a lidar com a necessidade de renovação processual representa uma abertura para a melhoria das condições financeiras e administrativas exigidas.</p>
                            
                            <iframe src="https://www.youtube.com/embed/C-xxSobIyVM" frameborder="0" allowfullscreen></iframe>

                            <p>O cuidado em identificar pontos críticos na necessidade de renovação processual prepara-nos para enfrentar situações atípicas decorrentes do sistema de participação geral.</p>





                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="diver"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-ultimas-noticias">
                   <?php include('widgets/main_sidebar.php');?>
                </div>
            </div>
        </div>
    </section>
    <!-- /content -->
<?php get_footer(); ?>