<?php get_header(); the_post(); get_template_part('partials/content','top'); ?>
<section class="bg-eeeeee">
<div>
  	<?php eletros_breadcrumbs(); ?>

    <div style="padding-bottom:50px;"></div>
</div>
<div class="container">
    
    <?php get_template_part('partials/content', 'social'); ?>

    <div class="row tabs tab-vertical">
        <div class="col-sm-3 tab-col-left">
            <ul>
            	<?php $args = array(
                	'post_type' => 'planos',
                	'posts_per_page' => -1,
                	'post_status' => 'publish',
                	'order' => 'ASC',
                	'post_parent' => $post->ID
                );
                 query_posts($args); $cont = 0;
                 while(have_posts()): the_post(); $cont++; ?>
                    <li class="<?php echo $cont == '1'? 'active' : null; ?>"><a data-toggle="pill" href="#tab_<?php echo $post->ID; ?>"><?php the_title(); ?></a></li>
                <?php endwhile; wp_reset_query(); ?>
            </ul>
            <div class="diver"></div>
            <div class="files">
               <?php the_content(); ?>
            </div>
        </div>
        <div class="col-sm-9 tab-col-right font-zoom">
            <div class="tab-content">
            <?php  query_posts($args); $cont_2 = 0; while(have_posts()): the_post(); $cont_2++; ?>
                <div id="tab_<?php echo $post->ID; ?>" class="tab-pane fade in <?php echo $cont_2 == '1'? 'active' : null; ?>">
                <h3><?php the_title(); ?></h3>
                 	<?php the_content(); ?>
                </div>
            <?php endwhile; wp_reset_query(); ?>
            </div>
        </div>
    </div>
</div>
</section>
<!-- // content -->
<?php get_footer(); ?>