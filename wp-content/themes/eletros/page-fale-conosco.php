<?php get_header(); the_post(); get_template_part('partials/content','top'); ?>

<section class="bg-eeeeee">
    <div>
       <?php eletros_breadcrumbs(); ?>
        <div style="padding-bottom:50px;"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Canais de Atendimento</h2>
            </div>
        </div>
        <div class="row fale-conosco">
            <div class="col-sm-6">
              <?php the_content(); ?>

            <div style="margin-bottom:30px;"></div>
            </div>
            <div class="col-sm-6">
            <?php echo do_shortcode( '[contact-form-7 id=123 html_class=form-horizontal]' ); ?>

            </div>
        </div>
    </div>
</section>
<!-- /content -->
<?php get_footer(); ?>