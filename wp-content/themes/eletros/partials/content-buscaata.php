<?php 
    $ano_mais_antigo = $ano_atual = date( 'Y' );
    $ata_mais_antiga = get_posts( array(
        'post_type' => 'atas',
        'posts_per_page' => 1,
        'order' => 'ASC',
        'orderby' => 'meta_value',
        'meta_type' => 'DATE',
        'meta_key' => 'data_emissao',
    ) );
    if ( count( $ata_mais_antiga ) ) {
        $data_mais_antiga = explode( '/', get_field( 'data_emissao', $ata_mais_antiga[0]->ID, false ) );
        $ano_mais_antigo = $data_mais_antiga[2];
    }
?><form method="get" action="<?php bloginfo('url'); ?>/atas">
<input type="hidden" name="conselho" value="<?php echo $post->post_name; ?>">
    <div class="fix">
        <div class="radio">
            <input id="radio_1_<?php echo $post->ID; ?>" type="radio" value="1" name="f" <?php checked( $_GET['f'], '1' ); ?>>
            <label for="radio_1_<?php echo $post->ID; ?>"></label>
            <span>Reuniões do Ano Corrente</span>
        </div>
    </div>
    <div class="fix">
        <div class="radio pull-left">
            <input id="radio_2_<?php echo $post->ID; ?>" type="radio" value="2" name="f" <?php checked( $_GET['f'], '2' ); ?>>
            <label for="radio_2_<?php echo $post->ID; ?>"></label>
            <span>Reuniões de um ano específico</span>
        </div>
        <select class="form-control ano pull-left" name="ano-especifico">
            <option>Ano</option>
            <?php for ( $ano = $ano_atual; $ano >= $ano_mais_antigo; $ano-- ) { ?>
                <option value="<?php echo $ano ?>" <?php selected( $_GET['ano-especifico'], $ano ); ?>><?php echo $ano ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="fix">
        <div class="radio">
            <input id="radio_3__<?php echo $post->ID; ?>" type="radio" value="3" name="f" <?php checked( $_GET['f'], '3' ); ?>>
            <label for="radio_3__<?php echo $post->ID; ?>"></label>
            <span>REuniões de um período específico</span>
        </div>
    </div>
    <div class="row periodo">
        <div class="col-sm-3">
            <div class="form-group">
                <label>De:</label>
                <select name="mes_from" class="form-control">
                    <option value="">Mês</option>
                    <option value="1" <?php selected( $_GET['mes_from'], '1' ); ?>>Janeiro</option>
                    <option value="2" <?php selected( $_GET['mes_from'], '2' ); ?>>Fevereiro</option>
                    <option value="3" <?php selected( $_GET['mes_from'], '3' ); ?>>Março</option>
                    <option value="4" <?php selected( $_GET['mes_from'], '4' ); ?>>Abril</option>
                    <option value="5" <?php selected( $_GET['mes_from'], '5' ); ?>>Maio</option>
                    <option value="6" <?php selected( $_GET['mes_from'], '6' ); ?>>Junho</option>
                    <option value="7" <?php selected( $_GET['mes_from'], '7' ); ?>>Julho</option>
                    <option value="8" <?php selected( $_GET['mes_from'], '8' ); ?>>Agosto</option>
                    <option value="9" <?php selected( $_GET['mes_from'], '9' ); ?>>Setembro</option>
                    <option value="10" <?php selected( $_GET['mes_from'], '10' ); ?>>Outubro</option>
                    <option value="11" <?php selected( $_GET['mes_from'], '11' ); ?>>Novembro</option>
                    <option value="12" <?php selected( $_GET['mes_from'], '12' ); ?>>Dezembro</option>
                </select>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label>&nbsp;</label>
                <select class="form-control" name="ano_from">
                    <option>Ano</option>
                    <?php for ( $ano = $ano_atual; $ano >= $ano_mais_antigo; $ano-- ) { ?>
                        <option value="<?php echo $ano ?>" <?php selected( $_GET['ano_from'], $ano ); ?>><?php echo $ano ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label>Até:</label>
                <select name="mes_to" class="form-control">
                    <option value="">Mês</option>
                    <option value="1" <?php selected( $_GET['mes_to'], '1' ); ?>>Janeiro</option>
                    <option value="2" <?php selected( $_GET['mes_to'], '2' ); ?>>Fevereiro</option>
                    <option value="3" <?php selected( $_GET['mes_to'], '3' ); ?>>Março</option>
                    <option value="4" <?php selected( $_GET['mes_to'], '4' ); ?>>Abril</option>
                    <option value="5" <?php selected( $_GET['mes_to'], '5' ); ?>>Maio</option>
                    <option value="6" <?php selected( $_GET['mes_to'], '6' ); ?>>Junho</option>
                    <option value="7" <?php selected( $_GET['mes_to'], '7' ); ?>>Julho</option>
                    <option value="8" <?php selected( $_GET['mes_to'], '8' ); ?>>Agosto</option>
                    <option value="9" <?php selected( $_GET['mes_to'], '9' ); ?>>Setembro</option>
                    <option value="10" <?php selected( $_GET['mes_to'], '10' ); ?>>Outubro</option>
                    <option value="11" <?php selected( $_GET['mes_to'], '11' ); ?>>Novembro</option>
                    <option value="12" <?php selected( $_GET['mes_to'], '12' ); ?>>Dezembro</option>
                </select>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label>&nbsp;</label>
                <select class="form-control" name="ano_to">
                    <option>Ano</option>
                    <?php for ( $ano = $ano_atual; $ano >= $ano_mais_antigo; $ano-- ) { ?>
                        <option value="<?php echo $ano ?>" <?php selected( $_GET['ano_to'], $ano ); ?>><?php echo $ano ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <button type="submit" class="btn btn-blue-2 btn-block text-uppercase">Nova Consulta</button>
        </div>
    </div>
    <hr>
</form>