<!-- topo -->
<section>
    <div class="topo" style="background-image: url(<?php bloginfo('template_url'); ?>/img/bg-topo.png);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 content">
                    <div class="text">
                        <h1><?php if(is_category()): echo get_the_category()[0]->name; elseif(is_post_type_archive()): post_type_archive_title(); elseif(is_search()): echo 'Busca'; elseif(is_singular('noticias')): echo 'Notícias'; else: the_title(); endif; ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /topo -->