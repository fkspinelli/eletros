<div class="row">
    <div class="col-sm-12">
        <div class="tool-bar">
            <div class="pull-left">
               <a class="js-social-share" href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>"><i class="icon icon-social-facebook"></i></a>
                <a class="js-social-share" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>"><i class="icon icon-social-twitter"></i></a>
            </div>
            <div class="pull-right">
                <a href="#" class="more-font">+A</a>
                <a href="#" class="less-font">-A</a>
                <a href="#" class="print-page"><img src="<?php bloginfo('template_url'); ?>/img/icon/print.png"></a>
            </div>
        </div>
    </div>
</div>
