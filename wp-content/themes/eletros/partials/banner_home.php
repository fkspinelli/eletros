<section class="banner-home">
        <div id="bannerHome" class="carousel slide" data-interval="20000" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <?php
                $count = 0;
                $type = 'type_banner';
                $args=array('post_type' => $type, 'post_status' => 'publish', 'posts_per_page' => -1, 'caller_get_posts'=> 1, 'orderby' => 'data', 'order'=>'desc');
                $banner_query = null;
                $banner_query = new WP_Query($args);
                ?>

                <?php
                if( $banner_query->have_posts() ) {
                    while ($banner_query->have_posts()) : $banner_query->the_post();?>
                        <a href="<?php echo get_field('link'); ?>"  class="item <?php echo ($count==0) ? 'active':'' ?> " style="background-image:url(<?php echo wp_get_attachment_url( get_post_thumbnail_id( $banner_query->ID ) ); ?>);">
                            <div class="container">
                                <div class="row img-banner-mobile">
                                    <div class="col-sm-12">
                                        <img src="<?php echo get_field('imagem_mobile', $banner_query->ID)['url'] ?>" class="img">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="box-text">
                                            <div class="box-text-content">
                                                <p><?php the_title(); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                <?php 
                    $count++;
                    endwhile; 
                    wp_reset_query(); 
                } 
                ?>
            </div>
            <div class="container">
                <ol class="carousel-indicators">
                <?php
                if( $banner_query->have_posts() ) {
                    $count = 0;
                    while ($banner_query->have_posts()) : $banner_query->the_post();?>
                        <li data-target="#bannerHome" data-slide-to="<?php echo $count; ?>" class="<?php echo ($count==0) ? 'active':'' ?>"></li>
                <?php 
                    $count++;
                    endwhile; 
                    wp_reset_query(); 
                } 
                ?>
                </ol>
            </div>
        </div>
    </section>