 <div class="programas">
                            <h2>Programas</h2>
                            <div class="box-programas-1">
                                <ul>
                                    <li>
                                        <a href="http://cultivar.eletros.com.br/" target="_blank">
                                            <img src="<?php bloginfo('template_url'); ?>/img/programas/cultivar-horizontal.png" class="img-responsive hover">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://eletros.infobase.com.br/" target="_blank">
                                            <img src="<?php bloginfo('template_url'); ?>/img/programas/espaco-eletros.png" class="img-responsive hover">
                                        </a>
                                    </li>
                                    <li class="text-center">
                                        <a href="/pro-equidade-de-genero-e-raca/" style="display: inline-block;">
                                            <img src="<?php bloginfo('template_url'); ?>/img/programas/pro-equidade.png" class="img-responsive hover">
                                        </a>
                                        <div style="width:60px;display: inline-block;"></div>
                                        <a href="/eletros-convenio/" style="display: inline-block;">
                                            <img src="<?php bloginfo('template_url'); ?>/img/programas/eletros.png" class="img-responsive hover">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="box-programas-2">
                                <ul>
                                    <li>
                                        <a href="http://www.eletrossaude.com.br/" target="_blank">
                                            <img src="<?php bloginfo('template_url'); ?>/img/programas/logo-eletros-saudes.jpg" class="img-responsive hover">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/fabes">
                                            <img src="<?php bloginfo('template_url'); ?>/img/programas/fabes.png" class="img-responsive hover" style="width:200px;">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>