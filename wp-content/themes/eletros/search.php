<?php get_header();
global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

if( strlen($query_string) > 0 ) {
	foreach($query_args as $key => $string) {
		$query_split = explode("=", $string);
		$search_query[$query_split[0]] = urldecode($query_split[1]);
	} // foreach
} //if

$search = new WP_Query($search_query);

get_template_part('partials/content','top');
?>

<section class="bg-eeeeee">
    <div>
        <div class="breadcrumb">
            <div class="container">
                <i class="icon icon-circle-slelected"></i>
                <ul>
                    <li><a href="#">Página inicial</a></li>
                    <li><a>busca</a></li>
                </ul>
            </div>
        </div>
        <div style="padding-bottom:30px;"></div>
    </div>
    <div class="container resultado-busca">
        <div class="row">
            <div class="col-sm-12">
                <h2>Você buscou por <b>"<?php the_search_query(); ?>"</b>. Foram encontrados <?php echo $wp_query->found_posts; ?> resultados.</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form class="form-horizontal" role="form">
                    <div class="form-group input-search">
                        <label class="control-label col-sm-9 text-uppercase" for="filter">Filtrar por:</label>
                        <div class="col-sm-3">
                            <select class="form-control" id="filter">
                                <option>Todos</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

            	<?php if($wp_query->found_posts > 0):  
            	 ?>
                <h3>Resultados/</h3>
                <ul>
                <?php while ( have_posts() ) : the_post(); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <h4><?php the_title(); ?></h4>
                            <p><?php the_excerpt(); ?></p>
                            <span>LEIA MAIS <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></span>
                        </a>
                    </li>
                 <?php endwhile; ?>
                </ul>

            <?php endif;?>
               
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="pull-right" style="margin-top:-20px;">
                    <p class="pull-left" style="margin-top:6px;">Foram encontrados <?php echo $wp_query->found_posts; ?> resultados </p>
                   <?php  the_posts_pagination( array( 'mid_size'  => 2 ) ); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /content -->
<?php get_footer();?>
