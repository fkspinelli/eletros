<?php if(wp_get_current_user()->roles[0] == 'administrator' || wp_get_current_user()->roles[0] == 'subscriber'): ?>
<?php get_header(); the_post(); get_template_part('partials/content','top'); ?>
<section class="bg-eeeeee">
    <div>
        <?php eletros_breadcrumbs(); ?>
        <div style="padding-bottom:50px;"></div>
        
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-3"> 
                <a href="/conselhos/" class="btn btn-default text-uppercase" style="margin-bottom: 20px;"><i class="fa fa-chevron-left" aria-hidden="true" style="vertical-align: middle;"></i> Voltar</a>
            </div>
        </div>
        <div class="row tabs tab-vertical">
            <div class="col-sm-3 tab-col-left">
                <ul>
                     <?php  $c = get_the_ID();  $args = array('post_type'=>'conselhos'); query_posts($args); while (have_posts()): the_post(); ?>
                        <li class="<?php echo $c == $post->ID ? 'active' : null; ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>                       
                    <?php endwhile; wp_reset_query(); ?>
                </ul>
            </div>
            <div class="col-sm-9 tab-col-right font-zoom">
                <div class="tab-content">
                    <div id="tab1" class="tab-pane fade in active <?php global $post; $post_slug=$post->post_name; echo $post_slug;?>">
                        <div class="area-conselho">
                            <div class="calendar">
                                <h2>Calendário de reuniões</h2>
                                <!-- <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>2016</option>
                                            </select> 
                                        </div>
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" id="d">
                                        <div id="z"></div>

                                        <link rel="stylesheet" type="text/css" href="https://www.eletros.com.br/wp-content/themes/eletros/css/jquery-ui.min.css">
                                        <script src="https://www.eletros.com.br/wp-content/themes/eletros/js/jquery-ui.min.js"></script>
                                        <script>
                                            $(function(){
                                                var array = new Array();
                                                <?php $ds = trim(strip_tags(get_the_content())); $datas = explode(';', $ds); foreach ($datas as $d) :
                                                $br_date = explode('/',$d);
                                                $br = $br_date[2].'/'.$br_date[1].'/'.$br_date[0];
                                                 ?>
                                                    array.push("<?php echo $br; ?>"); 
                                                <?php endforeach; ?>
                                                $('#z').datepicker({
                                                    inline: true,
                                                    altField: '#d',
                                                    dateFormat: 'dd/mm/yy',
                                                    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                                    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                                    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                                    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                                    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                                    minDate: new Date(),
                                                    beforeShowDay: function( date ) {
                                                       if ($.inArray($.datepicker.formatDate('yy/mm/dd', date), array) > -1){
                                                             return [true, 'highlight'];
                                                        } else {
                                                             return [true, ''];
                                                        }
                                                    },
                                                    onSelect: function(dateText, inst) {
                                                        $('.popover-datepicker').remove();
                                                        setTimeout(function(){
                                                            var el = $(".ui-state-default:contains("+dateText.split("/")[0].replace(/^(0+)(\d)/g,"$2")+")");
                                                            var _top = el.offset().top;
                                                            var _left = el.offset().left;
                                                            for (var i = 0; i < messages.length; i++){
                                                                if (messages[i].date == dateText){
                                                                    if ($('.tab-pane').hasClass(messages[i].tipo)) {
                                                                        $('body').append('<div class="popover-datepicker"><h4>'+messages[i].title+'</h4>'+messages[i].description+'</div>');
                                                                        $('.popover-datepicker').css({'top':_top - $('.popover-datepicker').height() - 62, 'left':_left})
                                                                        $('.popover-datepicker').hide().fadeIn();
                                                                    }
                                                                }
                                                            }
                                                        });
                                                    }
                                                });
                                                $(document).click(function(e) {
                                                    if ( $(e.target).closest('#z table').length === 0 ) {
                                                        $('.popover-datepicker').remove();
                                                    }
                                                });
                                            });

                                            var messages = [
                                                {
                                                    'tipo':'conselho-fiscal',
                                                    'date':'26/01/2018', 
                                                    'description':'Apresentação da Política de Investimento aprovada', 
                                                    'title':'Reunião CFE'
                                                },
                                                {
                                                    'tipo':'conselho-fiscal',
                                                    'date':'23/02/2018', 
                                                    'description':'Apresentação da GRC Auditoria das Patrocinadoras (Controles Internos)', 
                                                    'title':'Reunião CFE'
                                                },
                                                {
                                                    'tipo':'conselho-fiscal',
                                                    'date':'13/03/2018', 
                                                    'description':'Balanço ES e ELETROS e Demonstrações Atuariais com emissão de Parecer', 
                                                    'title':'Reunião CFE'
                                                },
                                                {
                                                    'tipo':'conselho-fiscal',
                                                    'date':'27/04/2018', 
                                                    'description':'Apresentação do critério de rateio aprovado. 1ª Minuta Relatório de manifestação do exercício de 2017.', 
                                                    'title':'Reunião CFE'
                                                },
                                                {
                                                    'tipo':'conselho-fiscal',
                                                    'date':'25/05/2018', 
                                                    'description':'Prestação de Contas da FABES para emissão de Parecer', 
                                                    'title':'Reunião CFE'
                                                },
                                                {
                                                    'tipo':'conselho-fiscal',
                                                    'date':'29/06/2018', 
                                                    'description':'Aprovação do Relatório de Manifestação do exercício de 2017', 
                                                    'title':'Reunião CFE'
                                                },
                                                {
                                                    'tipo':'conselho-fiscal',
                                                    'date':'26/10/2018', 
                                                    'description':'1ª Minuta Relatório de Manifestação do 1º semestre/2018', 
                                                    'title':'Reunião CFE'
                                                },
                                                {
                                                    'tipo':'conselho-fiscal',
                                                    'date':'30/11/2018', 
                                                    'description':'Parecer do CFE prévio ao CDE - Testes de Aderência das Taxas de Juros e das Hipóteses Biométricas.', 
                                                    'title':'Reunião CFE'
                                                },
                                                {
                                                    'tipo':'conselho-fiscal',
                                                    'date':'28/12/2018', 
                                                    'description':'Aprovação do Relatório de Manifestação do 1º semestre/2018', 
                                                    'title':'Reunião CFE'
                                                },
                                                {
                                                    'tipo':'conselho-deliberativo',
                                                    'date':'09/01/2018', 
                                                    'description':'Regulamento do Plano CD Puro', 
                                                    'title':'Reunião CDE'
                                                },
                                                {
                                                    'tipo':'conselho-deliberativo',
                                                    'date':'02/02/2018', 
                                                    'description':'Apresentação sobre Coaching/Plano de Treinamento <br> Apresentação FABES; Extinção GT FABES', 
                                                    'title':'Reunião CDE'
                                                },
                                                {
                                                    'tipo':'conselho-deliberativo',
                                                    'date':'22/03/2018', 
                                                    'description':'Balanço/DAs', 
                                                    'title':'Reunião CDE'
                                                },
                                                {
                                                    'tipo':'conselho-deliberativo',
                                                    'date':'06/04/2018', 
                                                    'description':'Balanço do Exercício do Plano Eletros-Saúde – Assistencial – Prazo legal para envio (ANS): 15/04', 
                                                    'title':'Reunião CDE'
                                                },
                                                {
                                                    'tipo':'conselho-deliberativo',
                                                    'date':'08/06/2018', 
                                                    'description':'Prestação de Contas da FABES – prazo final de envio à Provedoria de Fundações – MPERJ – Até 30/06', 
                                                    'title':'Reunião CDE'
                                                },
                                                {
                                                    'tipo':'conselho-deliberativo',
                                                    'date':'09/11/2018', 
                                                    'description':'Orçamentos da ELETROS, do ES <br> Prazo legal limite: Idem Política de Investimento da ELETROS <br> Reajuste de Mensalidades do ES', 
                                                    'title':'Reunião CDE'
                                                },
                                                {
                                                    'tipo':'conselho-deliberativo',
                                                    'date':'14/12/2018', 
                                                    'description':'Política de Investimento da FABES – prazo legal até 15/12 Idem Orçamento da FABES <br> Prazo final para aprovação da Política de Investimento da ELETROS (até o final de dezembro)<br> Taxa de Juros Empréstimos <br> Testes de Aderência das Taxas de Juros e das Hipóteses Biométricas, se possível com Parecer do CFE prévio', 
                                                    'title':'Reunião CDE'
                                                }
                                            ];
                                        </script>
                                    </div>
                                </div>
                                <style type="text/css">
                                    #d {width: 0; height: 0; position: absolute; visibility: hidden;}
                                    .ui-widget-content {background: transparent; width: 500px; margin: 0 auto; }
                                    .ui-widget.ui-widget-content {border: none; }
                                    .ui-datepicker .ui-datepicker-title {font-size: 20px; font-weight: 300; color: #2566b1; text-transform: uppercase; }
                                    .ui-datepicker .ui-datepicker-header {width: 250px; margin: 0 auto; }
                                    .ui-widget-header {background: transparent; border: 0; }
                                    .ui-widget-header a {text-align: center; }
                                    .ui-widget-header a span {display: none !important; }
                                    .ui-datepicker .ui-datepicker-prev:before, .ui-datepicker .ui-datepicker-next:before {font-size: 25px; color: #626366; }
                                    .ui-datepicker .ui-datepicker-prev:before {font-family: FontAwesome; content: "\f104"; }
                                    .ui-datepicker .ui-datepicker-next:before {font-family: FontAwesome; content: "\f105"; }
                                    .tabs.tab-vertical .tab-col-right table {margin-top: 20px; }
                                    .tabs.tab-vertical .tab-col-right thead {background-color: #626366; }
                                    .ui-datepicker table tbody {background-color: #fff; } .tabs.tab-vertical .tab-col-right table > tbody > tr:nth-of-type(even), .ranking-das-corretoras table > tbody > tr:nth-of-type(even), .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active {background-color: transparent; border: 0; }
                                    .tabs.tab-vertical .tab-col-right tr td, .ranking-das-corretoras tr td {line-height: 10px; padding: 0 15px; }
                                    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active {font-size: 16px; font-weight: 300; color: #626366; }
                                    .ui-datepicker td span, .ui-datepicker td a {text-align: center; padding: 15px 2px !important; }
                                    .ui-state-active {background-color: #cccccc !important; color: #fff !important; }
                                     td.highlight a {background-color: #2c8ecd !important; color: #fff !important; }
                                    .popover-datepicker {z-index: 9999; background-color: #eeeeee; border: 1px solid #2c8ecd; padding: 20px; position: absolute; top: 0; left: 0; font-size: 14px; font-style: italic; font-weight: 300; color: #000000; width: 280px; max-width: 100%; }
                                    .popover-datepicker:before {content: " "; display: block; background: url(<?php echo bloginfo('template_url');?>/img/seta-popover-datepicker.png) no-repeat; width: 17px; height: 21px; position: absolute; bottom: -21px; left: -1px; }
                                    .popover-datepicker h4 {color: #000; font-style: normal; font-weight: 500; }
                                </style>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</section>
<!-- // content -->
<script>
    $(document).ready(function(){
        $('.item-home a').text('Inicial').prop('href','/conselhos');
    });
</script>
<?php get_footer(); endif; ?>