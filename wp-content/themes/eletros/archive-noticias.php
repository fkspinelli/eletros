<?php get_header(); get_template_part('partials/content','top'); ?>
  <section>
        <div class="bg-eeeeee">
            <div>
                <?php eletros_breadcrumbs(); ?>
                <div style="padding-bottom:50px;"></div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 detalhes-noticia">
                    <form class="form-horizontal" role="form">
                            <div class="form-group input-search">
                                <label class="control-label col-sm-9 text-uppercase" for="filter">Filtrar por:</label>
                                <div class="col-sm-3">
                                   <?php wp_dropdown_categories(['class'=>'form-control','id'=>'filter','hide_if_empty'=>true,'taxonomy'=>'notice_category','exclude'=>'21','show_option_none'=>'Todas','option_none_value'  => '-1','selected'=>$_GET['c']]); ?> 
                                </div>
                            </div>
                        </form>
                  
                    <?php  
                  

                        if(isset($_GET['c'])):
                            $cat = $_GET['c']; 
                           query_posts( array(  
                                'post_type' => 'noticias', 
                             
                                'posts_per_page' => 80, 
                                'tax_query' => array( 
                                    array( 
                                        'taxonomy' => 'notice_category', //or tag or custom taxonomy
                                        'field' => 'id', 
                                        'terms' => array($cat) 
                                    ) 
                                ) 
                            ) );
                        endif;
                        ?>
                        <div class="noticias">
                            <ul>
                                <?php while(have_posts()): the_post();  $cats = getMainCategory($post->ID, 'notice_category'); ?>
                                      <li>
                                      <div class="data-noticia-list"><?php echo get_the_date(); ?></div>
                                        <a href="<?php the_permalink(); ?>">
                                            <div class="box-label">
                                                <div style="background-color:<?php echo get_field('cor_categoria','notice_category_'.$cats[0]['id']); ?>;" class="label <?php echo $cats[0]['slug']; ?>"><?php echo $cats[0]['name']; ?></div>
                                            </div>
                                            <?php the_title(); ?>
                                            <div class="link">Leia mais <i class="fa fa-chevron-circle-right"></i></div>
                                        </a>
                                    </li>
                                
                                <?php endwhile; ?> 
                            </ul> 
                           <!-- <div class="row">
                                    <div class="col-sm-6">
                                        <a href="#" class="btn btn-gray-1 btn-block text-uppercase">mostrar mais as notícias</a>
                                    </div>
                                </div> -->
                        </div>
                    </div>
                    <div class="col-sm-4 col-ultimas-noticias">
                        <?php get_template_part('partials/content','programas'); ?>
                        <div style="margin-bottom:50px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="galeria-de-midia">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>Galeria de Mídia</h2>
                        <div class="slide">
                            <ul class="bxslider">
                                <?php query_posts('cat=22'); while(have_posts()): the_post(); ?>
                                    <li><a href="<?php bloginfo('url'); ?>/midias"><?php get_the_image(['size'=>'peq','link_to_post'=>false]); ?></a></li>
                               <?php endwhile; wp_reset_query(); ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="patrocinadoras">
            <div class="container">
                <div style="margin-top:30px;"></div>
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="patrocinadoras-home">Patrocinadoras</h2>
                        <div class="box-patrocinadoras">
                            <a href="http://www.eletrobras.com/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/patrocinadoras/eletrobras.png" class="hover"></a>
                            <a href="http://www.cepel.br/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/patrocinadoras/eletrobras-cepel.png" class="hover"></a>
                            <a href="http://www.ons.org.br/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/patrocinadoras/ons.png" class="hover"></a>
                            <a href="http://www.epe.gov.br/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/patrocinadoras/epe.png" class="hover"></a>
                            <a href="http://www.ceron.com.br/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/patrocinadoras/eletrobras-rondonia.png" class="hover"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /content -->
    <script>
        $(document).ready(function(){
            $('select#filter').change(function(){
                if($(this).val() == '-1'){
                     window.location.href = '<?php bloginfo('url'); ?>/noticias';
                }else{
                     window.location.href = '<?php bloginfo('url'); ?>/noticias?c=' + $(this).val();
                }
               
            });
        });
    </script>
<?php get_footer(); ?>