<?php get_header(); the_post(); 
    // definições
    $image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
    $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),'normal' );
    $cats = getMainCategory($post->ID, 'notice_category');

    get_template_part('partials/content','top');
    
    ?>
   
    <section class="bg-eeeeee">
        <div>
            <?php eletros_breadcrumbs(); ?>
            <div style="padding-bottom:50px;"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 detalhes-noticia">
                   <?php get_template_part('partials/content', 'social'); ?>

                    <div class="row">
                        <div class="col-sm-12">
                            <div style="background-color:<?php echo get_field('cor_categoria','notice_category_'.$cats[0]['id']); ?>;" class="label <?php echo $cats[0]['slug']; ?>"><?php echo $cats[0]['name']; ?></div>
                            <div class="data-noticia"><?php echo get_the_date(); ?></div>
                        </div>
                    </div>
                    <div class="row font-zoom">
                        <div class="col-sm-12">
                            <h1><?php the_title(); ?></h1>

                            <img src="<?php echo $image_url ?>">
                            <?php the_content(); ?>

                              <?php $fotos = CFS()->get('fotos'); ?>
                              <?php if(!empty($fotos)): ?>
                              <div class="galery">
                                <br>
                                <div class="fotorama" data-nav="thumbs">
                                  <?php  foreach ($fotos as $foto) : ?>
                                  <?php $src = wp_get_attachment_image_src($foto['foto'], 'large'); ?>
                                  <img src="<?php echo $src[0]; ?>" data-caption="<?php echo $foto['titulo']; ?>">
                                  <?php endforeach; ?>
                                </div>
                                <br>
                                <br>
                              </div>
                              <?php endif; ?>
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="diver"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-ultimas-noticias">
                   <?php include('widgets/main_sidebar.php');?>
                </div>
            </div>
        </div>
    </section>
    <!-- /content -->
<?php get_footer(); ?>