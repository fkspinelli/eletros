<?php 
include('../../../../../wp-config.php');
$args = array('post_type'=>'pegr', 'post__in'=>array($_POST['id']));
query_posts($args);
$res = '';
while(have_posts()): the_post();
	$res = '<h4>'.get_the_title().'</h4>';
	$res .= wpautop(get_the_content());	
endwhile;

echo json_encode(array('response'=>$res));