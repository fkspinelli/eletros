<?php 
include('../../../../../wp-config.php');



$args = array('post_type'=>'conveniados');

//add_filter( 'posts_where', 'title_filter', 10, 2 ); 

if($_POST['c'] != ''){
	$args['tax_query'] = array(
        array(
            'taxonomy' => 'conveniado_category',
            'terms' => $_POST['c'] //if field is ID you can reference by cat/term number
        )
    );
}

if($_POST['s'] != ''){
	$args['s'] = $_POST['s'];
}

query_posts($args);

//echo json_encode($args);

$arr = array();
$i = 0;
while (have_posts()) {
	$i++;
	the_post();

	$arr[$i]['title'] = get_the_title();
	$arr[$i]['content'] = get_the_content();

}
//remove_filter( 'posts_where', 'title_filter', 10, 2 );
echo json_encode($arr);