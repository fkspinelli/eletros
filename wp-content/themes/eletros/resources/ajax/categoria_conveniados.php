<?php 
include('../../../../../wp-config.php');

$terms = get_terms( array(
    'taxonomy' => 'conveniado_category',
    'hide_empty' => true,
) );

echo json_encode($terms);