<?php

add_image_size('med', 293,194,true);
add_image_size('peq', 204,136,true);

add_image_size('publicacoes_thumb', 126,168,true);

// Adicionando excerpt para paginas
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}



the_posts_pagination( array(
    'mid_size'  => 2,
    'prev_text' => __( 'Back', 'textdomain' ),
    'next_text' => __( 'Onward', 'textdomain' ),
) );

if (function_exists('register_sidebar')) {
    register_sidebar(array(
		'name'=>'Sidebar',
    'before_widget'=>'<div class="widget">',
    'after_widget'=>'</div>',
		'before_title'=>'<h3>',
		'after_title'=>'</h3>',
    ));
}

function getPercent($v, $t){
    $res = ($v*100)/$t;
    return round($res);
}
function getVideoID($video_src){
       $video_id = false;
    $url = parse_url($url);
    if (strcasecmp($url['host'], 'youtu.be') === 0)
    {
        #### (dontcare)://youtu.be/<video id>
        $video_id = substr($url['path'], 1);
    }
    elseif (strcasecmp($url['host'], 'www.youtube.com') === 0)
    {
        if (isset($url['query']))
        {
            parse_str($url['query'], $url['query']);
            if (isset($url['query']['v']))
            {
                #### (dontcare)://www.youtube.com/(dontcare)?v=<video id>
                $video_id = $url['query']['v'];
            }
        }
        if ($video_id == false)
        {
            $url['path'] = explode('/', substr($url['path'], 1));
            if (in_array($url['path'][0], array('e', 'embed', 'v')))
            {
                #### (dontcare)://www.youtube.com/(whitelist)/<video id>
                $video_id = $url['path'][1];
            }
        }
    }
    return $video_id;
}


add_theme_support( 'post-thumbnails' );

add_action( 'init', 'create_post_type' );

// Alterando nome do grupo de assinantes para conselhieors
function change_role_name() {
    global $wp_roles;

    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    //You can list all currently available roles like this...
    //$roles = $wp_roles->get_names();
    //print_r($roles);

    //You can replace "administrator" with any other role "editor", "author", "contributor" or "subscriber"...
    $wp_roles->roles['subscriber']['name'] = 'Conselheiro';
    $wp_roles->role_names['subscriber'] = 'Conselheiro';           
}

// Criado shortcode de login

add_action( 'init', 'my_add_shortcodes' );

function my_add_shortcodes() {

  //add_shortcode( 'eletros_login_form', 'my_login_form_shortcode' );
}
function my_login_form_shortcode() {

  if ( is_user_logged_in() ){
     $current_user = wp_get_current_user();
    return 'Olá, '.$current_user->display_name.' | <a href="'.site_url('/conselhos/').'">Área do conselho</a> | <a href="'.wp_logout_url( home_url() ).'">Sair</a>';
  }

  return wp_login_form( array('label_username'=>'','label_password'=>'','label_log_in'=>'ENTRAR', 'echo' => false,'redirect' => site_url('/conselhos/') ) );
}

// Adiciona o aviso de "Esqueci minha senha" acima do formulario
function eletros_login_form_top( $login_form_top, $args ) {
  if ( ! empty( $_GET['envio-senha'] ) AND 'OK' == $_GET['envio-senha'] ) {
    $login_form_top .= '<div id="area-conselho-rodape-aviso">O link para redefinição de senha foi enviado para o e-mail informado.</div>';
  }
  return $login_form_top;
}
add_filter( 'login_form_top', 'eletros_login_form_top', 10, 2 );

// Adiciona o "Esqueci minha senha" abaixo do botao
function eletros_login_form_bottom( $login_form_bottom, $args ) {
  $login_form_bottom .= '<a href="' . home_url('/esqueci-senha') . '">Esqueci minha senha</a>';
  return $login_form_bottom;
}
//add_filter( 'login_form_bottom', 'eletros_login_form_bottom', 10, 2 );

// Redirecionamento depois de login
function eletros_login_redirect( $redirect_to, $request, $user  ) {
  return ( is_array( $user->roles ) && in_array( 'administrator', $user->roles ) ) ? admin_url() : site_url('/conselhos/');
}
add_filter( 'login_redirect', 'eletros_login_redirect', 10, 3 );


add_action('init', 'change_role_name');

function create_post_type() {
   // post tpe de conselhos
  register_post_type( 'conselhos',
    array(
      'labels' => array(
        'name' => __( 'Conselhos' ),
        'singular_name' => __( 'Conselho' )
      ),
      'menu_position'         => 10,
      'public' => true,
      'has_archive' => true,
      'supports' => array('title','editor', 'revisions')
    )
  );
// post tpe de conselhos
  register_post_type( 'atas',
    array(
      'labels' => array(
        'name' => __( 'Atas' ),
        'singular_name' => __( 'Ata' )
      ),
      'menu_position'         => 11,
      'public' => true,
      //'has_archive' => true,
      'supports' => array('title')
    )
  );
  register_post_type( 'type_banner',
    array(
      'labels' => array(
        'name' => __( 'Banners' ),
        'singular_name' => __( 'Banner' )
      ),
      'menu_position'         => 3,
      'public' => true,
      'has_archive' => true,
      'supports' => array('title','thumbnail')
    )
  );
   register_post_type( 'noticias',
    array(
      'labels' => array(
        'name' => __( 'Notícias' ),
        'singular_name' => __( 'Notícia' ),
        'add_new_item'  => __( 'Adicionar nova notícia'),
      ),
      'public' => true,
      'has_archive' => true,
      'taxonomies'            => array( 'notice_category' ),
      'menu_position'         => 4,
      'rewrite' => array('slug' => 'noticias'),
      'supports' => array('title','thumbnail','excerpt','editor','revisions')
    )
  );
   register_post_type( 'pegr',
    array(
      'labels' => array(
        'name' => __( 'PEGR News' ),
        'singular_name' => __( 'PEGR News' ),
        'add_new_item'  => __( 'Adicionar novo PEGR News'),
      ),
      'public' => true,
      'has_archive' => true,
      //'taxonomies'            => array( 'conveniado_category' ),
      'menu_position'         => 5,
      //'rewrite' => array('slug' => 'conveniados'),
      'supports' => array('title','excerpt','editor','revisions')
    )
  );
 register_post_type( 'conveniados',
    array(
      'labels' => array(
        'name' => __( 'Conveniados' ),
        'singular_name' => __( 'Conveniado' ),
        'add_new_item'  => __( 'Adicionar novo conveniado'),
      ),
      'public' => true,
      'has_archive' => true,
      'taxonomies'            => array( 'conveniado_category' ),
      'menu_position'         => 7,
      'rewrite' => array('slug' => 'conveniados'),
      'supports' => array('title','excerpt','editor','revisions')
    )
  );
     register_post_type( 'publicacoes',
    array(
      'labels' => array(
        'name' => __( 'Publicações' ),
        'singular_name' => __( 'Publicação' ),
        'add_new_item'  => __( 'Adicionar nova publicação'),
      ),
      'public' => true,
      'has_archive' => true,
      'taxonomies'            => array( 'publicacoes_category' ),
      'menu_position'         => 6,
      'rewrite' => array('slug' => 'publicacoes'),
      'supports' => array('title','thumbnail')
    )
  );


    register_post_type( 'planos',
    array(
      'labels' => array(
        'name' => __( 'Planos' ),
        'singular_name' => __( 'Plano' ),
        'add_new_item'  => __( 'Adicionar novo plano'),
      ),
      'public' => true,
      'has_archive' => true,
      'hierarchical' => true,
      //'taxonomies'            => array( 'plano_category' ),
      'menu_position'         => 5,
      'rewrite' => array('slug' => 'planos'),
      'capability_type'       => 'page',
      'supports' => array('title','thumbnail','excerpt','editor','revisions','page-attributes')
    )
  );


  register_post_type( 'banners_top_convenio',
  array(
    'labels' => array(
      'name' => __( 'Banner' ),
      'singular_name' => __( 'PEGR News' ),
      'add_new_item'  => __( 'Adicionar novo PEGR News'),
    ),
    'public' => true,
    'has_archive' => true,
    //'taxonomies'            => array( 'conveniado_category' ),
    'menu_position'         => 5,
    //'rewrite' => array('slug' => 'conveniados'),
    'supports' => array('title','excerpt','editor','revisions')
  )
);



}

//Quando der problema no post type acionar esta função
flush_rewrite_rules();


function add_query_vars_filter( $vars ){
  $vars[] = "c";
  return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

// Adicionando Taxonomias
add_action( 'init', 'custom_taxonomy', 0 );

function custom_taxonomy() {

    $labels_notice = array(
    'name'                       => _x( 'Categoria de notícias', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria de notícias', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria de notícia', 'text_domain' )
    );
    $labels_publ = array(
    'name'                       => _x( 'Categoria de publicações', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria de publicações', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria de publicações', 'text_domain' )
    );

    $labels_conv = array(
    'name'                       => _x( 'Categoria de conveniados', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria de conveniados', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria de conveniados', 'text_domain' )
    );
   $args_notice = array(
    'labels'                     => $labels_notice,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'notice_category', array( 'noticias' ), $args_notice );
   
// Categoria de publicações
   $args_publ = array(
    'labels'                     => $labels_publ,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );

  register_taxonomy( 'publicacoes_category', array( 'publicacoes' ), $args_publ );

  // Categoria de conveniados
   $args_conv = array(
    'labels'                     => $labels_conv,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );

  register_taxonomy( 'conveniado_category', array( 'conveniados' ), $args_conv );

}

// Cirnaod service
function noticias_func()
{
global $post;
 $res = '<div class="row"><div class="col-md-9">';
    $args_noticias = array('post_type'=>'pegr', 'posts_per_page'=>1); query_posts($args_noticias); while(have_posts()): the_post(); $current_post = $post->ID;
        $res .='<div class="content_noticias">
          <h4>'.get_the_title().'</h4>
           '.wpautop(get_the_content()).'
        </div>';
       endwhile; wp_reset_query();
  $res .= '</div>
  <div class="col-md-3">
  <h5 >Arquivos </h5>';
                                  
  $args_noticias_list = array('post_type'=>'pegr');
                                       
  query_posts($args_noticias_list); $day_check = ''; 
                                         
  while(have_posts()) : the_post();   
                                       
   $day = get_the_date('j');   
    
    if ($day != $day_check) {     
    if ($day_check != '') {       
       $res .= '</ul></div>'; // close the list here     
        }    $res .= '<div style="margin-bottom:10px;" class="dropdown"><button style="background:#fff; border:none;" class="dropdown-toggle" type="button" data-toggle="dropdown" id="menu_'.$post->ID.'"><i style="color:gray;" class="fa fa-caret-right"></i> '.get_the_date('M(Y)').'</button><ul style="padding-left: 20px;" class="dropdown-menu" role="menu" aria-labelledby="menu_'.$post->ID.'">';   
        } 

       $res .= '<li style="margin-bottom: 0;line-height: 0;"><a style="padding:0;display: inline;" class="news_l" data-id="'.$post->ID.'" href="javaScript:;">'.get_the_title().'</a></li>';
       $day_check = $day; 
  endwhile; wp_reset_query();
  $res .='</div></div>';

  return $res;
}
add_shortcode( 'noticias', 'noticias_func' );

/**
  *Registrando Widget
**/

add_action( 'widgets_init', 'eletros_widgets_init' );
function eletros_widgets_init() {
   
    # Registradno Sidebars
    register_sidebar( array(
        'name' => __( 'Sidebar Principal', 'eletros-main-sidebar' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets nesta área serão mostradas na barra lateral', 'eletros-main-sidebar' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
      ) );
}

// Pegando a categoria principal
function getMainCategory($post_id, $tax){

  $post_categories = get_the_terms($post_id, $tax);
  $cats = array();                    
  foreach($post_categories as $c){
      $cat = get_category( $c );
     
      if(($cat->slug != 'sem-categoria') && $cat->slug != 'destaque'){
          $cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug, 'id' => $cat->term_id );
      }
  }
  return $cats;
}

// Breadcrumbs
function eletros_breadcrumbs() {
       
    // Settings
    $separator          = '';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = 'Página inicial';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo ' <div class="breadcrumb"><div class="container"><i class="icon icon-circle-slelected"></i><ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
           
        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li class="separator separator-home"> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ul></div></div>';
           
    }
       
}


/*// Register Custom Post Type
function custom_post_type() {

  $labels = array(
    'name'                  => _x( 'Post Types', 'Post Type General Name', 'text_domain' ),
    'singular_name'         => _x( 'Post Type', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'             => __( 'Post Types', 'text_domain' ),
    'name_admin_bar'        => __( 'Post Type', 'text_domain' ),
    'archives'              => __( 'Item Archives', 'text_domain' ),
    'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
    'all_items'             => __( 'All Items', 'text_domain' ),
    'add_new_item'          => __( 'Add New Item', 'text_domain' ),
    'add_new'               => __( 'Add New', 'text_domain' ),
    'new_item'              => __( 'New Item', 'text_domain' ),
    'edit_item'             => __( 'Edit Item', 'text_domain' ),
    'update_item'           => __( 'Update Item', 'text_domain' ),
    'view_item'             => __( 'View Item', 'text_domain' ),
    'search_items'          => __( 'Search Item', 'text_domain' ),
    'not_found'             => __( 'Not found', 'text_domain' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
    'featured_image'        => __( 'Featured Image', 'text_domain' ),
    'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
    'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
    'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
    'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
    'items_list'            => __( 'Items list', 'text_domain' ),
    'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
    'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
  );
  $args = array(
    'label'                 => __( 'Post Type', 'text_domain' ),
    'description'           => __( 'Post Type Description', 'text_domain' ),
    'labels'                => $labels,
    'supports'              => array( ),
    'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,    
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'post_type', $args );

}
add_action( 'init', 'custom_post_type', 0 );



// Register Custom Taxonomy
function custom_taxonomy() {

  $labels = array(
    'name'                       => _x( 'Taxonomies', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Taxonomy', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Taxonomy', 'text_domain' ),
    'all_items'                  => __( 'All Items', 'text_domain' ),
    'parent_item'                => __( 'Parent Item', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
    'new_item_name'              => __( 'New Item Name', 'text_domain' ),
    'add_new_item'               => __( 'Add New Item', 'text_domain' ),
    'edit_item'                  => __( 'Edit Item', 'text_domain' ),
    'update_item'                => __( 'Update Item', 'text_domain' ),
    'view_item'                  => __( 'View Item', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular Items', 'text_domain' ),
    'search_items'               => __( 'Search Items', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'no_terms'                   => __( 'No items', 'text_domain' ),
    'items_list'                 => __( 'Items list', 'text_domain' ),
    'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'taxonomy', array( 'post' ), $args );

}
add_action( 'init', 'custom_taxonomy', 0 );




*/

function eletros_atas_posts_date_format( $clause, $wp_query ) {
  if ( ! empty( $wp_query->query_vars['meta_date_format'] ) ) {
    $clause = preg_replace( '/CAST\(([\w\.]*) AS DATE\)/', "STR_TO_DATE($1, '".$wp_query->query_vars['meta_date_format']."')", $clause);
  }
  return $clause;
}
add_filter( 'posts_where', 'eletros_atas_posts_date_format', 99, 2 );
add_filter( 'posts_orderby', 'eletros_atas_posts_date_format', 99, 2 );

add_action("wp_ajax_gravar_avaliacao", "gravar_avaliacao");
add_action("wp_ajax_listar_avaliacoes", "listar_avaliacoes");
add_action("wp_ajax_validar_eletros", "validar_eletros");
add_action("wp_ajax_gravar_sugestao_empresa", "gravar_sugestao_empresa");


add_action("wp_enqueue_scripts", "meu_enqueue_scripts");

function meu_enqueue_scripts() {
  wp_enqueue_script('scripts', get_template_directory_uri()."/js/script.js" );
  wp_localize_script('scripts', 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
}

function empresas_convenio() {

    global $wpdb;
    
    $load = 0;
    $categorias = [];
    $search = "";
    $lancamento = null;
    $estado = null;

    if (isset($_REQUEST['load'])) {
        $load = (int) $_REQUEST['load'];
    }

    if (isset($_REQUEST['categorias']) && is_array($_REQUEST['categorias'])) {
        $categorias = $_REQUEST['categorias'];
    }

    if (isset($_REQUEST['s'])) {
        $search = $_REQUEST['s'];
    }

    if (isset($_REQUEST['estado'])) {
        $estado = $_REQUEST['estado'];
    }

    if (isset($_REQUEST['lancamento'])) {
        $lancamento = $_REQUEST['lancamento'] == 'true' ? true : false;
    }

    $args = array(
        'posts_per_page' => 12,
        'offset' => $load,
        'post_status' => 'publish',
        'post_type' => 'conveniados'
    );

    if (isset($_REQUEST['ordem'])) {

        switch($_REQUEST['ordem']) {
            case 'az':
                $args['orderby'] = 'title';
                $args['order'] = 'ASC';
                break;

            case 'za':
                $args['orderby'] = 'title';
                $args['order'] = 'DESC';
                break;
            case 'antigo':
                $args['orderby'] = 'date';
                $args['order'] = 'ASC';
                break;
            case 'novo':
                $args['orderby'] = 'date';
                $args['order'] = 'DESC';
                break;
            default:
                $args['orderby'] = 'date';
                $args['order'] = 'ASC';
            
                break;

        }
    }

    if (count($categorias) > 0) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'conveniado_category',
                'field' => 'slug',
                'terms' => $categorias
            )
        );
    }

    /**if (strlen($estado) > 0) {
        $args['meta_query']	= array(
            array(
                'key'	 	=> 'estado',
                'value'	  	=> $estado,
                //'compare' 	=> 'IN',
            ),
        );
        //$args['meta_key'] = 'estado';
        //$args['meta_value'] = $estado;
    }**/

    if (strlen($search) > 0) {
        $args['s'] = $search;
    }

    if ($lancamento) {
        $args['date_query'] = array(
            'after' => date('Y-m-d', strtotime('-30 days')) 
        );
    }

    $response = [];
    
    $conveniados = get_posts($args);
    
    $i = 0;
    
    foreach ($conveniados as $conveniado) :
        
        $postMeta = get_post_meta($conveniado->ID);

        $tablename = $wpdb->prefix . 'ratings_eletros';

        $avaliacao = $wpdb->get_var(
            "
            SELECT AVG(rating)   
            FROM $tablename   
            WHERE enabled = 1 AND id_post = " . $conveniado->ID
        );

        $img = "";
        $descricao = "";
        $textoDesconto = "";
        $desconto = "";

        if (isset($postMeta['imagem'])) {
            $img = wp_get_attachment_image_url(current($postMeta['imagem']));
        }

        if (isset($postMeta['descricao_da_empresa'])) {
            $descricao = current($postMeta['descricao_da_empresa']);
        }

        if (isset($postMeta['descricao_da_empresa'])) {
            $desconto = current($postMeta['percentual_desconto']);
        }

        if (isset($postMeta['texto_obervacao_desconto'])) {
            $textoDesconto = current($postMeta['texto_obervacao_desconto']);
        }

        

        $response[] = [
            'name' => $conveniado->post_title,
            'url' => get_permalink() . "/eletros-convenio/convenio/?" .  $conveniado->post_name,
            'img' => $img,
            'descricao' => $descricao,
            'desconto' => $desconto,
            'textoDesconto' => $textoDesconto,
            'avaliacao'   => ($avaliacao ? $avaliacao : 0)
        ];

    endforeach;

    echo json_encode($response);


    wp_die();
}

add_action("wp_ajax_empresas_convenio", "empresas_convenio");
add_action("wp_ajax_nopriv_empresas_convenio", "empresas_convenio");

function listar_avaliacoes() {

    global $wpdb; // this is how you get access to the database

    $idpost = $_POST['post_id'];
    $pagina = isset($_POST['pagina']) ? $_POST['pagina']: 1;
    $qtdPagina = 2;

    $mat = $pagina -1; //ASSIM INICIAREMOS DA LINHA ZERO DO BANCO
    $inicio = $mat * $qtdPagina;


    $tablename = $wpdb->prefix . 'ratings_eletros';

    $total = $wpdb->get_var(
        "
		SELECT count(*)   
        FROM $tablename 
        WHERE enabled = 1
        AND id_post = " . $idpost
        
    );

    $resultados = $wpdb->get_results(
            "
		SELECT *   
		FROM $tablename   
        WHERE id_post = " . $idpost . "
        AND enabled = 1
		order by created_at desc
	    LIMIT $inicio,$qtdPagina"
    );

    $json = [];

    foreach($resultados as $resultado) {
        $dateCreated = date_create_from_format('Y-m-d H:i:s', $resultado->created_at);
        $json[] = [
            'name' => $resultado->name,
            'message' => $resultado->description,
            'rating' => $resultado->rating,
            'created_at' => $dateCreated->format('d/m/Y')
        ];
    }


    $totalPagina= ceil($total/$qtdPagina);

    echo json_encode(['data' => $json, 'total' => $total, 'pages' => $totalPagina, 'page' => $pagina]);


    wp_die();
}

function gravar_avaliacao() {
    global $wpdb; // this is how you get access to the database

    $idpost = $_POST['post_id'];
    $name = $_POST['name'];
    $number_eletros= $_POST['number_eletros'];
    $message = $_POST['message'];
    $star = (int) $_POST['star'];

    $tablename = $wpdb->prefix . 'ratings_eletros';

    $wpdb->insert(
        $tablename,
        array(
            'name' => $name,
            'id_post' => $idpost,
            'eletros_number' => $number_eletros,
            'description' => $message,
            'rating' => $star,
            'created_at' => current_time('mysql', 1),
        )
    );

    $empresa = $wpdb->get_var( $wpdb->prepare( "SELECT post_title FROM $wpdb->posts WHERE id = %d", $idpost ));
    

    $to = 'email@emai.com';
    $subject = 'Sugestão de Empresa';
    $message =<<<HTML
        <h2>Uma avaliação de empresa foi enviada.</h2>
        <p>Nome: $name</p>
        <p>N. Eletros: $number_eletros</p>
        <p>Empresa: $empresa</p>
        <p>Descrição: $message</p>
        <p>Pontuação: $star</p>
        

HTML;
    $headers = array('Content-Type: text/html; charset=UTF-8');
    
    wp_mail( $to, $subject, $body, $headers );

    echo json_encode(['message' => 'ok']);


    wp_die();
}

function gravar_sugestao_empresa() {
    global $wpdb; // this is how you get access to the database

    $nome = $_POST['nome'];
    $eletros = $_POST['numero_eletros'];
    $empresa = $_POST['empresa'];
    $telefone = $_POST['telefone'];
    $email = $_POST['email'];
    $site = $_POST['site'];

    $tablename = $wpdb->prefix . 'sugestao_empresa';
    
    $result = $wpdb->insert(
        $tablename,
        array(
            'nome' => $nome,
            'numero' => $eletros,
            'empresa' => $empresa,
            'telefone' => $telefone,
            'email' => $email,
            'site' => $site,
            'created_at' => current_time('mysql', 1),
        )
    );

    $to = 'email@emai.com';
    $subject = 'Sugestão de Empresa';
    $message =<<<HTML
        <h2>Uma sugestão de empresa foi enviada</h2>
        <p>Nome: $nome</p>
        <p>N. Eletros: $eletros</p>
        <p>Empresa: $empresa</p>
        <p>Telefone: $telefone</p>
        <p>E-mail: $email</p>
        <p>Site: $site</p>

HTML;
    $headers = array('Content-Type: text/html; charset=UTF-8');
    
    wp_mail( $to, $subject, $body, $headers );
    

    echo json_encode(['message' => 'ok']);


    wp_die();
}

function validar_eletros() {

    global $wpdb; // this is how you get access to the database

    $tablename = $wpdb->prefix . 'numero_eletros';

    $numero = $_POST['numero'];

    $existe = $wpdb->get_var(
        "
		SELECT count(numero)  
		FROM $tablename   
		WHERE numero = '" . $numero . "'"
    );
    
    echo json_encode(['existe' => (bool) $existe]);


    wp_die();
}

function rating_create_table() {
    // Acede ao objeto global de gestao de bd
    global $wpdb;

    // Verifica se a nova tabela existe
    // "prefix" o prefixo escolhido na instalacao do WordPress
    $tablename = $wpdb->prefix . 'ratings_eletros';
    // Ao usar a funcao dbDelta() e necessario carregar este ficheiro
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    
    

    // Se a tabela nao existe sera criada
    if ( $wpdb->get_var( "SHOW TABLES LIKE '$tablename'" ) != $tablename ) {

        $sql = "CREATE TABLE `$tablename` (
          `id` INT NOT NULL AUTO_INCREMENT,
          `id_post` INT NOT NULL,
          `name` VARCHAR(100) NOT NULL,
          `eletros_number` VARCHAR(45) NOT NULL,
          `description` TEXT NOT NULL,
          `rating` INT NOT NULL DEFAULT 0,
          `enabled` INT NOT NULL DEFAULT 0,
          `created_at` DATETIME NOT NULL,
          PRIMARY KEY (`id`));
      ";

        

        // Funcao que cria a tabela na bd e executa as otimizacoes necessarias
        dbDelta( $sql );

    } else {
        $wpdb->query("ALTER TABLE {$wpdb->prefix}ratings_eletros ADD enabled INT(1) NOT NULL DEFAULT 0");
    }

}
add_action( 'init', 'rating_create_table' );

function segunda_via_cartao_create_table() {
    // Acede ao objeto global de gestao de bd
    global $wpdb;

    // Verifica se a nova tabela existe
    // "prefix" o prefixo escolhido na instalacao do WordPress
    $tablename = $wpdb->prefix . 'segunda_via_cartao_eletros';

    // Se a tabela nao existe sera criada
    if ( $wpdb->get_var( "SHOW TABLES LIKE '$tablename'" ) != $tablename ) {

        $sql = "CREATE TABLE `$tablename` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `nome` VARCHAR(255) NOT NULL,
            `patrocinadora` VARCHAR(255),
            `cpf` VARCHAR(20) NOT NULL,
            `ativo` INT NOT NULL DEFAULT 0,
            `assistido` INT NOT NULL DEFAULT 0,
            `pensionista` INT NOT NULL DEFAULT 0,
            `endereco` VARCHAR(255),
            `cep` VARCHAR(20),
            `bairro` VARCHAR(20) ,
            `cidade` VARCHAR(20),
            `eletros_number` VARCHAR(45) NOT NULL,
            `created_at` DATETIME NOT NULL,
            PRIMARY KEY (`id`));
      ";

        // Ao usar a funcao dbDelta() e necessario carregar este ficheiro
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        // Funcao que cria a tabela na bd e executa as otimizacoes necessarias
        dbDelta( $sql );

    }

}
add_action( 'init', 'segunda_via_cartao_create_table' );

function eletros_create_table() {
    // Acede ao objeto global de gestao de bd
    global $wpdb;

    // Verifica se a nova tabela existe
    // "prefix" o prefixo escolhido na instalacao do WordPress
    $tablename = $wpdb->prefix . 'numero_eletros';

    // Se a tabela nao existe sera criada
    if ( $wpdb->get_var( "SHOW TABLES LIKE '$tablename'" ) != $tablename ) {

        $sql = "CREATE TABLE `$tablename` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `numero` VARCHAR(255) NOT NULL,
            `created_at` DATETIME NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE INDEX `numero_UNIQUE` (`numero` ASC));
      ";

        // Ao usar a funcao dbDelta() e necessario carregar este ficheiro
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        // Funcao que cria a tabela na bd e executa as otimizacoes necessarias
        dbDelta( $sql );

    }

}
add_action( 'init', 'eletros_create_table' );

function sugestao_empresa_create_table() {
    // Acede ao objeto global de gestao de bd
    global $wpdb;

    // Verifica se a nova tabela existe
    // "prefix" o prefixo escolhido na instalacao do WordPress
    $tablename = $wpdb->prefix . 'sugestao_empresa';

    // Se a tabela nao existe sera criada
    if ( $wpdb->get_var( "SHOW TABLES LIKE '$tablename'" ) != $tablename ) {

        $sql = "CREATE TABLE `$tablename` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `nome` VARCHAR(255) NOT NULL,
            `numero` VARCHAR(255) NOT NULL,
            `empresa` VARCHAR(255),
            `telefone` VARCHAR(20),
            `email` VARCHAR(150),
            `site` VARCHAR(150),
            `created_at` DATETIME NOT NULL,
            PRIMARY KEY (`id`));
      ";


        // Ao usar a funcao dbDelta() e necessario carregar este ficheiro
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        // Funcao que cria a tabela na bd e executa as otimizacoes necessarias
        dbDelta( $sql );

    }

}
add_action( 'init', 'sugestao_empresa_create_table' );

//Telas

function eletros_pagina_sugestoes() {
    add_menu_page( 'Convênios', 'Convênios', 10,  'convenios' );
    add_submenu_page('convenios', 'Empresas Sugeridas', 'Empresas Sugeridas', 10, 'convenios-empresa-sugeridas', 'html_empresas_sugeridas'  );
    add_submenu_page('convenios', 'Avaliação Empresa', 'Avaliação Empresa', 10, 'convenios-avaliacao-empresas', 'html_avaliacoes_empresa'  );	
}

function html_empresas_sugeridas() {
	global $wpdb;
    
    $empresasListTable = new EmpresasSugeridasListTable();

    echo <<<HTML
    <div class="wrap">
        <h1>Empresas Sugeridas</h1>
HTML;
        $empresasListTable->prepare_items();
        $empresasListTable->display();

    echo <<<HTML
    </div>
HTML;
}

function html_avaliacoes_empresa() {
	global $wpdb;
    ob_start();
    $avaliacaoEmpresaListTable = new AvaliacaoEmpresaListTable();

    $linkExportar = sprintf("?page=%s&action=%s", $_REQUEST['page'], 'export');

    echo <<<HTML
    <div class="wrap">
        <h1>Avaliações</h1>
        <!--a href="$linkExportar" target="_blank">Exportar</a-->
HTML;
        $avaliacaoEmpresaListTable->prepare_items();
        $avaliacaoEmpresaListTable->display();

    echo <<<HTML
    </div>
HTML;
}

add_action( 'admin_menu', 'eletros_pagina_sugestoes');

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
    require_once( ABSPATH . 'wp-content/plugins/convenios/EmpresasSugeridasListTable.php' );
    require_once( ABSPATH . 'wp-content/plugins/convenios/AvaliacaoEmpresaListTable.php' );
}


