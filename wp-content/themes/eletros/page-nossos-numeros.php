<?php get_header(); the_post(); get_template_part('partials/content','top'); ?>


    <!-- content -->
    <section>
        <div class="nossos-numeros">
            <div class="bg-e4f4ff">
               <?php eletros_breadcrumbs(); ?>
                <div style="padding-bottom:50px;"></div>
            </div>
            <div class="listras">
                <?php $total_geral = 0; ?>
                <?php $args = array('post_type'=>'planos','post_parent'=>'0'); query_posts($args); while(have_posts()): the_post(); if(get_field('participantes') != ''): ?>
                <div class="df">
                <div class="container">
                    <div class="item">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="titulo">
                                    <h2><?php the_title(); ?></h2>
                                    <?php if(get_field('mes_referencia') != ''):?>
                                    <div class="mes-referencia">
                                        <p>Mês de referência:</p>
                                        <div><?php echo get_field('mes_referencia'); ?></div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <!-- <div class="col-sm-4 col-xs-10 col-xs-push-1">

                                <canvas id="chart-<?php echo $post->post_name; ?>" width="300" height="300"/>
                            </div> -->
                            <div class="col-sm-12">
                                <div class="chart">
                                    <h3>Número de Participantes</h3>
                                    <?php $list = preg_split("/\\r\\n|\\r|\\n/", get_field('participantes'));  $colors = ['#9f6b95','#b36e58', '#dab771', '#78a5a4', '#4a80b1']; ?>
                                    <div class="row">
                                        <div class="col-md-6 sec">
                                            <ul class="participantes-ativos">
                                                <?php $arr_p = array(); $cont = 0; foreach($list as $participante): $cont++; $p = explode('-', $participante); array_push($arr_p, $p[1]); ?>
                                                <li>
                                                    <div style="background-color:<?php echo $colors[$cont-1]; ?>;">
                                                        <span><?php echo $p[0]; ?></span>
                                                        <div class="percent" style="width: 0%;"></div>
                                                    </div>
                                                    <div class="num"><?php echo $p[1]; ?> <span></span></div>
                                                </li>
                                                <?php endforeach; ?>
                                                
                                            </ul>
                                            <div class="total">
                                                <div>
                                                    Participantes Ativos
                                                </div>
                                                <div class="tot">
                                                  <?php echo $ativos = array_sum($arr_p); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <ul class="participantes-assistidos">
                                                <li>
                                                    <div>Assistidos</div>
                                                    <div><?php echo get_field('assistidos'); ?></div>
                                                </li>
                                                <li>
                                                    <div>Benefícios de <br> pensão por morte(*)</div>
                                                    <div><?php echo get_field('beneficios'); ?></div>
                                                </li>
                                            </ul>
                                            <div class="total">
                                                <div style="line-height: 46px;">
                                                    Assistidos
                                                </div>
                                                <div>
                                                   <?php echo $assistidos = (get_field('assistidos')+get_field('beneficios')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php 
                                        $total_plano = $ativos + $assistidos;
                                        $total_geral += $total_plano;
                                    ?>
                                    <h4>Total do Plano: <?php echo $total_plano; ?></h4>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                
                </div>
               
                <?php endif; endwhile; wp_reset_query(); ?>
            
            </div>
            <div class="bg-e4f4ff">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="cl-626366"><i><small>(*) BENEFÍCIOS DE PENSÃO POR MORTE SUBSTITUI A NOMENCLATURA DE PENSIONISTAS E CONSIDERA O TOTAL DE PARTICIPANTES FALECIDOS.</small></i></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="total-geral">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Total de Participantes: <?php echo $total_geral; ?></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /content -->
    <?php /* <script src="<?php bloginfo('template_url'); ?>/js/Chart.min.js"></script>
   <script>
        <?php $args_2 = array('post_type'=>'planos','post_parent'=>'0'); query_posts($args_2); while(have_posts()): the_post(); if(get_field('participantes') != ''): $list = preg_split("/\\r\\n|\\r|\\n/", get_field('participantes'));  $colors = ['#9f6b95','#b36e58', '#dab771', '#78a5a4', '#4a80b1'];   ?>
               
                    var planoData<?php echo $post->ID; ?> = [

                     <?php $arr_d = array(); foreach($list as $participante): $p = explode('-', $participante);  array_push($arr_d, $p[1]); endforeach;  $ac  = array_sum($arr_d); ?>
                     
   <?php $arr_p = array(); $cont = '0'; foreach($list as $participante): $cont++; $p = explode('-', $participante); array_push($arr_p, $p[1]);  ?>
                          {value: "<?php echo getPercent($p[1], $ac); ?>", color: "<?php echo $colors[$cont-1]; ?>", label: "<?php echo $p[0]; ?>"},
                        <?php endforeach; ?>
                    ];
            <?php endif; endwhile; wp_reset_query(); ?>
                    window.onload = function() {
                    if($(window).width()>768){
                       showTooltips = false;
                    }else{
                        showTooltips = true;
                    }
                     <?php $args_3 = array('post_type'=>'planos','post_parent'=>'0'); query_posts($args_3); while(have_posts()): the_post(); if(get_field('participantes') != ''): ?>
               
                    var ctxPlanoDb<?php echo $post->ID; ?> = document.getElementById("chart-<?php echo $post->post_name; ?>").getContext("2d");
                   var polarArea<?php echo $post->ID; ?>  = new Chart(ctxPlanoDb<?php echo $post->ID; ?>).Doughnut(planoData<?php echo $post->ID; ?>, {
                        responsive: true,
                        // showTooltips: showTooltips,
                        // onAnimationComplete: function() {
                        //     if(!showTooltips){
                        //         this.showTooltip(this.segments);
                        //     }
                        // },
                        tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>%",
                    });
                   

                    <?php endif; endwhile; wp_reset_query(); ?>
                     };
                </script>*/?>
<?php get_footer(); ?>


<style type="text/css">
@media (max-width: 768px){
    [class*="col-"] {
        position: inherit;
    }
}
</style>

<script type="text/javascript">
// $(document).ready(function(){
//     $('.num').each(function(){
//         num = parseInt($(this).text());
//         tot = parseInt($(this).parents('.sec').find('.tot').text());

//         res = (num*100)/tot;

//         $(this).parents('li').find('.percent').css({'width':res+'%'});
//         $(this).find('br').remove();
//         $(this).find('span').text(' ('+res.toFixed(0)+'%)');

//         console.log(res);
//     });
// });
</script>