<?php get_header(); get_template_part('partials/content','top'); ?>
<section class="bg-eeeeee">
    <div>
       <?php eletros_breadcrumbs(); ?>
        <div style="padding-bottom:30px;"></div>
    </div>
    <div class="container midia">

        <div class="row">
            <div class="col-sm-12">
                <h2>Confira os eventos promovidos pela Eletros.</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-right">
                <form method="get" action="" class="form form-inline" role="form">
                    <div class="form-group input-search">
                        <label style="vertical-align: 15px;" class="text-uppercase" for="filter">Filtrar por:</label>
                        <div class="form-group">
                            <input id="from" placeholder="No período de..." class="form-control" type="text" name="from" value="<?php echo isset($_GET['from']) ? $_GET['from'] : null; ?>">
                        
                        </div>
                        <div class="form-group">
                         <input id="to" placeholder="Até o período..." class="form-control" type="text" name="to" value="<?php echo isset($_GET['to']) ? $_GET['to'] : null; ?>">
                         </div>
                        <div class="form-group">
                        <?php wp_dropdown_categories(['class'=>'form-control','child_of'=>'22','id'=>'filter','hide_if_empty'=>true,'show_option_none'=>'Selecionar tipos de mídia','option_none_value'  => '-1','selected'=>$_GET['cat']]); ?>
                          <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row row-eq-height">
            <?php
            $args = array();
            if(isset($_GET['cat'])):

                $cat = $_GET['cat']; 
                
                if($cat != '-1'): $args = array('cat'=>$cat); endif;
                
            endif;
            if(isset($_GET['to']) and isset($_GET['from'])):
                if($_GET['to'] != '' and $_GET['from'] != ''):
                    $ex_to = explode('/', $_GET['to']);
                    $ex_from = explode('/', $_GET['from']);

                    $args += ['date_query' => array(
                            array(
                                'after'     => array(
                                    'year'  => $ex_from[2],
                                    'month' => $ex_from[1],
                                    'day'   => $ex_from[0], 
                                ),
                                'before'    => array(
                                    'year'  => $ex_to[2],
                                    'month' => $ex_to[1],
                                    'day'   => $ex_to[0], 
                                ),
                                'inclusive' => true,
                            ),
                        )];
                endif;
            endif;
            if(isset($_GET['cat']) || (isset($_GET['to']) and isset($_GET['from']))):
               // print_r($args);
                query_posts($args);
            endif;
             if(have_posts()): while(have_posts()): the_post(); $g_type = get_field('yt_video_url') != '' ? 'galeriaVideo' : 'galeriaModal' ; ?>
            <div class="col-sm-4 colh">
                <a data-toggle="modal" data-target="#<?php echo $g_type.'_'.$post->ID; ?>">
                    <div class="panel panel-default">
                        <div class="image" style="background-size:cover;background-image:url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'med')[0]; ?>);">
                            <?php get_the_image(['size'=>'med', 'link_to_post'=>false]); ?>
                        </div>
                        <div class="panel-footer">
                            <p><?php the_title(); ?></p>
                            <span>veja a galeria <i class="fa fa-search-plus"></i></span>
                        </div>
                    </div>
                </a>
            </div>
             <div id="<?php echo $g_type.'_'.$post->ID; ?>" class="modal fade" role="dialog">
                <div class="modal-dialog modal-md" align="center">
                    <?php if($g_type == 'galeriaVideo'): ?>
                        <iframe width="100%" height="330" src="https://www.youtube.com/embed/<?php getVideoID(get_field('yt_video_url')); ?>" frameborder="0" allowfullscreen></iframe>
                    <?php else:
                     $attachments = get_posts( array(
                        'post_type'   => 'attachment',
                        'numberposts' => -1,
                        'post_status' => 'published',
                        'post_parent' => $post->ID
                    ) ); 

                     //print_r($attachments);
                    ?>
                    <div class="fotorama" data-nav="thumbs">
                        <?php foreach ($attachments as $f): ?>
                            <img src="<?php echo wp_get_attachment_image_src($f->ID, 'normal')[0]; ?>">
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endwhile; endif; ?>
        </div>
        <!--<div class="row">
            <div class="col-sm-12">
                <div class="pull-right">
                    <p class="pull-left" style="margin-top:6px;">Foram encontrados 132 resultados </p>
                    <ul class="pagination pull-left">
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                    </ul>
                </div>
            </div>
        </div>-->
    </div>
</section>
<script>
        $(document).ready(function(){
          $.datepicker.regional['pt-BR'] = {
    closeText: 'Fechar',
    prevText: '&#x3c',
    nextText: '&#x3e;',
    currentText: 'Hoje',
    monthNames: ['Janeiro de','Fevereiro de','Mar&ccedil;o de','Abril de','Maio de','Junho de',
    'Julho de','Agosto de','Setembro de','Outubro de','Novembro de','Dezembro de'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
    'Jul','Ago','Set','Out','Nov','Dez'],
    dayNames: ['Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado'],
    dayNamesShort: ['D','S','T','Q','Q','S','S'],
    dayNamesMin: ['D','S','T','Q','Q','S','S'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
$.datepicker.setDefaults($.datepicker.regional['pt-BR']);
              $( "#from" ).datepicker({

     dateFormat: "dd/mm/yy",
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({

      dateFormat: "dd/mm/yy",
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
        });
</script>
<!-- /content -->
<?php get_footer(); ?>