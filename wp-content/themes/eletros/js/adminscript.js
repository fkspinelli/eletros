jQuery(document).ready(function(){
	updateLabelCFS();
	jQuery(document).on('keypress', '.cfs_input input', function(){
		updateLabelCFS();
	});
	jQuery(document).on('change', '.cfs_input select', function(){
		updateLabelCFS();
	});
	jQuery(document).on('click', '.cfs_add_field', function(){
		setTimeout(function(){
			updateLabelCFS();
		});
	});

	function updateLabelCFS(){
		jQuery('.loop_wrapper').each(function(){
			jQuery(this).find('.label').text( jQuery(this).find('[name*="cfs[input]"]').val() );
		});
	}
});