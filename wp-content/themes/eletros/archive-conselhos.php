<!--?php /* Template name: Template Conselho */ ?-->
<?php if(wp_get_current_user()->roles[0] == 'administrator' || wp_get_current_user()->roles[0] == 'subscriber'): ?>
<?php get_header(); get_template_part('partials/content','top'); ?>
 <section class="bg-eeeeee">
        <div>
            <?php eletros_breadcrumbs(); ?>
            <div style="padding-bottom:50px;"></div>
        </div>
        <?php //$args = array('post_type'=>'conselhos'); query_posts($args); ?>
        <div class="container">
            <div class="row tabs tab-vertical">
                <div class="col-sm-3 tab-col-left">
                    <ul>
                    <?php $c=-1; while (have_posts()): the_post(); $c++; ?>
                        <li class="<?php echo $c=='0' ? 'active' : null; ?>"><a data-toggle="pill" href="#tab<?php echo $post->ID; ?>"><?php the_title(); ?></a></li>
                       
                    <?php endwhile; ?>
                    </ul>
                </div>
                <div class="col-sm-9 tab-col-right font-zoom">
                    <div class="tab-content">
                          <?php $j=-1; while (have_posts()): the_post(); $j++; ?>
                        <div id="tab<?php echo $post->ID; ?>" class="tab-pane fade <?php echo $j == '0' ? 'in' : null;?> active">
                            <div class="area-conselho">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="box-conselho">
                                            <div class="box-header">
                                                <h4><?php the_title(); ?></h4>
                                            </div>
                                            <div class="box-body">
                                                <ul>
                                                    <li><a href="<?php bloginfo('url'); ?>/atas?conselho=<?php echo $post->post_name; ?>">Reuniões</a></li>
                                                    <?php if(get_field('material') != ''): ?>
                                                        <li><a download href="<?php echo get_field('material'); ?>">Material de Reunião Mensal</a></li>
                                                    <?php endif; ?>
                                                    
                                                    <li><a href="<?php the_permalink(); ?>">Candelário de Reuniões</a></li>
                                                    <?php if(get_field('regimento') != ''): ?>
                                                        <li><a download href="<?php echo get_field('regimento'); ?>">Regimento Interno</a></li>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                        
                            </div>
                        </div><!-- end tab content -->
                       <?php endwhile;  ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script>
    $(document).ready(function(){
        $('.item-home a').text('Inicial').prop('href','/conselhos');
    });
</script>
    <!-- /content -->
<?php get_footer(); else: wp_redirect(home_url()); exit(); endif; ?>