<?php get_header();  get_template_part('partials/content','top'); $p = get_post(481);  ?>
<!-- content -->
<section class="bg-eeeeee">
    <div class="nossos-historia">

      <?php eletros_breadcrumbs(); ?>

        <div style="padding-bottom:50px;"></div>
        <div class="investimentos">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-push-8">
                        <div class="publicacao relatorio-anual">
                           <?php echo $p->post_content; ?>
                        </div>
                    </div>
                    <div class="col-sm-8 col-sm-pull-4">
                        <h2><?php echo $p->post_excerpt; ?></h2>
                        <form class="form-horizontal" role="form">
                            <div class="form-group input-search">
                                <div class="col-sm-4">
                                     <?php wp_dropdown_categories(['class'=>'form-control','id'=>'filter','hide_if_empty'=>true,'taxonomy'=>'publicacoes_category','show_option_none'=>'Selecionar','option_none_value'  => '-1','selected'=>$_GET['c']]); ?> 
                                </div>
                            </div>
                        </form>
                        <hr>
                        <div class="row">
                        <?php 
                        if(isset($_GET['c'])):
                            $cat = $_GET['c']; 
                           query_posts( array(  
                                'post_type' => 'publicacoes', 
                             
                                'posts_per_page' => 80, 
                                'tax_query' => array( 
                                    array( 
                                        'taxonomy' => 'publicacoes_category', //or tag or custom taxonomy
                                        'field' => 'id', 
                                        'terms' => array($cat) 
                                    ) 
                                ) 
                            ) );
                        endif;

                        while(have_posts()) : the_post(); ?>
                            <div class="col-md-3 col-sm-4 publicacao">
                                <h4><?php the_title(); ?></h4>
                                <?php get_the_image(['size'=>'publicacoes_thumb','link_to_post'=>false]); ?>

                                
                                <?php if (get_field('arquivo')) { ?>
                                    <a href="<?php echo get_field('arquivo')['url']; ?>" class="btn-pdf" download>Download</a>
                                <?php } else if (get_field('link_externo')) { ?>
                                    <a href="<?php echo get_field('link_externo'); ?>" target="_blank" class="btn-external-link">Acesse aqui</a>
                                <?php } ?>

                            </div>
                        <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /content -->
<script>
        $(document).ready(function(){
            $('select#filter').change(function(){
                if($(this).val() == '-1'){
                     window.location.href = '<?php bloginfo('url'); ?>/publicacoes';
                }else{
                     window.location.href = '<?php bloginfo('url'); ?>/publicacoes?c=' + $(this).val();
                }
               
            });
        });
    </script>

<?php get_footer(); ?>