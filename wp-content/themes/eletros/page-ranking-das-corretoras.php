<!--?php /* Template name: Template padrão de página */ ?-->
<?php get_header(); the_post(); $p = get_post($post->ID); ?>

<!-- topo -->
<section>
    <div class="topo" style="background-image: url(<?php bloginfo('template_url'); ?>/img/bg-topo.png);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 content">
                    <div class="text">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /topo -->

<!-- content -->
<section>
 <?php eletros_breadcrumbs(); ?>
    <div style="padding-bottom:50px;"></div>
    <div class="<?php echo $p->post_name;  ?>">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <!-- <?php the_content(); ?> -->






















<p>A habilitação das corretoras para intermediarem as operações de mercados ações, futuros e derivativos se dá em duas etapas.</p>
                    <ul>
                        <li><b>Pré-qualificação:</b>   Para realizarem a pré-qualificação, as corretoras devem ter operado pelo menos 0,5% do volume total negociado na BMFBovespa (Mercado à vista e de Opções) nos últimos 12 meses contados a partir da data de avaliação, não terem histórico de violação de decoro político ou fraude, além das respostas fornecidas no QUESTIONÁRIO (abaixo) serem consideradas validadas de acordo com critérios internos previamente estabelecidos.</li>
                        <li><b>Seleção de Corretoras:</b> Anualmente, a ELETROS avalia as corretoras pré-qualificadas pelo critério acima e selecionam as instituições pelas quais irá operar renda variável, futuros e derivativos nos próximos dois semestres do ano subsequente. A avaliação leva em consideração a qualidade dos serviços operacionais, de análise e de devolução de corretagem ofertados por cada instituição.</li>
                    </ul>
                    <p>O prazo para recebimento dos questionários é até 02/12/2016. <br><br></p>

                    <h3>DADOS GERAIS DA INSTITUIÇÃO</h3>
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>1) Nome da Instituição</label>
                                    [text* Nome-da-Instituicao class:form-control]
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>2) CNPJ:</label>
                                    [text* CNPJ class:form-control]
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>3) Relacione os nomes de todos os sócios/diretores da corretora e forneça um breve currículo dos mesmos:</label>
                                    [textarea Nomes-de-todos-os-socios-diretores-da-corretora class:form-control rows:10]
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>4) Relacione os profissionais ligados às atividades operacionais (gerentes e pessoal da mesa) que prestam serviços aos clientes (PJ) na BM&FBovespa, tanto no mercado à vista quanto no mercado futuro, apresentando ao lado um breve currículo destes, indicando se são autônomos ou funcionários contratados:</label>
                                    [textarea Numero-na-BM-FBovespa class:form-control rows:10]
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label>5) Relacione o(s) operador(es) que seria(m) indicado(s) para eventualmente atender(em) a ELETROS. Informe quanto tempo está nesta posição. Forneça ao lado de cada nome, o tempo de trabalho, os telefones e e-mails de cada um.</label>
                                <button type="button" class="btn btn-blue-2" data-toggle="modal" data-target="#addModal">Novo</button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <table id="list" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">
                                                Nome
                                            </th>
                                            <th scope="col">
                                                TempoTrabalhado
                                            </th>
                                            <th scope="col">
                                                Emails
                                            </th>
                                            <th scope="col">
                                                Telefones
                                            </th>
                                            <th scope="col">
                                                &nbsp;
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>

                            </div>
                        </div>

                        <h3><br><br> ESTRUTURA OPERACIONAL DA INSTITUIÇÃO</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>6) Possui sistema de gravação na mesa de operações?</label>
                                    [radio Possui-sistema-de-gravacao-na-mesa-de-operacoes label_first use_label_element default:2 "Sim" "Não"]
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>7) A ELETROS exige que as ordens de bolsa enviadas à determinada instituição sejam sempre executadas pela própria instituição que a recebeu. Desta forma pergunta-se: as ordens da ELETROS serão sempre executadas sem repasse a terceiros?</label>
                                    [radio As-ordens-da-ELETROS-serao-sempre-executadas-sem-repasse-a-terceiros label_first use_label_element default:2 "Sim" "Não"]
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>8) Qual dos tipos abaixo a corretora se enquadra?</label>
                                    [radio Qual-dos-tipos-abaixo-a-corretora-se-enquadra label_first use_label_element "Participante de Negociação Pleno (PNP)" "Participante de Negociação (PN)"]
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>9) Aceita enviar à ELETROS, sempre que requisitada, parciais das operações realizadas ao longo dia?</label>
                                    [radio Aceita-envia-a-ELETROS-sempre-que-requisitada-parciais-das-operacoes-realizadas-ao-longo-dia label_first use_label_element default:2 "Sim" "Não"]
                                </div>
                            </div>
                        </div>

                        <h3>METODOLOGIA DE TRABALHO</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>10)  Possui carteira própria? Caso positivo descreva, sucintamente, os mecanismos de mitigação de conflitos de interesses com as ordens dos clientes.</label>
                                    <div class="radio">
                                        <label><input type="radio" name="radio4" value="sim">Sim</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="radio4" value="nao" checked>Não</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    [textarea Possui-carteira-propria class:form-control rows:10 disabled]
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>11)  É permitido aos corretores da instituição operar para si ou para pessoas a eles relacionadas? Caso positivo descreva, sucintamente, os mecanismos de mitigação de conflito de interesses com as ordens dos clientes:</label>
                                    <div class="radio">
                                        <label><input type="radio" name="radio5" value="sim">Sim</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="radio5" value="nao" checked>Não</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    [textarea E-permitido-aos-corretores-da-instituicao-operar-para-si-ou-para-pessoas-a-eles-relacionadas class:form-control rows:10 disabled:disabled]
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>12)  O sistema de remuneração variável dos operadores é individual?</label>
                                    [radio O-sistema-de-remuneracao-variavel-dos-operadores-e-individual use_label_element default:2 "Sim" "Não"]
                                </div>
                            </div>
                        </div>

                        <h3>METODOLOGIA DE TRABALHO</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>13)  Pertence a um grupo empresarial nacional ou internacional? Em caso positivo, cite-o:</label>
                                    <div class="radio">
                                        <label><input type="radio" name="radio7" value="sim">Sim</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="radio7" value="nao" checked>Não</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    [text Pertence-a-um-grupo-empresarial-nacional-ou-internacional class:form-control disabled:disabled]
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>14)  Qual o patrimônio líquido da corretora? Favor enviar cópia do último Balanço Patrimonial auditado em pdf:</label>
                                    [text Qual-o-patrimonio-liquido-da-corretora class:form-control]
                                    <div style="margin-top: 10px;">
                                        [file file-208 Balanco-Patrimonial-auditado filetypes:pdf]
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>15)  Possui certificados e/ou selos de qualidade (ISO etc)? Quais?</label>
                                    [textarea Possui-certificados-e-ou-selos-de-qualidade-ISO-etc class:form-control rows:10 disabled:disabled]
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group check-file">
                                    <label>16)  Possui Código de Ética? Em caso positivo, envie uma cópia do arquivo em PDF. </label>
                                    [radio Possui-Codigo-de-Etica label_first use_label_element default:2 "Sim" "Não"]
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>17)  Qual o tempo de atuação da corretora na BM&FBovespa, tanto no mercado à vista quanto no mercado futuro?</label>
                                    [radio Qual-o-tempo-de-atuacao-da-corretora-na-BMFBovespa-tanto-no-mercado-a-vista-quanto-no-mercado-futuro label_first use_label_element default:1 "Menos de 2 anos" "Entre 2 e 5 anos" "Entre 5 e 10 anos" "Mais de 10 anos"]
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>18)  Qual seria a devolução de corretagem praticada nas operações de renda variável da ELETROS?</label>
                                    [text Qual-seria-a-devolucao-de-corretagem-praticada-nas-operacoes-de-renda-variavel-da-ELETROS class:form-control]
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>19) Qual seria a devolução de corretagem praticada nas operações de derivativos (DI Futuro e Dólar)?</label>
                                    [text Qual-seria-a-devolucao-de-corretagem-praticada-nas-operacoes-de-derivativos-DI-Futuro-e-Dolar class:form-control]
                                </div>
                            </div>
                        </div>

                        <p>Nota: A ELETROS exige que seja enviado parciais das operações realizadas ao longo do dia. Este item é obrigatório, caso a corretora não o cumpra será retirada do ranking.</p>

                        <button type="submit" class="btn btn-blue-2" style="margin-right: 10px;">Enviar</button>
                        <a href="javascript: history.back(-1);" class="btn btn-default">Voltar</a>

                        <div style="margin-bottom: 100px;"></div>

                    </form>

                    <div id="addModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Novo</h4>
                                </div>
                                <div class="modal-body">
                                    <div id="novo">
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                    <label>Nome:</label>
                                                    <input type="text" class="form-control" name="nome">
                                                </div>
                                                <div class="form-group">
                                                    <label>Emails:</label>
                                                    <textarea class="form-control" rows="4" name="emails"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label>Tempo de Trabalho:</label>
                                                    <input type="text" class="form-control" name="tempo_de_trabalho">
                                                </div>
                                                <div class="form-group">
                                                    <label>Telefones:</label>
                                                    <textarea class="form-control" rows="4" name="telefones"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="cancelInfo" class="btn btn-default">Cancelar</button>
                                    <button type="button" id="saveInfo" class="btn btn-blue-2">Salvar</button>
                                </div>
                            </div>
                        </div>
                    </div>


























                </div>
            </div>
        </div>
    </div>
</section>
<!-- /content -->

<?php get_footer(); ?>