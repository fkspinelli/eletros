 <div class="ultimas-noticias">
    <h2>Últimas Notícias</h2>
    
    <ul>
    <?php

    
        $the_query = new WP_Query( array (  
        'post_type' => 'noticias' ,
        'posts_per_page' => '4',
        'post__not_in' => array($post->ID)
         ) );

        while($the_query->have_posts()) : $the_query->the_post(); $cats = getMainCategory($post->ID, 'notice_category');
        ?>
        <li>
            <a href="<?php the_permalink(); ?>">
                <div class="box-label">
                    <div style="background-color:<?php echo get_field('cor_categoria','notice_category_'.$cats[0]['id']); ?>;" class="label <?php echo $cats[0]['slug']; ?>"><?php echo $cats[0]['name']; ?></div>
                </div>
                <?php the_title(); ?>
                <div class="link">Leia mais <i class="fa fa-chevron-circle-right"></i></div>
            </a>
        </li>
       <?php endwhile; wp_reset_query(); ?>
    </ul>
    <div class="row">
        <div class="col-sm-12">
            <a href="<?php bloginfo('url'); ?>/noticias" class="btn btn-gray-1 btn-block text-uppercase">Veja todas as notícias</a>
        </div>
    </div>
</div>