<div class="ultimas-noticias">
    <h2>Últimas Notícias</h2>
    <ul>
        <?php

        $cat1 = get_category_by_slug('');
        $id1 = $cat1->term_id;

        $the_query = new WP_Query( array (  
        'post_type' => 'noticias' ,
        'posts_per_page' => 3,
        'post__not_in' => $featured
         ) );

        while($the_query->have_posts()) : $the_query->the_post();
        ?>
            <li>
                <a href="<?php the_permalink(); ?>">
                    <div class="box-label">
                        <?php
                            $post_categories = get_the_terms($post->ID, 'notice_category');

                            $cats = array();
                            foreach($post_categories as $c){
                                $cat = get_category( $c );
                                $cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug );
                                if(($cat->slug != 'sem-categoria') && $cat->slug != 'noticias' && $cat->slug != 'destaque'){
                                  
                                    ?><div style="background-color:<?php echo get_field('cor_categoria','notice_category_'.$cat->term_id); ?>;" class="label <?php echo $cat->slug; ?>"><?php echo $cat->name; ?></div><?php
                                }
                            }
                        ?>
                    </div>
                    <?php the_title(); ?>
                    <div class="link">Leia mais <i class="fa fa-chevron-circle-right"></i></div>
                </a>
            </li>
        <?php
        endwhile;
        wp_reset_query();
        ?>
    </ul>
    <div class="row">
        <div class="col-sm-6">
            <a href="<?php bloginfo('url'); ?>/noticias" class="btn btn-gray-1 btn-block text-uppercase">Veja todas as notícias</a>
        </div>
    </div>
</div>