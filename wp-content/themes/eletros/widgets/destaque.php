<?php $a_dest = array('post_type'=>'noticias', 'posts_per_page'=>'2','tax_query'   => array(
        array(
            'taxonomy'  => 'notice_category',
            'field'     => 'slug',
            'terms'     => 'destaque', // exclude media posts in the news-cat custom taxonomy
            'operator'  => 'IN')
            ),
       ); 
query_posts($a_dest); ?>
<div class="destaque">
    <h2>Em Destaque</h2>
    <div class="row">
        <?php while (have_posts()): the_post(); $cats = getMainCategory($post->ID, 'notice_category');
            $featured[] = $post->ID;
        ?>
        <div class="col-sm-6">
            <div style="background-color:<?php echo get_field('cor_categoria','notice_category_'.$cats[0]['id']); ?>;" class="box-destaque <?php echo $cats[0]['slug']; ?>">
                <a href="<?php the_permalink();?>">
            
                    <div style="background-color:<?php echo get_field('cor_categoria','notice_category_'.$cats[0]['id']); ?>;" class="label absolute"><?php echo $cats[0]['name']; ?></div>
                     <?php echo get_the_image(['size'=>'med', 'link_to_post'=> false]); ?>
                    <div class="text">
                       <?php the_title();?>
                        <div class="link">Leia mais <i class="fa fa-chevron-circle-right"></i></div>
                    </div>
                </a>
            </div>
        </div>
        <?php endwhile; wp_reset_query(); ?>
       
    </div>
</div>
