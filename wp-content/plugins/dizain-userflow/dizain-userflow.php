<?php
/**
 * Plugin Name:       Dizain Userflow Login
 * Description:       A plugin that replaces the WordPress login flow with a custom page.
 * Version:           1.0.0
 * Author:            Herculano Chaves
 * License:           GPL-2.0+
 * Text Domain:       dizain-userflow-plugin
 */
 
class Personalize_Login_Plugin {
 
    /**
     * Initializes the plugin.
     *
     * To keep the initialization fast, only add filter and action
     * hooks in the constructor.
     */
    public function __construct() {

    	add_shortcode( 'custom-login-form', array( $this, 'render_login_form' ) );

    	add_action( 'login_form_login', array( $this, 'redirect_to_custom_login' ) );

     	add_action( 'login_form_lostpassword', array( $this, 'redirect_to_custom_lostpassword' ) );

     	add_shortcode( 'custom-password-lost-form', array( $this, 'render_password_lost_form' ) );

     	add_action( 'login_form_lostpassword', array( $this, 'do_password_lost' ) );

     	add_action( 'login_form_rp', array( $this, 'redirect_to_custom_password_reset' ) );


		add_action( 'login_form_resetpass', array( $this, 'redirect_to_custom_password_reset' ) );

		add_shortcode( 'custom-password-reset-form', array( $this, 'render_password_reset_form' ) );

		add_action( 'login_form_rp', array( $this, 'do_password_reset' ) );
		add_action( 'login_form_resetpass', array( $this, 'do_password_reset' ) );

		add_filter( 'retrieve_password_message', array( $this, 'replace_retrieve_password_message' ), 10, 4 );

		add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));



    }
  
    /**
	 * Plugin activation hook.
	 *
	 * Creates all WordPress pages needed by the plugin.
	 */
	public static function plugin_activated() {
	    // Information needed for creating the plugin's pages
	    $page_definitions = array(
	        'member-login' => array(
	            'title' => __( 'Login', 'personalize-login' ),
	            'content' => '[custom-login-form]'
	        ),
	       
	        'esqueci-senha' => array(
		        'title' => __( 'Esqueci minha senha', 'personalize-login' ),
		        'content' => '[custom-password-lost-form]'
		    ),
		    'resetar-senha' => array(
		        'title' => __( 'Gerar nova senha', 'personalize-login' ),
		        'content' => '[custom-password-reset-form]'
		    )
	    );
	 
	    foreach ( $page_definitions as $slug => $page ) {
	        // Check that the page doesn't exist already
	        $query = new WP_Query( 'pagename=' . $slug );
	        if ( ! $query->have_posts() ) {
	            // Add the page using the data from the array above
	            wp_insert_post(
	                array(
	                    'post_content'   => $page['content'],
	                    'post_name'      => $slug,
	                    'post_title'     => $page['title'],
	                    'post_status'    => 'publish',
	                    'post_type'      => 'page',
	                    'ping_status'    => 'closed',
	                    'comment_status' => 'closed',
	                )
	            );
	        }
	    }

	}

	/**
	 * Redirects the user to the custom "Forgot your password?" page instead of
	 * wp-login.php?action=lostpassword.
	 */
	public function redirect_to_custom_lostpassword() {
	    if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
	        if ( is_user_logged_in() ) {
	            $this->redirect_logged_in_user();
	            exit;
	        }
	 
	        wp_redirect( home_url( 'esqueci-senha' ) );
	        exit;
	    }
	}

	/**
	 * Redirect the user to the custom login page instead of wp-login.php.
	 */
	function redirect_to_custom_login() {
	    if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
	        $redirect_to = isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : null;
	     
	        if ( is_user_logged_in() ) {
	            $this->redirect_logged_in_user( $redirect_to );
	            exit;
	        }
	 
	        // The rest are redirected to the login page
	        $login_url = home_url( 'member-login' );
	        if ( ! empty( $redirect_to ) ) {
	            $login_url = add_query_arg( 'redirect_to', $redirect_to, $login_url );
	        }
	 
	        wp_redirect( $login_url );
	        exit;
	    }
	}

	/**
	 * A shortcode for rendering the login form.
	 *
	 * @param  array   $attributes  Shortcode attributes.
	 * @param  string  $content     The text content for shortcode. Not used.
	 *
	 * @return string  The shortcode output
	 */
	public function render_login_form( $attributes, $content = null ) {
	    // Parse shortcode attributes
	    $default_attributes = array( 'show_title' => false );
	    $attributes = shortcode_atts( $default_attributes, $attributes );
	    $show_title = $attributes['show_title'];
	 // Check if user just updated password
$attributes['password_updated'] = isset( $_REQUEST['password'] ) && $_REQUEST['password'] == 'changed';
	    if ( is_user_logged_in() ) {
	        return __( 'Você está logado com sucesso.', 'personalize-login' );
	    }
	     
	    // Pass the redirect parameter to the WordPress login functionality: by default,
	    // don't specify a redirect, but if a valid redirect URL has been passed as
	    // request parameter, use it.
	    $attributes['redirect'] = '';
	    if ( isset( $_REQUEST['redirect_to'] ) ) {
	        $attributes['redirect'] = wp_validate_redirect( $_REQUEST['redirect_to'], $attributes['redirect'] );
	    }
	     
	    // Render the login form using an external template
	    return $this->get_template_html( 'login_form', $attributes );
	}

	/**
	 * A shortcode for rendering the form used to initiate the password reset.
	 *
	 * @param  array   $attributes  Shortcode attributes.
	 * @param  string  $content     The text content for shortcode. Not used.
	 *
	 * @return string  The shortcode output
	 */
	public function render_password_lost_form( $attributes, $content = null ) {
	    // Parse shortcode attributes
	    $default_attributes = array( 'show_title' => false );
	    $attributes = shortcode_atts( $default_attributes, $attributes );

	    // Retrieve possible errors from request parameters
		$attributes['errors'] = array();
		if ( isset( $_REQUEST['errors'] ) ) {
		    $error_codes = explode( ',', $_REQUEST['errors'] );
		 
		    foreach ( $error_codes as $error_code ) {
		        $attributes['errors'][] = $this->get_error_message( $error_code );
		    }
		}
	 
	    if ( is_user_logged_in() ) {
	        return __( 'Você está logado com sucesso.', 'personalize-login' );
	    } else {
	    	// Check if the user just requested a new password 
		$attributes['lost_password_sent'] = isset( $_REQUEST['checkemail'] ) && $_REQUEST['checkemail'] == 'confirm';

	        return $this->get_template_html( 'password_lost_form', $attributes );
	    }
	}

	/**
	 * A shortcode for rendering the form used to reset a user's password.
	 *
	 * @param  array   $attributes  Shortcode attributes.
	 * @param  string  $content     The text content for shortcode. Not used.
	 *
	 * @return string  The shortcode output
	 */
	public function render_password_reset_form( $attributes, $content = null ) {
	    // Parse shortcode attributes
	    $default_attributes = array( 'show_title' => false );
	    $attributes = shortcode_atts( $default_attributes, $attributes );
	 
	    if ( is_user_logged_in() ) {
	        return __( 'Você está logado com sucesso.', 'personalize-login' );
	    } else {
	        if ( isset( $_REQUEST['login'] ) && isset( $_REQUEST['key'] ) ) {
	            $attributes['login'] = $_REQUEST['login'];
	            $attributes['key'] = $_REQUEST['key'];
	 
	            // Error messages
	            $errors = array();
	            if ( isset( $_REQUEST['error'] ) ) {
	                $error_codes = explode( ',', $_REQUEST['error'] );
	 
	                foreach ( $error_codes as $code ) {
	                    $errors []= $this->get_error_message( $code );
	                }
	            }
	            $attributes['errors'] = $errors;
	 
	            return $this->get_template_html( 'password_reset_form', $attributes );
	        } else {
	            return __( 'Link para gerar senha inválido.', 'personalize-login' );
	        }
	    }
	}

	/**
	 * Returns the message body for the password reset mail.
	 * Called through the retrieve_password_message filter.
	 *
	 * @param string  $message    Default mail message.
	 * @param string  $key        The activation key.
	 * @param string  $user_login The username for the user.
	 * @param WP_User $user_data  WP_User object.
	 *  
	 * @return string   The mail message to send.
	 */
	public function replace_retrieve_password_message( $message, $key, $user_login, $user_data ) {
	    // Create new message
	    $msg  =  'Redefinição de Senha – Área do Conselho Eletros <br/><br/>';
	    $msg  .='Olá, </br></br>';
	    $msg  .= 'Seguem as informações solicitadas para recuperar seu acesso à Área do Conselho do site eletros.com.br <br/><br/>';
	    $msg  .= sprintf( __( 'CPF/Login: %s.', 'personalize-login' ), $user_login ) .'<br/>';

	    $msg  .= 'Senha: <a href="'.site_url( "wp-login.php?action=rp&key=$key&login=".rawurlencode( $user_login )).'">Clique aqui para criar uma nova senha</a> <br/><br/>';

	    $msg  .= 'Por motivos de segurança, o link acima só será válido durante as próximas 24 horas. <br/><br/>';
	    $msg  .= 'Se você não deseja redefinir sua senha ou não solicitou estas informações, pode ignorar este e-mail com segurança. <br/><br/>';
	    $msg  .= '________________________________________ <br/><br/><br/>';
	   
	    $msg .= 'Por favor, não responda esta mensagem. Em caso de dúvidas, acesse os nossos Canais de Atendimento. <br/>';
	 
	    return $msg;
	}

	/**
	 * Resets the user's password if the password reset form was submitted.
	 */
	public function do_password_reset() {
	    if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
	        $rp_key = $_REQUEST['rp_key'];
	        $rp_login = $_REQUEST['rp_login'];
	 
	        $user = check_password_reset_key( $rp_key, $rp_login );
	 
	        if ( ! $user || is_wp_error( $user ) ) {
	            if ( $user && $user->get_error_code() === 'expired_key' ) {
	                wp_redirect( home_url( 'member-login?login=expiredkey' ) );
	            } else {
	                wp_redirect( home_url( 'member-login?login=invalidkey' ) );
	            }
	            exit;
	        }
	 
	        if ( isset( $_POST['pass1'] ) ) {
	            if ( $_POST['pass1'] != $_POST['pass2'] ) {
	                // Passwords don't match
	                $redirect_url = home_url( 'resetar-senha' );
	 
	                $redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
	                $redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
	                $redirect_url = add_query_arg( 'error', 'password_reset_mismatch', $redirect_url );
	 
	                wp_redirect( $redirect_url );
	                exit;
	            }
	 
	            if ( empty( $_POST['pass1'] ) ) {
	                // Password is empty
	                $redirect_url = home_url( 'resetar-senha' );
	 
	                $redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
	                $redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
	                $redirect_url = add_query_arg( 'error', 'password_reset_empty', $redirect_url );
	 
	                wp_redirect( $redirect_url );
	                exit;
	            }
	 
	            // Parameter checks OK, reset password
	            reset_password( $user, $_POST['pass1'] );

	            // Aqui eu logo e redireciono
	           	wp_clear_auth_cookie();
			    wp_set_current_user ( $user->ID );
			    wp_set_auth_cookie  ( $user->ID );
	          
	            wp_redirect(home_url('conselho'));
	        } else {
	            echo "Requisição inválida.";
	        }
	 
	        exit;
	    }
	}


	/**
	 * Redirects to the custom password reset page, or the login page
	 * if there are errors.
	 */
	public function redirect_to_custom_password_reset() {
	    if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
	        // Verify key / login combo
	        $user = check_password_reset_key( $_REQUEST['key'], $_REQUEST['login'] );
	        if ( ! $user || is_wp_error( $user ) ) {
	            if ( $user && $user->get_error_code() === 'expired_key' ) {
	                wp_redirect( home_url( 'member-login?login=expiredkey' ) );
	            } else {
	                wp_redirect( home_url( 'member-login?login=invalidkey' ) );
	            }
	            exit;
	        }
	 
	        $redirect_url = home_url( 'resetar-senha' );
	        $redirect_url = add_query_arg( 'login', esc_attr( $_REQUEST['login'] ), $redirect_url );
	        $redirect_url = add_query_arg( 'key', esc_attr( $_REQUEST['key'] ), $redirect_url );
	 
	        wp_redirect( $redirect_url );
	        exit;
	    }
	}

	/**
	 * Initiates password reset.
	 */
	public function do_password_lost() {
	    if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
	        $errors = retrieve_password();
	        if ( is_wp_error( $errors ) ) {
	            // Errors found
	            $redirect_url = home_url( 'esqueci-senha' );
	            $redirect_url = add_query_arg( 'errors', join( ',', $errors->get_error_codes() ), $redirect_url );
	        } else {
	            // Email sent
	            $redirect_url = home_url( 'esqueci-senha' );
	            $redirect_url = add_query_arg( 'checkemail', 'confirm', $redirect_url );
	        }
	 
	        wp_redirect( $redirect_url );
	        exit;
	    }
	}

	/**
	 * Renders the contents of the given template to a string and returns it.
	 *
	 * @param string $template_name The name of the template to render (without .php)
	 * @param array  $attributes    The PHP variables for the template
	 *
	 * @return string               The contents of the template.
	 */
	private function get_template_html( $template_name, $attributes = null ) {
	    if ( ! $attributes ) {
	        $attributes = array();
	    }
	 
	    ob_start();
	 
	    do_action( 'personalize_login_before_' . $template_name );
	 
	    require( 'templates/' . $template_name . '.php');
	 
	    do_action( 'personalize_login_after_' . $template_name );
	 
	    $html = ob_get_contents();
	    ob_end_clean();
	 
	    return $html;
	}
	/**
	 * Redirects the user to the correct page depending on whether he / she
	 * is an admin or not.
	 *
	 * @param string $redirect_to   An optional redirect_to URL for admin users
	 */
	private function redirect_logged_in_user( $redirect_to = null ) {
	    $user = wp_get_current_user();
	    if ( user_can( $user, 'manage_options' ) ) {
	        if ( $redirect_to ) {
	            wp_safe_redirect( $redirect_to );
	        } else {
	            wp_redirect( admin_url() );
	        }
	    } else {
	        wp_redirect( home_url( 'member-account' ) );
	    }
	}

	/**
	 * Finds and returns a matching error message for the given error code.
	 *
	 * @param string $error_code    The error code to look up.
	 *
	 * @return string               An error message.
	 */
	private function get_error_message( $error_code ) {
	    switch ( $error_code ) {
	        case 'empty_username':
	            return __( 'You do have an email address, right?', 'personalize-login' );
	 
	        case 'empty_password':
	            return __( 'You need to enter a password to login.', 'personalize-login' );
	 
	        case 'invalid_username':
	            return __(
	                "We don't have any users with that email address. Maybe you used a different one when signing up?",
	                'personalize-login'
	            );
	 
	        case 'incorrect_password':
	            $err = __(
	                "The password you entered wasn't quite right. <a href='%s'>Did you forget your password</a>?",
	                'personalize-login'
	            );
	            return sprintf( $err, wp_lostpassword_url() );

	        // Lost password
 
			case 'empty_username':
			    return __( 'Você precisa digitar um e-mail para continuar.', 'personalize-login' );
			 
			case 'invalid_email':
			 return __( 'Você precisa digitar um e-mail válido para continuar.', 'personalize-login' );

			case 'invalidcombo':
			    return __( 'Não encontramos usuários regitrados com este endereço de email.', 'personalize-login' );
			// Reset password
 
			case 'expiredkey':
			 	return __( 'O link de ativação para gerar nova senha expirou, tente novamente.', 'personalize-login' );

			case 'invalidkey':
			    return __( 'O link de ativação para gerar nova senha, já não é mais válido, tente novamente.', 'personalize-login' );
			 
			case 'password_reset_mismatch':
			    return __( "Os campos de senha e confirmar senha não estão idênticos.", 'personalize-login' );
			     
			case 'password_reset_empty':
			    return __( "Desculpe, não aceitamos campos de senha vazios.", 'personalize-login' );
				 
	        default:
	            break;
	    }
	     
	    return __( 'An unknown error occurred. Please try again later.', 'personalize-login' );
	}
	     
}
 
// Initialize the plugin
$personalize_login_pages_plugin = new Personalize_Login_Plugin();


// Create the custom pages at plugin activation
register_activation_hook( __FILE__, array( 'Personalize_Login_Plugin', 'plugin_activated' ) );