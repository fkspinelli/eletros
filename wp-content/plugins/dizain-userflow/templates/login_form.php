<div class="login-form-container">
    <?php if ( $attributes['show_title'] ) : ?>
        <h2><?php _e( 'Sign In', 'personalize-login' ); ?></h2>
    <?php endif; ?>
    <?php if ( $attributes['password_updated'] ) : ?>
      <div class="alert alert-success">
        <p>
            Sua senha foi alterada com sucesso! Favor faça seu login com a nova senha
        </p>
        </div>

    <?php endif; ?>
     
    <?php
        // wp_login_form(
        //     array(
        //         'label_username' => __( 'Email', 'personalize-login' ),
        //         'label_log_in' => __( 'Sign In', 'personalize-login' ),
        //         'redirect' => $attributes['redirect'],
        //     )
        // );
    ?>

<h2>Faça seu login</h2>
<p><?php _e( 'Entre com seu cpf e senha.', 'personalize-login' ); ?></p>
<div style="margin-bottom: 40px;"></div>

<form name="loginform" id="loginform" action="/wp-login.php" method="post" autocomplete="off">
    <div class="row">
        <div class="col-sm-4">
            <input type="text" name="log" id="user_login" class="input form-control cpfuser" value="" size="20" placeholder="CPF" autocomplete="off">
        </div>
        <div class="col-sm-4">
            <input type="password" name="pwd" id="user_pass" class="input form-control" value="" size="20" placeholder="Senha" autocomplete="off">
        </div>
        <div class="col-sm-4">
            <button type="submit" name="wp-submit" id="wp-submit" class="button-primary btn btn-blue-1 btn-block text-uppercase">Entrar</button>
            <input type="hidden" name="redirect_to" value="<?php echo home_url('conselho'); ?>">
        </div>
    </div>
    <p class="login-remember"><br><label><input name="rememberme" type="checkbox" id="rememberme" value="forever"> Lembrar-me</label></p>
</form>

     
    <a class="forgot-password" href="<?php echo wp_lostpassword_url(); ?>">
        <?php _e( 'Esqueceu sua senha?', 'personalize-login' ); ?>
    </a>

    <div style="margin-bottom: 160px;"></div>
</div>