<?php if ( count( $attributes['errors'] ) > 0 ) : ?>
    <?php foreach ( $attributes['errors'] as $error ) : ?>
        <div class="alert alert-danger">
        <p>
            <?php echo $error; ?>
        </p>
        </div>

    <?php endforeach; ?>
<?php endif; ?>

<h2>Salve sua nova senha</h2>
<p>Utilize os campos abaixo para salvar uma nova senha.</p>
<div style="margin-bottom: 40px;"></div>

<div id="password-reset-form" class="">
    <?php if ( $attributes['show_title'] ) : ?>
        <h3><?php _e( 'Pick a New Password', 'personalize-login' ); ?></h3>
    <?php endif; ?>
 
    <form name="resetpassform" id="resetpassform" action="<?php echo site_url( 'wp-login.php?action=resetpass' ); ?>" method="post" autocomplete="off">
        <input type="hidden" id="user_login" name="rp_login" value="<?php echo esc_attr( $attributes['login'] ); ?>" autocomplete="off" />
        <input type="hidden" name="rp_key" value="<?php echo esc_attr( $attributes['key'] ); ?>" />
         
        <?php if ( count( $attributes['errors'] ) > 0 ) : ?>
            <?php foreach ( $attributes['errors'] as $error ) : ?>
                <p>
                    <?php echo $error; ?>
                </p>
            <?php endforeach; ?>
        <?php endif; ?>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <input type="password" name="pass1" id="pass1" class="form-control" size="20" value="" autocomplete="off" placeholder="<?php _e( 'NOVA SENHA', 'personalize-login' ) ?>" />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <input type="password" name="pass2" id="pass2" class="form-control" size="20" value="" autocomplete="off" placeholder="CONFIRMAR NOVA SENHA" />
                </div>
            </div>
            <div class="col-sm-4">
                <button type="submit" name="submit" id="resetpass-button" class="btn btn-blue-2 btn-block text-uppercase" style="padding: 8px 12px;"><?php _e( 'Salvar nova senha', 'personalize-login' ); ?></button>
            </div>
        </div>
         
        <!-- <p class="description"><?php echo wp_get_password_hint(); ?></p> -->
    </form>
    <div style="margin-bottom: 160px;"></div>
</div>