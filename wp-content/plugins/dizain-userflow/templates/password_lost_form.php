<div id="password-lost-form" class="widecolumn">
    
    <?php if ( $attributes['lost_password_sent'] ) : ?>
        <div class="alert alert-success">
          <?php _e( 'Verifique seu e-mail para ver um link para redefinir sua senha.', 'personalize-login' ); ?>
        </div>
    <?php endif; ?>
    

    <h2>Quero gerar uma nova senha</h2>
    <p>
        <?php
            _e(
                "Para redefinir sua senha, preencha os campos abaixo com o e-mail de cadastro e CPF. Você receberá as orientações por e-mail:",
                'personalize_login'
            );
        ?>
    </p>
    <div style="margin-bottom: 40px;"></div>
 
    <form id="lostpasswordform" action="<?php echo wp_lostpassword_url(); ?>" method="post">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <input type="text" name="user_login" id="user_login" class="form-control cpfuser" placeholder="<?php _e( 'CPF', 'personalize-login' ); ?>">
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <button type="submit" name="submit" class="btn btn-blue-2 btn-block text-uppercase" style="padding: 8px 12px;"><?php _e( 'Enviar', 'personalize-login' ); ?></button>
                </div>
            </div>
        </div>
    </form>
    <div style="margin-bottom: 160px;"></div>
</div>