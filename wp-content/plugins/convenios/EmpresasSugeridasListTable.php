<?php

class EmpresasSugeridasListTable extends WP_List_Table
{
    
    public static function get_sugestoes($per_page = 5, $page_number = 1)
    {
            
        global $wpdb;
            
        $sql = "SELECT * FROM {$wpdb->prefix}sugestao_empresa";
            
        if (! empty( $_REQUEST['orderby'] )) {
            $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
            $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
        }
            
        $sql .= " LIMIT $per_page";
            
        $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;
            
            
        $result = $wpdb->get_results( $sql, 'ARRAY_A' );
            
        return $result;
    }
    
    public static function record_count()
    {
        global $wpdb;
          
        $sql = "SELECT COUNT(*) FROM {$wpdb->prefix}sugestao_empresa";
          
        return $wpdb->get_var( $sql );
    }
    
    public function no_items()
    {
        _e( 'Nenhuma Sugestão.', 'sp' );
    }
    
    function get_columns()
    {
             
        $columns = [
      //'cb'      => '<input type="checkbox" />',
        'id'    => __( '#', 'sp' ),
        'nome'    => __( 'Nome', 'sp' ),
        'numero' => __( 'Número Eletros', 'sp' ),
        'empresa'    => __( 'Empresa', 'sp' ),
        'telefone'    => __( 'Telefone', 'sp' ),
        'email'    => __( 'E-mail', 'sp' ),
        'site'    => __( 'Site', 'sp' ),
        'created_at'    => __( 'Enviado em', 'sp' )
        ];
          
        return $columns;
    }
    
    public function get_hidden_columns()
    {
        return array();
    }
        
    public function get_sortable_columns()
    {
        return array('created_at' => array('created_at', false));
    }
    
    public function column_default($item, $column_name)
    {
        
        switch ($column_name) {
            case 'id':
            case 'nome':
            case 'numero':
            case 'empresa':
            case 'telefone':
            case 'email':
            case 'site':
            case 'created_at':
                return $item[ $column_name ];
                break;
                    
            default:
                return print_r( $item, true ) ;
        }
    }
    public function prepare_items()
    {
    
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();
    
        $this->_column_headers = array($columns, $hidden, $sortable);
        ;
            
        /** Process bulk action */
        //$this->process_bulk_action();
        
        $per_page     = $this->get_items_per_page( 'sugestoes_per_page', 5 );
        $current_page = $this->get_pagenum();
        $total_items  = self::record_count();
        
        $this->set_pagination_args( [
        'total_items' => $total_items, //WE have to calculate the total number of items
        'per_page'    => $per_page //WE have to determine how many items to show on a page
        ] );
    
            
        $this->items = self::get_sugestoes( $per_page, $current_page );
    }
}
