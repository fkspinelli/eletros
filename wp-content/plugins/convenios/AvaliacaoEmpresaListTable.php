<?php

class AvaliacaoEmpresaListTable extends WP_List_Table
{
    
    public static function get_avaliacoes($per_page = 5, $page_number = 1)
    {
            
        global $wpdb;
            
        $sql = "SELECT * FROM {$wpdb->prefix}ratings_eletros as r left join $wpdb->posts as p on p.ID = r.id_post ";
        
        if (! empty( $_REQUEST['orderby'] )) {
            $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
            $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
        } else {
            $sql .= ' ORDER BY created_at desc';
        }
            
        $sql .= " LIMIT $per_page";
            
        $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;
            
            
        $result = $wpdb->get_results( $sql, 'ARRAY_A' );
        
            
        return $result;
    }
    
    public static function record_count()
    {
        global $wpdb;
          
        $sql = "SELECT COUNT(*) FROM {$wpdb->prefix}ratings_eletros";
          
        return $wpdb->get_var( $sql );
    }
    
    public function no_items()
    {
        _e( 'Nenhuma Sugestão.', 'sp' );
    }
    
    
    function get_columns()
    {
             
        $columns = [
      //'cb'      => '<input type="checkbox" />',
        'id'    => __( '#', 'sp' ),
        'name'    => __( 'Nome', 'sp' ),
        'post_title'    => __( 'Empresa', 'sp' ),
        'eletros_number' => __( 'Número Eletros', 'sp' ),
        'description'    => __( 'Avaliação', 'sp' ),
        'rating'    => __( 'Pontos', 'sp' ),
        'enabled'   => __( 'Habilitado?', 'sp' ),
        'created_at'    => __( 'Enviado em', 'sp' )
        ];
          
        return $columns;
    }
    
    public function get_hidden_columns()
    {
        return array();
    }
        
    public function get_sortable_columns()
    {
        return array('created_at' => array('created_at', false));
    }

    public function column_id($item)
    {
        $actions = array(
            'delete' => sprintf('<a href="?page=%s&action=%s&id=%s">Remover</a>', $_REQUEST['page'], 'delete', $item['id']),
        );
        return sprintf('%1$s %2$s', $item['id'], $this->row_actions($actions));
    }

    public function column_enabled($item)
    {

        if (! (bool) $item['enabled']) {
            $actions = array(
                'Habilitar' => sprintf('<a href="?page=%s&action=%s&id=%s">Habilitar</a>', $_REQUEST['page'], 'enabled', $item['id']),
            );

            $valor = "Não";
        } else {
            $actions = array(
                'Desabilitar' => sprintf('<a href="?page=%s&action=%s&id=%s">Desabilitar</a>', $_REQUEST['page'], 'disabled', $item['id']),
            );
            $valor = "Sim";
        }
         
        return sprintf('%1$s %2$s', $valor, $this->row_actions($actions));
    }

    public function column_default($item, $column_name)
    {
        
        switch ($column_name) {
            case 'id':
            case 'name':
            case 'eletros_number':
            case 'description':
            case 'rating':
            case 'enabled':
            case 'post_title':
            case 'created_at':
                return $item[ $column_name ];
                break;
                    
            default:
                return print_r( $item, true ) ;
        }
    }
    public function prepare_items()
    {
    
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();
        
        $this->print_column_headers( false );

        $this->_column_headers = array($columns, $hidden, $sortable);
        ;
            
        /** Process bulk action */
        $this->process_bulk_action();
        
        $per_page     = $this->get_items_per_page( 'avaliacoes_per_page', 15 );
        $current_page = $this->get_pagenum();
        $total_items  = self::record_count();
        
        $this->set_pagination_args( [
        'total_items' => $total_items, //WE have to calculate the total number of items
        'per_page'    => $per_page //WE have to determine how many items to show on a page
        ] );
    
            
        $this->items = self::get_avaliacoes( $per_page, $current_page );
    }

    public function process_bulk_action()
    {
        global $wpdb;
        if ('delete' === $this->current_action()) {
            if (isset($_GET['id'])) {
                if (!empty($_GET['id'])) {
                    $id=$_GET['id'];

                    $table_name = $wpdb->prefix . 'ratings_eletros';
                    
                    $wpdb->query("delete from $table_name WHERE id =$id");
                }
            }
        }

        if ('enabled' === $this->current_action()) {
            
            if (isset($_GET['id'])) {
                if (!empty($_GET['id'])) {
                    $id=$_GET['id'];

                    $table_name = $wpdb->prefix . 'ratings_eletros';
                    
                    $wpdb->query("update $table_name set enabled=1 WHERE id =$id");
                }
            }
        }

        if ('disabled' === $this->current_action()) {
            if (isset($_GET['id'])) {
                if (!empty($_GET['id'])) {
                    $id=$_GET['id'];

                    $table_name = $wpdb->prefix . 'ratings_eletros';
                    
                    $wpdb->query("update $table_name set enabled=0 WHERE id =$id");
                }
            }
        }

        if ('export' === $this->current_action()) {
            $this->generate_csv();
        }
    }

    public function generate_csv() {
        global $wpdb;
        $csv_output = '';
        $table_name = $wpdb->prefix . 'ratings_eletros';
    
        $result = $wpdb->get_results("SHOW COLUMNS FROM $table_name", 'ARRAY_A' );
        
        $i = 0;
        foreach($result as $row) {
            $csv_output = $csv_output . $row['Field'] . ",";
            $i++;
        }
        
        $csv_output .= "\n";
    
        $values = $wpdb->get_results("SELECT * FROM $table_name order by created_at desc", 'ARRAY_A' );
        
        foreach($values as $row) {
            foreach($row as $column) {
                $csv_output = $csv_output . $column . ",";
            }
        }
        
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"report.csv\";");
        header("Content-Transfer-Encoding: binary");
  
        echo $csv_output;
        exit;
      }
}
