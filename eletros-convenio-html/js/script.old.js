// Grafico historia numeros

var planoDbEletrobrasData = [{value: 80, color: "#d08770", label: "Cepel"}, {value: 1, color: "#5b90bf", label: "Autopatrocinador"}, {value: 1, color: "#ebcb8b", label: "ELETROS"}, {value: 112, color: "#b48ead", label: "Eletrobrás"}, {value: 22, color: "#96b5b4", label: "BPD (Vesting)"} ];
var PlanoCdOnsDataData = [{value: 816, color: "#b48ead", label: "ONS"}, {value: 69, color: "#d08770", label: "Autopatrocinador"}, {value: 56, color: "#ebcb8b", label: "BPD (Vesting)"} ];
var PlanoCdEletrobrasData = [{value: 788, color: "#b48ead", label: "Eletrobrás"}, {value: 295, color: "#d08770", label: "Cepel"}, {value: 120, color: "#ebcb8b", label: "ELETROS"}, {value: 76, color: "#96b5b4", label: "BPD (Vesting)"}, {value: 55, color: "#5b90bf", label: "Autopatrocinador"} ];

window.onload = function() {
    var ctxPlanoDbEletrobras = document.getElementById("chart-plano-db-eletrobras").getContext("2d");
    window.myPolarArea = new Chart(ctxPlanoDbEletrobras).Doughnut(planoDbEletrobrasData, {
        responsive: true
    });

    var ctxPlanoCdOns = document.getElementById("chart-plano-cd-ons").getContext("2d");
    window.myPolarArea = new Chart(ctxPlanoCdOns).Doughnut(PlanoCdOnsDataData, {
        responsive: true
    });

    var ctxPlanoCdEletrobras = document.getElementById("chart-plano-cd-eletrobras").getContext("2d");
    window.myPolarArea = new Chart(ctxPlanoCdEletrobras).Doughnut(PlanoCdEletrobrasData, {
        responsive: true
    });
};

$(document).ready(function(){
    $(window).on('load resize', function(){
        if( $(window).width() > 992 ){
            $('header .header-bottom .menu-b > li').hover(function(){
                if($(this).find('ul').size() > 0){
                    $(this).find('ul').slideDown(150);
                }
                }, function(){
                $(this).find('ul').slideUp(150);
            });
        }else {
            $('header .header-bottom .menu-b > li').click(function(event){
                event.preventDefault();
                if($(this).find('ul').size() > 0){
                    if( $(this).hasClass('active') ){
                        $(this).find('ul').slideUp();
                        $(this).removeClass('active');
                    }else{
                        $(this).find('ul').slideDown();
                        $(this).addClass('active');
                    }
                }
            });
        }
    });

    // show menu mobile
    $('header .fa.fa-bars').on('click touchstart', function(){
        if($(this).hasClass('active')){
            $('header .menu-a ul').slideUp();
            $(this).removeClass('active');
        }else{
            $('header .menu-a ul').slideDown();
            $(this).addClass('active');
        }
    });

    $('.bxslider').bxSlider({
        minSlides: 4,
        maxSlides: 4,
        slideWidth: 300,
        slideMargin: 0,
        responsive:true,
        prevText: '<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>',
        nextText: '<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>'
    });

    // font zoom
    $('.tool-bar .more-font').click(function(e){
        e.preventDefault();
        $('.font-zoom *').each(function(){
            var size = parseInt($(this).css('font-size').replace('px',''));
            var lineHeight = parseInt($(this).css('line-height').replace('px',''));
            $(this).css({'font-size':size+1+"px", "line-height":lineHeight+2+"px"});
        });
    });
    $('.tool-bar .less-font').click(function(e){
        e.preventDefault();
        $('.font-zoom *').each(function(){
            var size = parseInt($(this).css('font-size').replace('px',''));
            var lineHeight = parseInt($(this).css('line-height').replace('px',''));
            $(this).css({'font-size':size-1+"px", "line-height":lineHeight-2+"px"});
        });
    });
    $('.tool-bar .more-font')

});