<html>
<head>
	<title>Votação Eletros</title>
	 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/votacao_app/public/css/bootstrap.min.css">
	<link rel="stylesheet" href="/votacao_app/public/css/style.css">
	<script type="text/javascript" src="/votacao_app/public/js/jquery.js"></script>
	<script type="text/javascript" src="/votacao_app/public/js/jquery.mask.min.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
	@yield('content')
</body>
</html>