@extends('layouts.app')
@section('content')
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<img src="https://www.multilinks.com.br/eletros2016/https/images/logo.png" alt="Eletros">
			</div>
		</div>
		<!--<hr style="border:1px solid #a5a5a5;">-->
		<div class="row" style="overflow: hidden;    height: 60px;">
			<div class="col-md-12">
				@include('partials.top')
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="row">
					<div class="col-md-12">
						<h1>Recuperar senha</h1>
						<p class="text-left">Caro Participante,</p>
						<p class="text-left">Para sua segurança, solicitamos preencher os dados abaixo para realizar a sua recuperação de senha.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form action="#" class="frmRecover" method="post">
							<div class="form-group">
								<label for="cpf">CPF</label>
								<input type="text" name="cpf" class="form-control">
							</div>
							<div class="form-group">
								<label for="birth">Data de nascimento</label>
								<input type="text" name="birth" class="form-control">
							</div>
							<div class="form-group">
								<label for="mother_name">Primeiro nome da mãe</label>
								<input type="text" name="mother_name" class="form-control">
							</div>
							<div class="form-group">
								<div class="g-recaptcha pull-right" style="margin-bottom: 10px;" data-sitekey="6LeXmwkUAAAAAGnyz_D7RhzkrT6ObuDbjsNDUVyz"></div>
							</div>
							<div class="form-group text-right">
								<input type="button" value="Enviar" style="padding: 10px 50px;" class="btn btn-enviar btn-primary">
							</div>
						</form>
						<form method="post" action="https://www.multilinks.com.br/eletros2016/https/esp01.cfm" id="dataForm">
							<input type="hidden" name="cod">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function(){
			$('input[name="cpf"]').mask('999.999.999-99');
			$('input[name="birth"]').mask('99/99/9999');

			$('.btn-enviar').click(function(){

				var formData = $('.frmRecover').serializeArray();
				
				$.ajax({
					url: '{{ url('recover') }}',
					data: {formData: formData},
					type: 'POST'
				}).done(function(data){
				
					if((data != '') && !data.error){
						$('input[name="cod"]').val(data);
						$('#dataForm').submit();
						return;
					}else if(data.error){
						alert(data.error+ 'Errro');
					}else{
						alert('Você deve inserir corretamente os seus dados. Tente novamente');
					}

					

				});
			});
			
		});
	</script>
@endsection