<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Contracts\Routing\ResponseFactory;

use App\User;
use DB;

class SiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        return view('site.home');
    }
    public function recover(Request $request)
    {
        $data = $request->all();

       if($data['formData'][3]['value'] == ''){
            return response()->json(array('error'=>'Erro ao preencher Re-captcha. Tente novamente'));
        }

       $cpf = preg_replace('/[^0-9]/', '', $data['formData'][0]['value']);

       $first_name = explode(' ', strtoupper($data['formData'][2]['value']));
       $first_name = $first_name[0];

       $user = User::where('cpf', $cpf)
        ->where('birth', $data['formData'][1]['value'])
        ->where('mother_name', 'LIKE', $first_name.' %')
        ->first();
       
        $arr_cod = array();

        if(isset($user->id)){
            array_push($arr_cod, $user->code);
        }

        return response()->json($arr_cod);
    }
}
