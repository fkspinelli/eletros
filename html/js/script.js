$(document).ready(function() {
    $(window).on('load resize', function() {
        if ($(window).width() < 992) {
            $('header .header-bottom .menu-b > li>a').click(function(event) {
                event.preventDefault();
                if ($(this).find('~ul').size() > 0) {
                    if ($(this).hasClass('active')) {
                        $(this).find('~ul').slideUp('fast', function(){
                            $(this).parents('li').find('>a').removeClass('active');
                        });
                    } else {
                        $(this).find('~ul').slideDown('fast', function(){
                            $(this).parents('li').find('>a').addClass('active');
                        });
                        
                    }
                }
            });
        }
    });

    $('[name="CNPJ"]').mask("99.999.999/9999-99");
    $('.cpf').mask("999.999.999-99");


    $('.cpfuser').on('keydown', function() {
        var inp = $(this).unmask();
        setTimeout(function(){
            
            var str = $(inp).val();
            var patt = new RegExp("[a-z]", "i");
            var res = patt.test(str);
            if(res){
                $(inp).unmask();
            }else{
                $(inp).mask("999.999.999-99");
            }
        });
    });



    $('.files ul li a').each(function() {
        $(this).wrapInner("<p></p>");
        $(this).prepend('<i class="fa fa-file-pdf-o" aria-hidden="true"></i>');
    });
    $(".js-social-share").on("click", function(e) {
        e.preventDefault();
        window.open($(this).attr("href"), '1466529457969', 'width=500,height=300,toolbar=0,menubar=1,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
    });


    // show menu mobile
    $('header .fa.fa-bars').on('click touchstart', function() {
        if ($(this).hasClass('active')) {
            $('header .menu-a ul').slideUp('fast', function(){
                $('header .fa.fa-bars').removeClass('active');
            });
        } else {
            $('header .menu-a ul').slideDown('fast', function(){
                $('header .fa.fa-bars').addClass('active');
            });
        }
    });

    var slideSize = $('.galeria-de-midia ul li a img').size();

    if ($(window).width() > 715) {
        if (slideSize == 2) {
            _minSlides = 2;
            _maxSlides = 2;
            _slideMargin = 440;
        } else if (slideSize == 3) {
            _minSlides = 3;
            _maxSlides = 3;
            _slideMargin = 110;
        } else {
            _minSlides = 4;
            _maxSlides = 4;
            _slideMargin = 0;
        }

        if (slideSize < 5) {
            _prevText = '';
            _nextText = '';
        } else {
            _prevText = '<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>';
            _nextText = '<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>';
        }
    } else if ($(window).width() > 515) {
        _minSlides = 2;
        _maxSlides = 2;
        _slideMargin = 0;

        if (slideSize < 3) {
            _prevText = '';
            _nextText = '';
        } else {
            _prevText = '<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>';
            _nextText = '<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>';
        }
    } else {
        _minSlides = 1;
        _maxSlides = 1;
        _slideMargin = 0;

        if (slideSize < 2) {
            _prevText = '';
            _nextText = '';
        } else {
            _prevText = '<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>';
            _nextText = '<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>';
        }
    }

    $('.bxslider').bxSlider({
        minSlides: _minSlides,
        maxSlides: _maxSlides,
        slideWidth: 300,
        slideMargin: _slideMargin,
        responsive: true,
        prevText: _prevText,
        nextText: _nextText
    });


    // toolbar - font zoom
    $('.tool-bar .more-font').click(function(e) {
        e.preventDefault();
        $('.font-zoom *').each(function() {
            var size = parseInt($(this).css('font-size').replace('px', ''));
            var lineHeight = parseInt($(this).css('line-height').replace('px', ''));
            $(this).css({ 'font-size': size + 1 + "px", "line-height": lineHeight + 2 + "px" });
        });
    });
    $('.tool-bar .less-font').click(function(e) {
        e.preventDefault();
        $('.font-zoom *').each(function() {
            var size = parseInt($(this).css('font-size').replace('px', ''));
            var lineHeight = parseInt($(this).css('line-height').replace('px', ''));
            $(this).css({ 'font-size': size - 1 + "px", "line-height": lineHeight - 2 + "px" });
        });
    });

    // toolbar - print
    $('.tool-bar .print-page').click(function(e) {
        e.preventDefault();
        window.print();
    });

    $('.box-destaque img, .slide img').removeClass('thumbnail');

    $('.tabs.tab-vertical .tab-col-left li a').on('click touchstart', function() {;
        if ($(window).width() < 768) {
            $('html, body').animate({ scrollTop: 1200 }, 800);
        }
        if($('#selec_conveniados').length > 0){
            $('.extra_content').empty();
        }
    });

    var colH = 0;
    $('.row-eq-height .colh').each(function() {
        if ($(this).height() > colH) {
            colH = $(this).height();
        }
    });
    $('.row-eq-height .colh').height(colH + 20);


    // busca endereço pelo cep para o formulário 2ª via do cartão
    var inputsCEP = $('#logradouro, #bairro, #localidade');
    var inputsRUA = $('#cep, #bairro');
    var validacep = /^[0-9]{8}$/;

    function limpa_formulário_cep(alerta) {
      if (alerta !== undefined) {
        // alert(alerta);
        $('#modalAlerta .modal-body p').text(alerta);
        $('#modalAlerta').modal('show');
      }

      inputsCEP.val('');
    }

    // Digitando CEP
    $('#cep').on('blur', function(e) {

      var cep = $('#cep').val().replace(/\D/g, '');

      if (cep !== "" && validacep.test(cep)) {

        inputsCEP.val('...');


      $.get('https://viacep.com.br/ws/' + cep + '/json/', function(data) {

        if (!("erro" in data)) {

          if (Object.prototype.toString.call(data) === '[object Array]') {
            var data = data[0];
          }

          $.each(data, function(nome, info) {
            $('#' + nome).val(nome === 'cep' ? info.replace(/\D/g, '') : info).attr('info', nome === 'cep' ? info.replace(/\D/g, '') : info);
          });



        } else {
          limpa_formulário_cep("CEP não encontrado.");
        }

      });


      } else {
        limpa_formulário_cep(cep == "" ? undefined : "Formato de CEP inválido.");
      }
    });

    // Tab de busca de empresas conveniadas em Eletros convenios
      if($('#selec_conveniados').length > 0){
          $.ajax({
            url: '/wp-content/themes/eletros/resources/ajax/categoria_conveniados.php',
            type: 'post',
            dataType: 'json'
          }).done(function(data){
            var res = '';
           $.each(data, function(i,v){
                res += '<option value="'+v.term_id+'">'+v.name+'</option>';
            });

            $('#selec_conveniados').append(res);
          });

          $('.btn_conveniados').click(function(){
            $('.extra_content').empty();
            $('.loader').fadeIn();
            var s = $('input[name="searched"]').val();
            var c = $('select[name="cat"]').val();

            $.ajax({
                url: '/wp-content/themes/eletros/resources/ajax/conveniados.php',
                data: {s:s, c:c},
                type: 'post',
                dataType: 'json'
            }).done(function(data){
                $('.loader').fadeOut();
                if(data != ''){
                    var res = '';
                    $.each(data, function(i,v){
                        res += '<div class="row"><div col-md-12><h3>'+v.title+'</h3>'+v.content+'</div></div>';
                    });

                    $('.extra_content').append(res);
                }else{
                    $('.extra_content').append('<div class="alert alert-warning">Não encontrado nenhum resultado tente outra pesquisa.</div>');
                }
            });
          }); 
        }

        $('.news_l').click(function(){
            var id = $(this).data('id');
            $('.content_noticias').empty();
            $('.loader').fadeIn();
            $.ajax({
                url: '/wp-content/themes/eletros/resources/ajax/noticias-pegr.php',
                type: 'post',
                data: {id:id},
                dataType: 'JSON'
            }).done(function(data){
                //console.log(data['response']);
                 $('.loader').fadeOut();
                $('.content_noticias').html(data.response);
            });
        });

        $('#cancelInfo').click(function(){
            $('#novo .form-control').val('');
            $('#addModal').removeClass('edit');
            $('.ranking-das-corretoras #list .fa-pencil').removeClass('on');
            $('#addModal').modal("hide").removeClass('edit');
            $('.ranking-das-corretoras #list .fa-pencil').removeClass('on');
        });

        $('#saveInfo').click(function(){
            var nome = $('#novo [name="nome"]').val();
            var tempo_de_trabalho = $('#novo [name="tempo_de_trabalho"]').val();
            var emails = $('#novo [name="emails"]').val();
            var telefones = $('#novo [name="telefones"]').val();

            if($('#addModal').hasClass('edit')){
                $('.ranking-das-corretoras #list .fa-pencil.on').parents('tr').find('[name="nome[]"]').val(nome);
                $('.ranking-das-corretoras #list .fa-pencil.on').parents('tr').find('[name="tempo_de_trabalho[]"]').val(tempo_de_trabalho);
                $('.ranking-das-corretoras #list .fa-pencil.on').parents('tr').find('[name="emails[]"]').val(emails);
                $('.ranking-das-corretoras #list .fa-pencil.on').parents('tr').find('[name="telefones[]"]').val(telefones);
            }else{
                $('.ranking-das-corretoras #list tbody').append('<tr><td><input value="'+nome+'" name="nome[]" readonly></td><td><input value="'+tempo_de_trabalho+'" name="tempo_de_trabalho[]" readonly></td><td><input value="'+emails+'" name="emails[]" readonly></td><td><input value="'+telefones+'" name="telefones[]" readonly></td><td><i class="fa fa-pencil" aria-hidden="true"></i><i class="fa fa-times" aria-hidden="true"></i></td></tr>');
            }
            
            $('#novo .form-control').val('');
            $('#addModal').modal("hide").removeClass('edit');
            $('.ranking-das-corretoras #list .fa-pencil').removeClass('on');

            checkTable();
        });

        $("#addModal").on('hide.bs.modal', function () {
            $('#novo .form-control').val('');
        });

        $(document).on('click', '.ranking-das-corretoras #list .fa-times', function(){
            $(this).parents('tr').remove();
            checkTable();
        });

        $(document).on('click', '.ranking-das-corretoras #list .fa-pencil', function(){
            $(this).addClass('on')
            $('#addModal').modal("show").addClass('edit');

            var nome = $(this).parents('tr').find('[name="nome[]"]').val();
            var tempo_de_trabalho = $(this).parents('tr').find('[name="tempo_de_trabalho[]"]').val();
            var emails = $(this).parents('tr').find('[name="emails[]"]').val();
            var telefones = $(this).parents('tr').find('[name="telefones[]"]').val();

            $('#addModal').find('[name="nome"]').val(nome);
            $('#addModal').find('[name="tempo_de_trabalho"]').val(tempo_de_trabalho);
            $('#addModal').find('[name="emails"]').val(emails);
            $('#addModal').find('[name="telefones"]').val(telefones);
        });

        function checkTable(){
            var qtd = $('.ranking-das-corretoras #list tbody tr').size();
            if(qtd == 0){
                $('.ranking-das-corretoras #list').hide();
            }else{
                $('.ranking-das-corretoras #list').show();
            }
        }

        $('.ranking-das-corretoras input[type="radio"]').change(function(){
            if($(this).val() == 'sim' || $(this).val() == 'Sim'){
                $(this).parentsUntil('.row').find('.form-control').prop('disabled', false);
            }else{
                $(this).parentsUntil('.row').find('.form-control').prop('disabled', true).val('');
            }
        });


        $('.ranking-das-corretoras input[type="radio"]').each(function(){
            if($(this).val() == 'sim' || $(this).val() == 'Sim'){
                $(this).parentsUntil('.row').find('.form-control').prop('disabled', false);
            }else{
                $(this).parentsUntil('.row').find('.form-control').prop('disabled', true).val('');
            }            
        });

        $('.check-file input[type="file"]').hide().prop('disabled', true);
        $('.check-file input').change(function(){
            if($(this).val() == "Sim"){
                $('.check-file input[type="file"]').slideDown().prop('disabled', false);
            }else{
                $('.check-file input[type="file"]').slideUp().prop('disabled', true);
            }
        });

});
