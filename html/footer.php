    <!-- footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-3 eletros">
                    <img src="img/logo-footer.png" class="img-responsive">
                </div>
                <div class="col-sm-3">
                    <form>
                        <div class="form-group search">
                            <input type="search" class="form-control" placeholder="Busca">
                            <button type="submit"><i class="icon icon-basic-magnifier"></i></button>
                        </div>
                    </form>
                </div>
                <div class="col-sm-6">
                    <div class="redes-sociais">
                        <a href="#" target="_blank"><i class="icon icon-social-facebook-circle"></i></a>
                        <a href="#" target="_blank"><i class="icon icon-social-tumblr-circle"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 eletros hidden-xs">
                    A ELETROS é uma entidade fechada de previdência privada criada em 1971, em que podem ser participantes os empregados da Eletrobras, do Cepel, da EPE , do ONS, da Eletrobras Distribuição Rondônia e da própria ELETROS.
                    <br/>
                    <br/>
                    <p>A sua função é garantir a esses participantes benefícios de aposentadoria complementares aos da Previdência Social, contribuindo assim para a manutenção do padrão de vida desses participantes e seus </p>
                    <div class="direitos">
                        2016 - @Fundação Eletrobrás de Seguridade Social ELETROS
                    </div>
                </div>
                <div class="col-sm-3">
                    <p>A ELETROS</p>
                    <ul>
                        <li><a href="#">História</a></li>
                        <li><a href="#">Nossos Números</a></li>
                        <li><a href="#">Patrocinadoras</a></li>
                        <li><a href="#">Governança Corporativa</a></li>
                        <li><a href="#">Misssão e Visão</a></li>
                        <li><a href="#">Estatuto</a></li>
                        <li><a href="#">Código de Ética</a></li>
                    </ul>
                    <p>Nossos Planos</p>
                    <ul>
                        <li><a href="#">Plano BD Eletrobrás</a></li>
                        <li><a href="#">Plano CD Eletrobrás</a></li>
                        <li><a href="#">Plano CD CERON</a></li>
                        <li><a href="#">Plano CV ONS</a></li>
                        <li><a href="#">Plano CV EPE</a></li>
                    </ul>
                    <p>Investimentos</p>
                    <ul>
                        <li><a href="#">Gestão</a></li>
                        <li><a href="#">Política de Investimento</a></li>
                        <li><a href="#">Conjutura e Risco</a></li>
                        <li><a href="#">Demostrações Contábeis</a></li>
                        <li><a href="#">Perfis de Investimento</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <p>Programas</p>
                    <ul>
                        <li><a href="#">Cultivar</a></li>
                        <li><a href="#">Espaço ELETROS</a></li>
                        <li><a href="#">Pró-Equidade de Gênero e Raça</a></li>
                        <li><a href="#">Eletros Convênio</a></li>
                    </ul>
                    <p><a href="#">Publicações</a></p>
                    <p><a href="#">Mídia</a></p>
                    <p><a href="#">FAle conosco</a></p>
                </div>
                <div class="col-sm-3 area-conselho">
                    <p class="blue">Área do Conselho</p>
                    <p>Este é um ambiente restrito aos integrantes dos órgãos colegiados da ELETROS.</p>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Nº Eletros">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Senha">
                            <a href="#">Esqueci a senha</a>
                        </div>
                        <button class="btn btn-blue-1 btn-block text-uppercase">ENTRAR</button>
                    </form>
                    <p class="blue">Links úteis</p>
                    <div class="links">
                        <a href="#" target="_blank">
                            <img src="img/banner-previc.png" class="img-responsive">
                        </a>
                        <a href="#" target="_blank">
                            <img src="img/abrapp.png" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- /footer -->
</body>

</html>
